<?php

if( !$gCms ) exit();

if( ! $this->CheckPermission('Modify Templates') )
{
	$params['errors'] = $this->Lang('accessdenied');
	$this->Redirect($id,'defaultadmin','',$params);
	return;
}

if( !isset($params['mode']) )
{
	$params['errors'] = $this->Lang('error_insufficientparams');
	$this->Redirect($id,'defaultadmin','',$params);
	return;
}

$params['origaction'] = $params['action'];
$templateprops = array('templateid'=>0, 'thumbwidth'=>'', 'thumbheight'=>'', 'resizemethod'=>'', 'maxnumber'=>'', 'sortitems'=>'');
$contents = "";
if( $params['mode'] == 'add' )
{
	$this->smarty->assign('formstart', $this->CreateFormStart ($id, 'do_addtemplate',$returnid,'post','', false, '', $params));
	$this->smarty->assign('templatename', $this->CreateInputText( $id, 'template', "", 40 ));
	if( isset($params['defaulttemplatepref']) && $params['defaulttemplatepref'] != '' )
	{
		$contents = $this->GetPreference($params['defaulttemplatepref']);
	}
	if( isset($params['template']) && $params['template'] != '' )
	{
		$contents = $this->GetTemplate($params['template']);
	}
}
else 
{
	if( !isset($params['template']) )
	{
		$params['errors'] = $this->Lang('error_insufficientparams');
		$this->Redirect($id,'defaultadmin','',$params);
		return;
	}
	$this->smarty->assign('formstart', $this->CreateFormStart ($id, 'do_edittemplate',$returnid,'post','', false, '', $params));
	$this->smarty->assign('templatename',$params['template']);
	$contents = $this->GetTemplate($params['template']);
	$templateprops = $this->_GetTemplateprops($params['template']);
}

$this->smarty->assign('hidden', $this->CreateInputHidden($id, 'templateid', $templateprops['templateid']));

$this->smarty->assign('prompt_thumbnailsize', $this->Lang('thumbnailsize'));
$resizemethodlist = array($this->Lang('crop')=>'cr',$this->Lang('scale')=>'sc',$this->Lang('zoomcrop')=>'zc',$this->Lang('zoomscale')=>'zs');
$this->smarty->assign('thumbnailsize',
		$this->Lang('leaveempty') . '<br />' .
		$this->Lang('width') . ':&nbsp;' .
		$this->CreateInputText( $id, 'thumbwidth', $templateprops['thumbwidth'], 4, 4 ) .
		'&nbsp;&nbsp;&nbsp;' . $this->Lang('height') . ':&nbsp;' .
		$this->CreateInputText( $id, 'thumbheight', $templateprops['thumbheight'], 4, 4 ) .
		'&nbsp;&nbsp;&nbsp;' . $this->Lang('resizemethod') . ':&nbsp;' .
		$this->CreateInputDropdown($id, 'resizemethod', $resizemethodlist, -1, $templateprops['resizemethod'])
);

$sortfieldlist = array(' -'=>'','filename'=>'s#file', 'filedate'=>'s#filedate', 'title'=>'s#title', 'comment'=>'s#comment', 'subgallery (true/false)'=>'n#isdir', 'active (true/false)'=>'n#active');
$sorttypelist = array($this->Lang('ascending')=>'+',$this->Lang('descending')=>'-');
$sortitems = explode('/',$templateprops['sortitems']);
$sortfields = '';
foreach ($sortitems as $sortitem)
{
	$sortfields .= '<p class="sortfield">' .
	$this->CreateInputDropdown($id, 'sortfield[]', $sortfieldlist, -1, substr($sortitem,0,1) . '#' . substr($sortitem,2)) . '&nbsp; ' .
	$this->CreateInputDropdown($id, 'sorttype[]', $sorttypelist, -1, substr($sortitem,1,1)) .
	'</p>';
}
$this->smarty->assign('prompt_sortingoptions', $this->Lang('sortingoptions'));
$this->smarty->assign('sortingoptions', '<p>' . $this->Lang('specifysortfields') . '</p>' .
	'<div id="sortfields">' . $sortfields . '</div>
	<p><a href="#" id="addfield">' . $this->Lang('addfield') . '</a>&nbsp;&nbsp;&nbsp;<a href="#" id="deletefield">' . $this->Lang('deletefield') . '</a></p>'
);

// TODO: add form inputs for sortingoptions

$contentscode = explode(TEMPLATE_SEPARATOR, $contents);

$this->smarty->assign('title',$this->Lang('title_template'));
$this->smarty->assign('prompt_templatename',$this->Lang('prompt_templatename'));
$this->smarty->assign('prompt_template',$this->Lang('prompt_template'));
$this->smarty->assign('template', $this->CreateSyntaxArea ($id, $contentscode[0], 'templatecontent'));

$this->smarty->assign('prompt_maxnumber',$this->Lang('maxnumber'));
$this->smarty->assign('maxnumber', $this->CreateInputText( $id, 'maxnumber', $templateprops['maxnumber'], 4, 10 ));
$this->smarty->assign('showallimages',$this->Lang('showallimages'));

$this->smarty->assign('availablevariables',$this->Lang('availablevariables'));
$this->smarty->assign('availablevariableslist',$this->Lang('availablevariableslist'));
$this->smarty->assign('availablevariableslink',$gCms->variables['admintheme']->DisplayImage('icons/system/info.gif', $this->Lang('availablevariables'),'','','systemicon'));

$this->smarty->assign('prompt_templatecss',$this->Lang('prompt_templatecss'));
$this->smarty->assign('templatecss', $this->CreateSyntaxArea ($id, str_replace("*}", "", $contentscode[1]), 'templatecss'));
$this->smarty->assign('prompt_templatejs',$this->Lang('prompt_templatejs'));
$this->smarty->assign('templatejs', $this->CreateSyntaxArea ($id, substr($contentscode[2],0,-2), 'templatejs'));
$this->smarty->assign('submit',$this->CreateInputSubmit ($id, 'submitbutton', $this->Lang('submit')));
$this->smarty->assign('apply',$params['mode'] == 'add' ? '' : $this->CreateInputSubmit($id, 'applybutton', $this->Lang('apply')));
$this->smarty->assign('cancel',$this->CreateInputSubmit ($id, 'cancel', $this->Lang('cancel')));
$this->smarty->assign('reset',$params['mode'] != 'add' && file_exists('..'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'Gallery'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'template_'.strtolower($params['template']).'.tpl') ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $this->CreateInputSubmit($id, 'resetbutton', $this->Lang('resetoriginal'), '', '', $this->Lang('resetoriginalwarning')) : '');
$this->smarty->assign('formend',$this->CreateFormEnd());

echo $this->ProcessTemplate('edittemplate.tpl');

?>
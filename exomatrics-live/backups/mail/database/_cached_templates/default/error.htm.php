<?php /* Smarty version 2.3.1, created on 2008-01-06 13:49:53
         compiled from default/error.htm */ ?>
<?php $this->_config_load($this->_tpl_vars['umLanguageFile'], "Error", 'local'); ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>UebiMiau - <?php echo $this->_config[0]['vars']['err_title']; ?>
</title>
	<link rel="stylesheet" href="themes/default/webmail.css" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_config[0]['vars']['default_char_set']; ?>
">
</head>

<body bgcolor="#778899" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">
<br><br><br>
<table width="400" border="0" cellspacing="0" cellpadding="2" align="center">
	<tr>
		<td><img src="images/logo.gif" width="183" height="73" border="0" alt=""></td>
	<tr>
		<td bgcolor=white>
			<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
				<tr><td align=right class="title">.: <font color=red size=3><b><?php echo $this->_config[0]['vars']['err_title']; ?>
</b></font> :.</td>
				<tr><td align=right class="cent"><br>
				<?php echo $this->_config[0]['vars']['err_msg']; ?>
<br><br>
				<small><?php echo $this->_config[0]['vars']['err_system_msg']; ?>
 
				<?php if ($this->_tpl_vars['umErrorCode'] == "1"): ?><?php echo $this->_config[0]['vars']['error_connect']; ?>

				<?php elseif ($this->_tpl_vars['umErrorCode'] == "2"): ?><?php echo $this->_config[0]['vars']['error_retrieving']; ?>

				<?php else: ?><?php echo $this->_config[0]['vars']['error_other']; ?>
<?php endif; ?>
				
				</small><br><br>
				<a href="logout.php?sid=<?php echo $this->_tpl_vars['umSid']; ?>
&tid=<?php echo $this->_tpl_vars['umTid']; ?>
&lid=<?php echo $this->_tpl_vars['umLid']; ?>
"><?php echo $this->_config[0]['vars']['err_exit']; ?>
</a><br><br>
				</td>
			</table>
		</td>
	</tr>
</td>
</table>
</body>
</html>
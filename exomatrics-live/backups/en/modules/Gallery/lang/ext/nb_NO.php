<?php
$lang['friendlyname'] = 'Gallery ';
$lang['moddescription'] = 'Den enkleste m&aring;ten &aring; behandle og vise fotoalbum p&aring;';
$lang['description'] = 'Et galleri som er enkelt &aring; bruke som automatisk viser bildene i en angitt katalog';
$lang['postinstall'] = 'Gallery modulen ble installert';
$lang['installed'] = 'Gallery modulen med versjon %s ble installert';
$lang['upgraded'] = 'Gallery modulen er oppgradert til versjon %s.';
$lang['postuninstall'] = 'Gallery modulen ble avinstallert';
$lang['uninstalled'] = 'Gallery modulen ble avinstallert';
$lang['really_uninstall'] = 'Er du sikker p&aring; at du vil avinstallere Gallery? Dette har ingen effekt p&aring; bildene, men alle kommentar-data og miniatyrbilder opprettet av Gallery vil g&aring; tapt.';
$lang['accessdenied'] = 'Tilgang nektet. Vennligst sjekk dine rettigheter.';
$lang['actions'] = 'Handlinger';
$lang['active'] = 'Aktive';
$lang['apply'] = 'Utf&oslash;r';
$lang['areyousure'] = 'Er du sikker p&aring; at du vil slette?';
$lang['cancel'] = 'Avbryt';
$lang['copy'] = 'Kopier';
$lang['default'] = 'Standard';
$lang['delete'] = 'Slett';
$lang['edit'] = 'Rediger';
$lang['error'] = 'Feil!';
$lang['setfalse'] = 'Sett usann';
$lang['settrue'] = 'Sett sann';
$lang['submit'] = 'Lagre';
$lang['galleries'] = 'Gallerier';
$lang['gallerypath'] = 'Galleri';
$lang['editgallery'] = 'Rediger galleri';
$lang['addgallery'] = 'Legg til galleri';
$lang['nogalleriestext'] = 'Ingen galleri tilgjengelig';
$lang['item'] = 'Bilde';
$lang['title'] = 'Tittel';
$lang['date'] = 'Dato';
$lang['nofilestext'] = 'Ingen bilder tilgjengelige';
$lang['gallerytitle'] = 'Galleritittel';
$lang['comment'] = 'Kommentar';
$lang['template'] = 'Mal';
$lang['hideparentlink'] = 'Skjul lenke til foreldre galleri';
$lang['usedefault'] = 'benytt standard';
$lang['sortbysettings'] = 'Sorter etter malinnstillinger';
$lang['galleryupdated'] = 'Galleri oppdatert';
$lang['error_updategalleryfailed'] = 'Oppdatering av galleriet feilet';
$lang['templates'] = 'Maler';
$lang['title_available_templates'] = 'Tilgjengelige maler';
$lang['prompt_name'] = 'Navn';
$lang['prompt_default'] = 'Standard';
$lang['prompt_newtemplate'] = 'Opprett ny mal';
$lang['title_template'] = 'Mal redigerer';
$lang['prompt_templatename'] = 'Malnavn';
$lang['thumbnailsize'] = 'Miniatyrbildest&oslash;rrelse (Frontend)';
$lang['leaveempty'] = '(la dette felt tomt om du vil benytte standard miniatyrbilder fra ImageManager)';
$lang['width'] = 'bredde';
$lang['height'] = 'h&oslash;yde';
$lang['resizemethod'] = 'skaleringsmetode';
$lang['crop'] = 'klipp';
$lang['scale'] = 'skaler';
$lang['zoomcrop'] = 'zoom &amp; klipp';
$lang['zoomscale'] = 'zoom &amp; skaler';
$lang['maxnumber'] = 'Maksimum antall enheter per side';
$lang['showallimages'] = '&Aring; la dette st&aring; tomt vil vise alle bilder uten sidelenker';
$lang['sortingoptions'] = 'Sorteringsvalg';
$lang['specifysortfields'] = 'Spesifiser felt(er) &aring; sortere etter';
$lang['ascending'] = 'stigende';
$lang['descending'] = 'synkende';
$lang['addfield'] = 'Legg til felt';
$lang['deletefield'] = 'Slett siste rad';
$lang['prompt_template'] = 'Malkilde';
$lang['prompt_templatejs'] = 'Mal javackript';
$lang['prompt_templatecss'] = 'Mal CSS-stilark';
$lang['resetoriginal'] = 'Reset to original';
$lang['resetoriginalwarning'] = 'Are you sure you want to reset the template code to its original code?';
$lang['resetoriginal'] = 'Tilbakestill til original';
$lang['resetoriginalwarning'] = 'Er du sikker p&aring; at du vil tilbakestille malkoden til dens originale kode?';
$lang['templateupdated'] = 'Malen ble oppdatert';
$lang['templateadded'] = 'Malen ble lagt til';
$lang['error_templatenameexists'] = 'Malnavnet eksisterer allerede';
$lang['templatedeleted'] = 'Mal slettet';
$lang['availablevariables'] = 'Tilgjengelige Smartyvariabler for mal';
$lang['availablevariableslist'] = 'These are the variables you can use to customize your template:<br /><br />
<code>{$module_message}</code> - error message, is only set if there&#039;s a message<br />
<code>{$gallerytitle}</code> - title of the gallery. If there is no title, this will show the directory name<br />
<code>{$gallerycomment}</code> - comment of the gallery<br />
<code>{$parentlink}</code> - link to the parent folder<br />
<code>{$hideparentlink}</code> - true/false<br />
<code>{$imagecount}</code> - shows e.g. &quot;6 images&quot;, depending on language<br />
<code>{$itemcount}</code> - number of items, images + folders<br />
<code>{$pages}</code> - number of pages<br />
<code>{$prevpage}</code> - link to previous page, if applicable<br />
<code>{$nextpage}</code> - link to next page, if applicable<br />
<code>{$pagelinks}</code> - links to each existing page<br />
<code>{$images}</code> - array with keys:<br />
 - <code>file</code> - relative path to the original image (or subgallery)<br />
 - <code>title</code> - title of the image. If there is no title, this will show the filename<br />
 - <code>comment</code> - comment to the image<br />
 - <code>filedate</code> - creation date/time<br />
 - <code>thumb</code> - relative path to the thumbnail. In case of a subgallery, this wil revert to the image which is set as default in that subgallery. If no image is set as default, a standard folder-icon will be used.<br />
 - <code>fileid</code> - unique id for the image or subgallery<br />
 - <code>isdir</code> - true if it&#039;s a subgallery<br />';
$lang['options'] = 'Valg';
$lang['allowed_extensions'] = 'Tillatte utvidelser(Extensions)';
$lang['use_comment_wysiwyg'] = 'Benytt en WYSIWYG redigerer p&aring; Gallery kommentarfeltet';
$lang['optionsupdated'] = 'Valgene ble oppdatert.';
$lang['image'] = 'Bilde';
$lang['images'] = 'bilder';
$lang['prevpage'] = 'Forrige';
$lang['nextpage'] = 'neste';
$lang['defaultgallerycomment'] = 'Thank you for installing the Gallery module. If you have uploaded some images to the &#039;uploads/images/Gallery/&#039; folder, you will see them below. You can edit titles, descriptions and thumbnail sizes in the admin section. Check out all the other features this module offers in the module-help.';
$lang['help_dir'] = 'Parameter for &aring; spesifisere en katalog, relativt til uploads/images/Gallery/';
$lang['help_template'] = 'Benytt en annen databasemal for visning av fotgalleriet. Denne mal m&aring; eksistere og v&aelig;re synlig i mal-fanen i gallery-adminomr&aring;det, men den trenger ikke v&aelig;re satt som standardmal. Om denne parameter ikke er satt, vil malen tilknytttet dennekatalogen bli benyttet, ellers blir standardmalen benyttet.';
$lang['help_number'] = 'Maksimum antall miniatyrbilder &aring; vise (per side). Ved &aring; la denne parameter v&aelig;re tom vil vise alle bilder.';
$lang['help_start'] = 'Start p&aring; nde bilde. Ved &aring; la denne parameter tom vil starte p&aring; f&oslash;rste bilde.';
$lang['help_show'] = 'Overstyr hvilke bilder &aring; vise. Mulige verdier er:
<ul>
<li>&#039;active&#039; - &aring; vise de bilder som er markert aktiv (standard)</li>
<li>&#039;inactive&#039; - &aring; vise kun de bilder som er satt som inaktive</li>
<li>&#039;all&#039; - &aring; vise alle bilder</li>
</ul>
';
$lang['help_category'] = '&Aring; vise kun bilder/gallerier tilegnet til de spesifiserte kategoriene. Benytt * etter navnet for &aring; vise barna ogs&aring;. Flere kategorier kan benyttes om de seprareres med komma. Ved &aring; la dette st&aring; blankt, vil alle katagorier vises.';
$lang['help_action'] = 'Override the default action. Use it in combination with the above parameters. Possible values are:
<ul>
<li>&#039;showrandom&#039; - to display a set of random thumb-images (applies only to the images which are stored in the database, defaults to a number of 6 images). Use &#039;/*&#039; after the directoryname in the dir parameter to include images from subdirectories</li>
<li>&#039;showlatest&#039; - to display the most recently added images (applies only to the images which are stored in the database, defaults to a number of 6 images) The dir parameter can be set, or the default Gallery-root will be used. Subdirectories are automatically included</li>
<li>&#039;showlatestdir&#039; - to display a set of random thumb-images from the most recently added directory (applies only to the images which are stored in the database, defaults to a number of 6 images)</li>
</ul>
Note that images are only stored in the database when the gallery is visited in the admin.';
$lang['changelog'] = '<ul>
<li>Version 1.1.2  5 December 2009. Update Fancybox system to version 1.2.6, Fix for Bugreport #4422: Error when sortingoption is empty, Built in check for templates/css directory (BR #4305).</li>
<li>Version 1.1.1. 12 November 2009. Fix for Bugreport #4294: Thumbfolder not created when upgrading.</li>
<li>Version 1.1. 8 November 2009. Added sorting and drag&amp;drop feature (FR #3907 #4210), Moved thumbnailsettings to templatestab, Make galleries searchable (FR #3956), Create backend thumbs if they don&#039;t exist in IM (FR #4101), Fix for BR #4027, Added fileid to image-object (FR #4259)</li>
<li>Version 1.0.3. 8 September 2009. Bugfix for [#4001], Improved synchronizing the database to files, and other minor improvements.</li>
<li>Version 1.0.2. 5 September 2009. Bugfix for [#3983] regarding spaces in directory names, Added templates Fancybox and Lytebox_slideshow as in Feature requests [#3944] and [#3967]</li>
<li>Version 1.0.1. 23 August 2009. Apply thumbnail-settings on &#039;Album-covers&#039; [#3924], Bugfix for [#3927], Added missing /div to thickbox-template, fixed an issue with generating thumbnails when no thumbs available in ImageManager, improved the naming of thumbs, Bugfix for [#3931], Prevent display the content of galleries that are set as inactive.</li>
<li>Version 1.0. 17 August 2009. Pagination parameters added, Standard templates changed accordingly, Extended the module-help, Added functions to show random or latest images, Support of pretty-urls, Various improvements</li>
<li>Version 0.6. 9 August 2009. Initial Beta Release.</li>
</ul>';
$lang['help'] = '<h3>What Does This Do?</h3>
<p>The Gallery module is an easy to use photo gallery which automatically shows the images of a specified directory. Subdirectories will be shown as subgalleries. It has lots of features, such as automatic thumbnailing, the use of multiple Lightbox-like templates or any css/javascript template you like, and you can give titles and descriptions to your galleries as well as your photos.</p>
<h3>How Do I Use It</h3>
<p>First, insert the module with the <code>{Gallery}</code> tag into your page or template anywhere you whish. Then upload some images with the Image Manager, File Manager (e.g. multiple images in zipfile) or FTP to the uploads/images/Gallery/ directory.</p>
<p>That&#039;s it!</p>
<p>If you want more photo galleries, simply create a subdirectory and upload your photos as described. By adding parameters to the <code>{Gallery}</code> tag, you can easily manipulate which subgallery will be shown in which template, e.g. <code>{Gallery dir=&quot;holidays/Netherlands2009&quot; template=&quot;Lightbox&quot;}</code></p>
<p>By default the thumbnails from the Image Manager are used to display the photo galleries.</p>
<h3>Advanced Options, but still easy to use</h3>
<p>In the admin section you have lots of other options:</p>
<ul>
<li>Set a photo as the default for a gallery, so it will show in the parent gallery as a &#039;cover&#039; in stead of the default folder-icon.</li>
<li>Give titles and descriptions to galleries.</li>
<li>Set a default template for each gallery.</li>
<li>Set thumbnail sizes for each template, with posibilities to scale, crop and/or zoom the images.</li>
<li>Specify for each template in which order the photos have to be sorted.</li>
<li>Overrule the sorting manually by dragging&amp;dropping the photos.
<li>Give titles and descriptions to photos.</li>
<li>Switch a specific photo or gallery to inactive, preventing it from display.</li>
<li>Edit/copy templates or create new ones. Check the info-icon beneath the template-code for the available variables.</li>
</ul>
<p>In order to edit templates, the user must belong to a group with the &#039;Modify Templates&#039; permission. To edit the global Gallery options, the user must belong to a group with the &#039;Modify Site Preferences&#039; permission.</p>
<p>All titles, descriptions and settings are stored in the database. The database will synchronize with the filedirectory each time the according gallery is visited in the Gallery-admin. A little warning: when you move an image or subdirectory to another directory, you will loose its title, description and settings.</p>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Feature Request or Bug Report, please visit the Module Forge
<a href="http://dev.cmsmadesimple.org/projects/gallery/">Gallery Page</a>.</li>
<li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.</li>
<li>Lastly, you may have some success emailing the author directly.</li>
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>

<h3>Copyright and License</h3>
<p>Copyright &amp;copy; 2009, Jos -<a href="mailto:josvd@live.nl">josvd@live.nl</a>-. All Rights Are Reserved.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>';
$lang['utmz'] = '156861353.1250297380.1752.42.utmccn=(referral)|utmcsr=helminsen.no|utmcct=/install/upgrade.php|utmcmd=referral';
$lang['utma'] = '156861353.179052623084110100.1210423577.1257631821.1257635100.2088';
$lang['qca'] = '1210971690-27308073-81952832';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353.2.10.1257635100';
?>
<?php /* Smarty version 2.3.1, created on 2007-09-12 15:41:50
         compiled from default/address-form.htm */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'escape', 'default/address-form.htm', 42, false),)); ?><?php $this->_config_load($this->_tpl_vars['umLanguageFile'], "AddressBook", 'local'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>UebiMiau - <?php echo $this->_config[0]['vars']['adr_title']; ?>
</title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_config[0]['vars']['default_char_set']; ?>
">
</head>
<link rel="stylesheet" href="themes/default/webmail.css" type="text/css">
<script language="JavaScript" src="themes/default/webmail.js" type="text/javascript"></script>

<?php echo $this->_tpl_vars['umJS']; ?>


<body bgcolor="#778899" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">


<table width="760" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor=white>
	<tr>
		<td valign=top width="15%">
                        <table cellspacing=1 cellpadding=3 width="100%" border=0 bgcolor=White>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"> <a class="menu" href="javascript:goinbox()"><?php echo $this->_config[0]['vars']['messages_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:newmsg()"><?php echo $this->_config[0]['vars']['compose_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:refreshlist()"><?php echo $this->_config[0]['vars']['refresh_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:folderlist()"><?php echo $this->_config[0]['vars']['folders_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:search()"><?php echo $this->_config[0]['vars']['search_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:addresses()"><?php echo $this->_config[0]['vars']['address_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:emptytrash()"><?php echo $this->_config[0]['vars']['empty_trash_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:prefs()"><?php echo $this->_config[0]['vars']['prefs_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:goend()"><?php echo $this->_config[0]['vars']['logoff_mnu']; ?>
</a></td></tr>
                        </table>
		</td>

		<td bgcolor=white valign=top>
			<form name=form1 method=post action=addressbook.php>

			<input type=hidden name=sid value="<?php echo $this->_tpl_vars['umSid']; ?>
">
			<input type=hidden name=id value="<?php echo $this->_tpl_vars['umAddrID']; ?>
">
			<input type=hidden name=lid value="<?php echo $this->_tpl_vars['umLid']; ?>
">
			<input type=hidden name=tid value="<?php echo $this->_tpl_vars['umTid']; ?>
">
			<input type=hidden name=opt value="<?php echo $this->_tpl_vars['umOpt']; ?>
">

			<table width="400" border="0" cellspacing="1" cellpadding="0">
				<tr><td class="headerright" size="20%"><?php echo $this->_config[0]['vars']['adr_name']; ?>
 &nbsp;</td><td><input size="40" type=text name="name" value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAddrName'], "html"); ?>
" class="textbox"></td></tr>
				<tr><td class="headerright"><?php echo $this->_config[0]['vars']['adr_email']; ?>
 &nbsp;</td><td><input size="40" type=text name="email" value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAddrEmail'], "html"); ?>
" class="textbox"></td></tr>
				<tr><td class="headerright"><?php echo $this->_config[0]['vars']['adr_street']; ?>
 &nbsp;</td><td><input size="40" type=text name="street" value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAddrStreet'], "html"); ?>
" class="textbox"></td></tr>
				<tr><td class="headerright"><?php echo $this->_config[0]['vars']['adr_city']; ?>
 &nbsp;</td><td><input size="40" type=text name="city" value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAddrCity'], "html"); ?>
" class="textbox"></td></tr>
				<tr><td class="headerright"><?php echo $this->_config[0]['vars']['adr_state']; ?>
 &nbsp;</td><td><input size="3" type=text name="state" value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAddrState'], "html"); ?>
" class="textbox" maxlength=2></td></tr>
				<tr><td class="headerright"><?php echo $this->_config[0]['vars']['adr_work']; ?>
 &nbsp;</td><td><input size="40" type=text name="work" value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAddrWork'], "html"); ?>
" class="textbox"></td></tr>
				<tr><td>&nbsp;</td><td><input type=submit value="<?php echo $this->_config[0]['vars']['adr_save']; ?>
" class=button> &nbsp;<input type=button value="<?php echo $this->_config[0]['vars']['adr_back']; ?>
" class=button onClick="location = '<?php echo $this->_tpl_vars['umGoBack']; ?>
'"></td></tr>
			</table>
			</form>
		</td>
	</tr>
</table>
	


</body>
</html> 
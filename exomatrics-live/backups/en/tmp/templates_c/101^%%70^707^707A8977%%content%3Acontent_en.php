<?php /* Smarty version 2.6.25, created on 2013-07-07 14:24:18
         compiled from content:content_en */ ?>
<p>Hexomatrixx Energy Systems India Pvt Ltd (HESIL) is an organization engaged in &nbsp;procurement&nbsp; manufacturing and supplychain support services&nbsp; to&nbsp;industries engaged in energy, Petrochemicals, logistics and downstream&nbsp;distribution services in India. &nbsp;</p>
<ul style="color:#000;">
    <li><strong>A &nbsp;Total&nbsp;Supply&nbsp;Chain&nbsp;which includes&nbsp;&nbsp; Supplier/&nbsp;Customer Management</strong>&nbsp;to meet your entire day to day logistics and distribution requirements.</li>
    <li>Our services are designed to offer you a <strong>Flexible 24/7 service</strong> to meet your entire supply and logistics requirement.&nbsp;&nbsp;Our&nbsp;Consultants are&nbsp;available&nbsp;round the clock&nbsp;to meet&nbsp;any&nbsp;of your logistics /&nbsp;operational queries.&nbsp; &nbsp;</li>
    <li>At <strong>HESIL </strong>we have the ability to source branded or generic products or&nbsp; accessories world&nbsp;&nbsp; wide to meet any of your manufacturing requirement and relieves you from sourcing headaches.</li>
    <li>&nbsp;<strong>HESIL</strong> reduces your number of purchase orders, saving money instantly.&nbsp;</li>
    <li><strong>&nbsp;</strong>Consolidated singly monthly invoicing also cuts &lsquo;back office&rsquo; admin costs.</li>
</ul>
<!-- Add code here that should appear in the content block of all new pages -->
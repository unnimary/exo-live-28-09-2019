<?php
$lang['friendlyname'] = 'Gallery ';
$lang['moddescription'] = 'Le moyen le plus facile pour afficher et g&eacute;rer vos galeries photos.';
$lang['description'] = 'Une Galerie facile d&#039;emploi affichant automatiquement les images d&#039;un dossier sp&eacute;cifique.';
$lang['postinstall'] = 'Le module Gallery a &eacute;t&eacute; install&eacute; avec succ&egrave;s.';
$lang['installed'] = 'La version %s du module Gallery a &eacute;t&eacute; install&eacute;e.';
$lang['upgraded'] = 'Le module Gallery a &eacute;t&eacute; mis &agrave; jour &agrave; la version %s.';
$lang['postuninstall'] = 'Le module Gallery a &eacute;t&eacute; d&eacute;sinstall&eacute;.';
$lang['uninstalled'] = 'Le module Gallery a &eacute;t&eacute; d&eacute;sinstall&eacute;.';
$lang['really_uninstall'] = '&Ecirc;tes-vous certain de vouloir d&eacute;sinstaller Gallery&nbsp;? Cela n&#039;affectera pas les images, mais toutes les commentaires et vignettes cr&eacute;&eacute;s par Gallery seront perdus.';
$lang['accessdenied'] = 'Acc&egrave;s r&eacute;fus&eacute;. Veuillez v&eacute;rifier vos permissions.';
$lang['actions'] = 'Actions ';
$lang['active'] = 'Active ';
$lang['apply'] = 'Appliquer';
$lang['areyousure'] = '&Ecirc;tes-vous s&ucirc;r(e) de vouloir effacer ?';
$lang['cancel'] = 'Annuler';
$lang['copy'] = 'Copier';
$lang['default'] = 'D&eacute;faut';
$lang['delete'] = 'Effacer';
$lang['edit'] = 'Editer';
$lang['error'] = 'Erreur&nbsp;!';
$lang['setfalse'] = 'D&eacute;finir &agrave; faux';
$lang['settrue'] = 'D&eacute;finir &agrave; vrai';
$lang['submit'] = 'Sauvegarder';
$lang['galleries'] = 'Galeries';
$lang['gallerypath'] = 'Galerie';
$lang['editgallery'] = '&Eacute;diter la galerie';
$lang['addgallery'] = 'Ajouter une galerie';
$lang['nogalleriestext'] = 'Il n&#039;y a pas de galerie disponible';
$lang['item'] = 'Image ';
$lang['title'] = 'Titre';
$lang['date'] = 'Date ';
$lang['nofilestext'] = 'Aucune image disponible';
$lang['gallerytitle'] = 'Titre de la galerie';
$lang['comment'] = 'Commentaire';
$lang['template'] = 'Gabarit';
$lang['hideparentlink'] = 'Cacher le lien dans la galerie parente';
$lang['usedefault'] = 'Utiliser par d&eacute;faut';
$lang['createthumbs'] = '(Re)Cr&eacute;er les vignettes';
$lang['thumbscreated'] = 'Vignettes cr&eacute;es';
$lang['sortbysettings'] = 'Sort by template settings';
$lang['galleryupdated'] = 'La galerie a &eacute;t&eacute; mise &agrave; jour avec succ&egrave;s';
$lang['error_updategalleryfailed'] = 'Echec de la mise &agrave; jour de la galerie';
$lang['templates'] = 'Gabarits';
$lang['title_available_templates'] = 'Gabarits disponibles';
$lang['prompt_name'] = 'Nom';
$lang['prompt_default'] = 'D&eacute;faut';
$lang['prompt_newtemplate'] = 'Cr&eacute;er un nouveau gabarit';
$lang['title_template'] = 'Editeur de gabarit';
$lang['prompt_templatename'] = 'Nom du gabarit';
$lang['thumbnailsize'] = 'Taille des vignettes (sur le Frontend)';
$lang['leaveempty'] = '(laisser vide pour utiliser les vignettes depuis ImageManager)';
$lang['width'] = 'largeur';
$lang['height'] = 'hauteur';
$lang['resizemethod'] = 'M&eacute;thode de redimensionnement';
$lang['crop'] = 'recadrer';
$lang['scale'] = 'redimensionner ';
$lang['zoomcrop'] = 'zoomer &amp; recadrer';
$lang['zoomscale'] = 'zoomer &amp; redimensionner ';
$lang['maxnumber'] = 'Nombre maximal d&#039;objets par page';
$lang['showallimages'] = 'Laisser ce param&egrave;tre vide montrera toutes les images sans liens de page';
$lang['sortingoptions'] = 'Options de tri';
$lang['specifysortfields'] = 'Sp&eacute;cifier le champ sur lequel baser le tri';
$lang['ascending'] = 'croissant';
$lang['descending'] = 'd&eacute;croissant';
$lang['addfield'] = 'Ajouter un champ';
$lang['deletefield'] = 'Delete last row';
$lang['prompt_template'] = 'Source du gabarit';
$lang['prompt_templatejs'] = 'Gabarit JavaScript';
$lang['prompt_templatecss'] = 'Gabarit  CSS-feuille de style';
$lang['resetoriginal'] = 'Reset to original';
$lang['resetoriginalwarning'] = 'Are you sure you want to reset the template code to its original code?';
$lang['templateupdated'] = 'Le gabarit a &eacute;t&eacute; mis &agrave; jour avec succ&egrave;s.';
$lang['templateadded'] = 'Le gabarit a &eacute;t&eacute; ajout&eacute; avec succ&egrave;s.';
$lang['error_templatenameexists'] = 'Le nom du gabarit est d&eacute;j&agrave; attribu&eacute;.';
$lang['templatedeleted'] = 'Gabarit effac&eacute;';
$lang['availablevariables'] = 'Variable Smarty de gabarit disponibles';
$lang['availablevariableslist'] = 'Voici les variables que vous pouvez utiliser pour personnaliser vos gabarits&nbsp;:<br /><br />
<code>{$module_message}</code> - error message, is only set if there\'s a message<br />
<code>{$gallerytitle}</code> - title of the gallery. If there is no title, this will show the directory name<br />
<code>{$gallerycomment}</code> - comment of the gallery<br />
<code>{$parentlink}</code> - link to the parent folder<br />
<code>{$hideparentlink}</code> - true/false<br />
<code>{$imagecount}</code> - shows e.g. "6 images", depending on language<br />
<code>{$itemcount}</code> - number of items, images + folders<br />
<code>{$pages}</code> - number of pages<br />
<code>{$prevpage}</code> - link to previous page, if applicable<br />
<code>{$nextpage}</code> - link to next page, if applicable<br />
<code>{$pagelinks}</code> - links to each existing page<br />
<code>{$images}</code> - array with keys:<br />
 - <code>file</code> - relative path to the original image (or subgallery)<br />
 - <code>title</code> - title of the image. If there is no title, this will show the filename<br />
 - <code>comment</code> - comment to the image<br />
 - <code>filedate</code> - creation date/time<br />
 - <code>thumb</code> - relative path to the thumbnail. In case of a subgallery, this wil revert to the image which is set as default in that subgallery. If no image is set as default, a standard folder-icon will be used.<br />
 - <code>fileid</code> - unique id for the image or subgallery<br />
 - <code>isdir</code> - true if it\'s a subgallery<br />
';
$lang['options'] = 'Options ';
$lang['allowed_extensions'] = 'Extensions autoris&eacute;es';
$lang['use_comment_wysiwyg'] = 'Utiliser un &eacute;diteur visuel (WYSIWYG) dans le champ commentaire de la galerie';
$lang['optionsupdated'] = 'Les r&eacute;glagles ont &eacute;t&eacute; pris en compte.';
$lang['image'] = 'image ';
$lang['images'] = 'images ';
$lang['prevpage'] = 'pr&eacute;c&eacute;dente';
$lang['nextpage'] = 'suivante';
$lang['defaultgallerycomment'] = 'Merci d&#039;avoir install&eacute; le module Gallery. Si vous avez upload&eacute; des images dans le dossier &#039;uploads/images/Gallery&#039;  vous les verrez dessous. Vous pouvez modifier les titres, les descriptions et la taille des vignettes dans la section admin. D&eacute;couvrez toutes les autres possibilit&eacute;s dans l&#039;aide du module.';
$lang['help_dir'] = 'Param&eacute;trer pour un dossier sp&eacute;cifique, relatif &agrave; uploads/images/Gallery/';
$lang['help_template'] = 'Utiliser un gabarit diff&eacute;rent enregistr&eacute; en base de donn&eacute;es. Ce gabarit doit exister et &ecirc;tre visible dans l&#039;onglet &quot;gabarit&quot; du panneau d&#039;administration de Gallery, bien qu&#039;il ne soit pas n&eacute;cessaire d&#039;&ecirc;tre mis par d&eacute;faut. Si ce param&egrave;tre n&#039;est pas sp&eacute;cifi&eacute;, alors le gabarit qui est assign&eacute; au dossier sera utilis&eacute;, et sinon le gabarit par d&eacute;faut.';
$lang['help_number'] = 'Nombre maximal de vignettes &agrave; afficher (par page). Laisser vide affichera toutes les images';
$lang['help_start'] = 'D&eacute;buter &agrave; la &eacute;ni&eacute;me image. Laissez vide pour d&eacute;buter &agrave; la premi&egrave;re.';
$lang['help_show'] = 'Overide which images have to be shown. Possible values are:
<ul>
<li>&#039;active&#039; - to display the images marked as active (default)</li>
<li>&#039;inactive&#039; - to display only the images marked as inactive </li>
<li>&#039;all&#039; - to display all images</li>
</ul>
';
$lang['help_category'] = 'Pour afficher uniquement les images/galeries d&#039;une galeries pr&eacute;cise. Utiliser * apr&egrave;s le nom pour afficher les enfants. Vous pouvez utiliser plusieurs cat&eacute;gories en les s&eacute;parant par une virgule. Laisser vide affichera toutes les cat&eacute;gories.';
$lang['help_action'] = 'Override the default action. Use it in combination with the above parameters. Possible values are:
<ul>
<li>\'showrandom\' - to display a set of random thumb-images (applies only to the images which are stored in the database, defaults to a number of 6 images). Use \'/*\' after the directoryname in the dir parameter to include images from subdirectories</li>
<li>\'showlatest\' - to display the most recently added images (applies only to the images which are stored in the database, defaults to a number of 6 images) The dir parameter can be set, or the default Gallery-root will be used. Subdirectories are automatically included</li>
<li>\'showlatestdir\' - to display a set of random thumb-images from the most recently added directory (applies only to the images which are stored in the database, defaults to a number of 6 images)</li>
</ul>
Note that images are only stored in the database when the gallery is visited in the admin.
';
$lang['changelog'] = '<ul>
<li>Version 1.1.2  5 December 2009. Update Fancybox system to version 1.2.6, Fix for Bugreport #4422: Error when sortingoption is empty, Built in check for templates/css directory (BR #4305).</li>
<li>Version 1.1.1. 12 November 2009. Fix for Bugreport #4294: Thumbfolder not created when upgrading.</li>
<li>Version 1.1. 8 November 2009. Added sorting and drag&drop feature (FR #3907 #4210), Moved thumbnailsettings to templatestab, Make galleries searchable (FR #3956), Create backend thumbs if they don\'t exist in IM (FR #4101), Fix for BR #4027, Added fileid to image-object (FR #4259)</li>
<li>Version 1.0.3. 8 September 2009. Bugfix for [#4001], Improved synchronizing the database to files, and other minor improvements.</li>
<li>Version 1.0.2. 5 September 2009. Bugfix for [#3983] regarding spaces in directory names, Added templates Fancybox and Lytebox_slideshow as in Feature requests [#3944] and [#3967]</li>
<li>Version 1.0.1. 23 August 2009. Apply thumbnail-settings on \'Album-covers\' [#3924], Bugfix for [#3927], Added missing /div to thickbox-template, fixed an issue with generating thumbnails when no thumbs available in ImageManager, improved the naming of thumbs, Bugfix for [#3931], Prevent display the content of galleries that are set as inactive.</li>
<li>Version 1.0. 17 August 2009. Pagination parameters added, Standard templates changed accordingly, Extended the module-help, Added functions to show random or latest images, Support of pretty-urls, Various improvements</li>
<li>Version 0.6. 9 August 2009. Initial Beta Release.</li>
</ul>';
$lang['help'] = '<h3>Que fait ce module?</h3>
<p>The Gallery module is an easy to use photo gallery which automatically shows the images of a specified directory. Subdirectories will be shown as subgalleries. It has lots of features, such as automatic thumbnailing, the use of multiple Lightbox-like templates or any css/javascript template you like, and you can give titles and descriptions to your galleries as well as your photos.</p>
<h3>Comment l&#039;utiliser </h3>
<p>First, insert the module with the <code>{Gallery}</code> tag into your page or template anywhere you whish. Then upload some images with the Image Manager, File Manager (e.g. multiple images in zipfile) or FTP to the uploads/images/Gallery/ directory.</p>
<p>That\'s it!</p>
<p>If you want more photo galleries, simply create a subdirectory and upload your photos as described. By adding parameters to the <code>{Gallery}</code> tag, you can easily manipulate which subgallery will be shown in which template, e.g. <code>{Gallery dir="holidays/Netherlands2009" template="Lightbox"}</code></p>
<p>By default the thumbnails from the Image Manager are used to display the photo galleries.</p>
<h3>Options avanc&eacute;es, mais tout aussi faciles &agrave; utiliser</h3>
<p>Dans la sections admin, vous avez un tas d&#039;autres options :</p>
<ul>
<li>Set a photo as the default for a gallery, so it will show in the parent gallery as a \'cover\' in stead of the default folder-icon.</li>
<li>Give titles and descriptions to galleries.</li>
<li>Set a default template for each gallery.</li>
<li>Set thumbnail sizes for each template, with posibilities to scale, crop and/or zoom the images.</li>
<li>Specify for each template in which order the photos have to be sorted.</li>
<li>Overrule the sorting manually by dragging&dropping the photos.
<li>Give titles and descriptions to photos.</li>
<li>Switch a specific photo or gallery to inactive, preventing it from display.</li>
<li>Edit/copy templates or create new ones. Check the info-icon beneath the template-code for the available variables.</li>
</ul>
<p>In order to edit templates, the user must belong to a group with the \'Modify Templates\' permission. To edit the global Gallery options, the user must belong to a group with the \'Modify Site Preferences\' permission.</p>
<p>All titles, descriptions and settings are stored in the database. The database will synchronize with the filedirectory each time the according gallery is visited in the Gallery-admin. A little warning: when you move an image or subdirectory to another directory, you will loose its title, description and settings.</p>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Feature Request or Bug Report, please visit the Module Forge
<a href="http://dev.cmsmadesimple.org/projects/gallery/">Gallery Page</a>.</li>
<li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.</li>
<li>Lastly, you may have some success emailing the author directly.</li>
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>

<h3>Copyright and License</h3>
<p>Copyright &copy; 2009, Jos -<a href="mailto:josvd@live.nl">josvd@live.nl</a>-. All Rights Are Reserved.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>
';
$lang['utma'] = '156861353.1477216988.1255466896.1256841823.1257269172.3';
$lang['utmz'] = '156861353.1257269172.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=cmsms';
$lang['qca'] = 'P0-810403671-1255466896084';
$lang['utmb'] = '156861353.1.10.1257269172';
$lang['utmc'] = '156861353';
?>
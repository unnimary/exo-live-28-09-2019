<?php

if( ! $this->CheckPermission('Modify Templates') )
  {
    $params['errors'] = $this->Lang('accessdenied');
    $this->Redirect($id,'defaultadmin','',$params);
    return;
  }

if( !(isset($params['template'])) )
  {
    $this->_DisplayErrorPage( $id, $params, $returnid, $this->Lang('error_insufficientparams'));
    return;
  }

$template = cms_html_entity_decode($params['template']);
$this->DeleteTemplate($template);

// delete css-file
if( file_exists('../modules/Gallery/templates/css/' . $template . '.css') )
  {
    unlink('../modules/Gallery/templates/css/' . $template . '.css');
  }

// delete templateproperties
$query = "DELETE FROM " . cms_db_prefix() . "module_gallery_templateprops WHERE template=?";
$result = $db->Execute($query, array($template));

// set assigned galleries to default-template
$query = "UPDATE " . cms_db_prefix() . "module_gallery_props SET templateid=? WHERE template=?";
$result = $db->Execute($query, array(0, $template));

$params = array('tab_message'=> 'templatedeleted', 'active_tab' => 'templates');
$this->Redirect($id, 'defaultadmin', '', $params);

?>
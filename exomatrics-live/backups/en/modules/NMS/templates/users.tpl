{if isset($message)}
  {if $error != ''}
    <p><font color="red">{$message}</font></p>
  {else}
    <p>{$message}</p>
  {/if}
{/if}
{if $noform == ''}
{$startform}
<div class="pageoverflow">
  <p class="pagetext">{$prompt_userfilter}</p>
  <p class="pageinput">{$userfilter}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_listfilter}</p>
  <p class="pageinput">{$listfilter}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">{$info_listfilter}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">{$filter}</p>
</div>
{$endform}

<div class="pageoptions"><p class="pageoptions">{$itemcount}&nbsp;{$itemsfound}&nbsp;{$oftext}&nbsp;{$totalitems}</p></div>
{if $pagecount != ''}
<div class="pageoverflow">
<div class="pageshowrows">{$firstpage}&nbsp;{$prevpage}&nbsp;{$pagetext}&nbsp;{$curpage}&nbsp;{$oftext}&nbsp;{$pagecount}&nbsp;{$nextpage}&nbsp;{$lastpage}</div>
</div>
{/if}
{if $itemcount > 0}
<table border="0" cellspacing="0" cellpadding="0" class="pagetable">
 <thead>
  <tr>	
  <th>{$usertext}</th>
  <th>{$emailtext}</th>
  <th>{$nametext}</th>
  <th>{$confirmedtext}</th>
  <th>{$liststext}</th>
  <th>{$disabledtext}</th>
  <th class="pageicon">&nbsp;</th>
  <th class="pageicon">&nbsp;</th>
  </tr>
 </thead>
 <tbody>
{foreach from=$items item=entry}
  <tr class="{$entry->rowclass}">
    <td>{$entry->user}</td>
    <td>{$entry->email}</td>
    <td>{$entry->name}</td>
    <td>{$entry->confirmed}</td>
    <td>{$entry->lists}</td>
    <td>{$entry->disabled}</td>
    <td>{$entry->editlink}</td>
    <td>{$entry->deletelink}</td>
  </tr>
{/foreach}
 </tbody>
</table>
{/if}
{$createlink}&nbsp;{$importlink}&nbsp;{$feuimportlink}&nbsp;{$exportlink}&nbsp;{$bounceslink}<br/>
{/if}

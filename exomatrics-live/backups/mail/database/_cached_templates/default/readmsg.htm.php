<?php /* Smarty version 2.3.1, created on 2008-06-09 12:56:38
         compiled from default/readmsg.htm */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'escape', 'default/readmsg.htm', 6, false),
array('modifier', 'default', 'default/readmsg.htm', 65, false),
array('modifier', 'truncate', 'default/readmsg.htm', 87, false),
array('modifier', 'date_format', 'default/readmsg.htm', 91, false),
array('function', 'assign', 'default/readmsg.htm', 72, false),)); ?><?php $this->_config_load($this->_tpl_vars['umLanguageFile'], "Readmsg", 'local'); ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>UebiMiau - <?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umPageTitle'], "html"); ?>
</title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_config[0]['vars']['default_char_set']; ?>
">
</head>
<link rel="stylesheet" href="themes/default/webmail.css" type="text/css">

<script language="JavaScript" src="themes/default/webmail.js" type="text/javascript"></script>

<?php echo $this->_tpl_vars['umJS']; ?>


<body bgcolor="#778899" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">

<table cellspacing=2 cellpadding=0 border=0 align=center bgcolor=White width="760">
	<tr>
		<td valign=top width="15%">
			<table cellspacing=1 cellpadding=3 width="100%" border=0 bgcolor=White>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"> <a class="menu" href="javascript:goinbox()"><?php echo $this->_config[0]['vars']['messages_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:newmsg()"><?php echo $this->_config[0]['vars']['compose_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:folderlist()"><?php echo $this->_config[0]['vars']['folders_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:search()"><?php echo $this->_config[0]['vars']['search_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:addresses()"><?php echo $this->_config[0]['vars']['address_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:emptytrash()"><?php echo $this->_config[0]['vars']['empty_trash_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:prefs()"><?php echo $this->_config[0]['vars']['prefs_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:goend()"><?php echo $this->_config[0]['vars']['logoff_mnu']; ?>
</a></td></tr>
			</table>		
		</td>
		<td valign=top>

			<table cellspacing=1 cellpadding=2 width="100%" border=0 bgcolor=White align=center>
				<tr>

				</tr>
			</table>		
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td bgcolor=white>
						<table width="100%" border=0 cellspacing=1 cellpadding=0>
							<?php echo $this->_tpl_vars['umReplyForm']; ?>
			
							<tr>
								<td class=default colspan=2  height="18">&nbsp; 
									<?php if ($this->_tpl_vars['umHavePrevious'] == 1): ?>
									<a class="menu" href="<?php echo $this->_tpl_vars['umPreviousLink']; ?>
" title="<?php echo $this->_tpl_vars['umPreviousSubject']; ?>
"><?php echo $this->_config[0]['vars']['previous_mnu']; ?>
</a> :: 
									<?php endif; ?>
									<?php if ($this->_tpl_vars['umHaveNext'] == 1): ?>
									<a class="menu" href="<?php echo $this->_tpl_vars['umNextLink']; ?>
" title="<?php echo $this->_tpl_vars['umNextSubject']; ?>
"><?php echo $this->_config[0]['vars']['next_mnu']; ?>
</a> :: 
									<?php endif; ?>
									<a class="menu" href="javascript:goback()"><?php echo $this->_config[0]['vars']['back_mnu']; ?>
</a> :: 
									<a class="menu" href="javascript:reply()"><?php echo $this->_config[0]['vars']['reply_mnu']; ?>
</a> :: 
									<a class="menu" href="javascript:replyall()"><?php echo $this->_config[0]['vars']['reply_all_mnu']; ?>
</a> :: 
									<a class="menu" href="javascript:forward()"><?php echo $this->_config[0]['vars']['forward_mnu']; ?>
</a> :: 
									<a class="menu" href="javascript:printit()"><?php echo $this->_config[0]['vars']['print_mnu']; ?>
</a> :: 
									<a class="menu" href="javascript:headers()"><?php echo $this->_config[0]['vars']['headers_mnu']; ?>
</a>

								</td>
							</tr>

							<tr bgcolor=white>
								<td width="20%" height="18" class="headerright"><?php echo $this->_config[0]['vars']['from_hea']; ?>
 &nbsp;</td>
								<td class="default">
								<?php if (isset($this->_sections["i"])) unset($this->_sections["i"]);
$this->_sections["i"]['name'] = "i";
$this->_sections["i"]['loop'] = is_array($this->_tpl_vars['umFromList']) ? count($this->_tpl_vars['umFromList']) : max(0, (int)$this->_tpl_vars['umFromList']);
$this->_sections["i"]['show'] = true;
$this->_sections["i"]['max'] = $this->_sections["i"]['loop'];
$this->_sections["i"]['step'] = 1;
$this->_sections["i"]['start'] = $this->_sections["i"]['step'] > 0 ? 0 : $this->_sections["i"]['loop']-1;
if ($this->_sections["i"]['show']) {
    $this->_sections["i"]['total'] = $this->_sections["i"]['loop'];
    if ($this->_sections["i"]['total'] == 0)
        $this->_sections["i"]['show'] = false;
} else
    $this->_sections["i"]['total'] = 0;
if ($this->_sections["i"]['show']):

            for ($this->_sections["i"]['index'] = $this->_sections["i"]['start'], $this->_sections["i"]['iteration'] = 1;
                 $this->_sections["i"]['iteration'] <= $this->_sections["i"]['total'];
                 $this->_sections["i"]['index'] += $this->_sections["i"]['step'], $this->_sections["i"]['iteration']++):
$this->_sections["i"]['rownum'] = $this->_sections["i"]['iteration'];
$this->_sections["i"]['index_prev'] = $this->_sections["i"]['index'] - $this->_sections["i"]['step'];
$this->_sections["i"]['index_next'] = $this->_sections["i"]['index'] + $this->_sections["i"]['step'];
$this->_sections["i"]['first']      = ($this->_sections["i"]['iteration'] == 1);
$this->_sections["i"]['last']       = ($this->_sections["i"]['iteration'] == $this->_sections["i"]['total']);
?>
								 &nbsp;<a href="<?php echo $this->_tpl_vars['umFromList'][$this->_sections['i']['index']]['link']; ?>
" title="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umFromList'][$this->_sections['i']['index']]['title'], "html"); ?>
"><?php echo $this->_run_mod_handler('escape', true, $this->_run_mod_handler('default', true, $this->_tpl_vars['umFromList'][$this->_sections['i']['index']]['name'], $this->_config[0]['vars']['no_sender_text']), "html"); ?>
</a>
								<?php endfor; endif; ?>&nbsp; <a href="javascript:block_addresses()"><a href="javascript:catch_addresses()"><img src="./images/bookmark_it.gif" width="15" height="12" border="0" alt="<?php echo $this->_config[0]['vars']['catch_address']; ?>
"></a>
								</td>
							</tr>
							<tr bgcolor=white>
								<td height="18" class="headerright"><?php echo $this->_config[0]['vars']['to_hea']; ?>
 &nbsp;</td>
								<td class="default">
								<?php if (isset($this->_sections["i"])) unset($this->_sections["i"]);
$this->_sections["i"]['name'] = "i";
$this->_sections["i"]['loop'] = is_array($this->_tpl_vars['umTOList']) ? count($this->_tpl_vars['umTOList']) : max(0, (int)$this->_tpl_vars['umTOList']);
$this->_sections["i"]['show'] = true;
$this->_sections["i"]['max'] = $this->_sections["i"]['loop'];
$this->_sections["i"]['step'] = 1;
$this->_sections["i"]['start'] = $this->_sections["i"]['step'] > 0 ? 0 : $this->_sections["i"]['loop']-1;
if ($this->_sections["i"]['show']) {
    $this->_sections["i"]['total'] = $this->_sections["i"]['loop'];
    if ($this->_sections["i"]['total'] == 0)
        $this->_sections["i"]['show'] = false;
} else
    $this->_sections["i"]['total'] = 0;
if ($this->_sections["i"]['show']):

            for ($this->_sections["i"]['index'] = $this->_sections["i"]['start'], $this->_sections["i"]['iteration'] = 1;
                 $this->_sections["i"]['iteration'] <= $this->_sections["i"]['total'];
                 $this->_sections["i"]['index'] += $this->_sections["i"]['step'], $this->_sections["i"]['iteration']++):
$this->_sections["i"]['rownum'] = $this->_sections["i"]['iteration'];
$this->_sections["i"]['index_prev'] = $this->_sections["i"]['index'] - $this->_sections["i"]['step'];
$this->_sections["i"]['index_next'] = $this->_sections["i"]['index'] + $this->_sections["i"]['step'];
$this->_sections["i"]['first']      = ($this->_sections["i"]['iteration'] == 1);
$this->_sections["i"]['last']       = ($this->_sections["i"]['iteration'] == $this->_sections["i"]['total']);
?><?php if ($this->_tpl_vars['firstto'] == "no"): ?>; <?php endif; ?> &nbsp;<a href="<?php echo $this->_tpl_vars['umTOList'][$this->_sections['i']['index']]['link']; ?>
" title="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umTOList'][$this->_sections['i']['index']]['title'], "html"); ?>
"><?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umTOList'][$this->_sections['i']['index']]['name'], "html"); ?>
</a><?php $this->_plugins['function']['assign'][0](array('var' => "firstto",'value' => "no"), $this); if($this->_extract) { extract($this->_tpl_vars); $this->_extract=false; } ?><?php endfor; else: ?>&nbsp;<?php echo $this->_config[0]['vars']['no_recipient_text']; ?>
<?php endif; ?>
								</td>
							</tr>	

							<?php if ($this->_tpl_vars['umHaveCC']): ?>
								<tr bgcolor=white>
									<td height="18" class="headerright"><?php echo $this->_config[0]['vars']['cc_hea']; ?>
 &nbsp;</td>
									<td class="default">
									<?php if (isset($this->_sections["i"])) unset($this->_sections["i"]);
$this->_sections["i"]['name'] = "i";
$this->_sections["i"]['loop'] = is_array($this->_tpl_vars['umCCList']) ? count($this->_tpl_vars['umCCList']) : max(0, (int)$this->_tpl_vars['umCCList']);
$this->_sections["i"]['show'] = true;
$this->_sections["i"]['max'] = $this->_sections["i"]['loop'];
$this->_sections["i"]['step'] = 1;
$this->_sections["i"]['start'] = $this->_sections["i"]['step'] > 0 ? 0 : $this->_sections["i"]['loop']-1;
if ($this->_sections["i"]['show']) {
    $this->_sections["i"]['total'] = $this->_sections["i"]['loop'];
    if ($this->_sections["i"]['total'] == 0)
        $this->_sections["i"]['show'] = false;
} else
    $this->_sections["i"]['total'] = 0;
if ($this->_sections["i"]['show']):

            for ($this->_sections["i"]['index'] = $this->_sections["i"]['start'], $this->_sections["i"]['iteration'] = 1;
                 $this->_sections["i"]['iteration'] <= $this->_sections["i"]['total'];
                 $this->_sections["i"]['index'] += $this->_sections["i"]['step'], $this->_sections["i"]['iteration']++):
$this->_sections["i"]['rownum'] = $this->_sections["i"]['iteration'];
$this->_sections["i"]['index_prev'] = $this->_sections["i"]['index'] - $this->_sections["i"]['step'];
$this->_sections["i"]['index_next'] = $this->_sections["i"]['index'] + $this->_sections["i"]['step'];
$this->_sections["i"]['first']      = ($this->_sections["i"]['iteration'] == 1);
$this->_sections["i"]['last']       = ($this->_sections["i"]['iteration'] == $this->_sections["i"]['total']);
?><?php if ($this->_tpl_vars['firstcc'] == "no"): ?>; <?php endif; ?> <a href="<?php echo $this->_tpl_vars['umCCList'][$this->_sections['i']['index']]['link']; ?>
" title="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umCCList'][$this->_sections['i']['index']]['title'], "html"); ?>
"><?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umCCList'][$this->_sections['i']['index']]['name'], "html"); ?>
</a><?php $this->_plugins['function']['assign'][0](array('var' => "firstcc",'value' => "no"), $this); if($this->_extract) { extract($this->_tpl_vars); $this->_extract=false; } ?><?php endfor; endif; ?>
									</td>
								</tr>	
							<?php endif; ?>

							<tr bgcolor=white>
								<td height="18" class="headerright"><?php echo $this->_config[0]['vars']['subject_hea']; ?>
 &nbsp;</td>
								<td class="default"> &nbsp;<?php echo $this->_run_mod_handler('escape', true, $this->_run_mod_handler('truncate', true, $this->_run_mod_handler('default', true, $this->_tpl_vars['umSubject'], $this->_config[0]['vars']['no_subject_text']), 100, "...", true), "html"); ?>
</td>
							</tr>	
							<tr bgcolor=white>
								<td height="18" class="headerright"><?php echo $this->_config[0]['vars']['date_hea']; ?>
 &nbsp;</td>
								<td class="default"> &nbsp;<?php echo $this->_run_mod_handler('date_format', true, $this->_tpl_vars['umDate'], $this->_config[0]['vars']['date_format']); ?>
</td>
							</tr>	
							<?php if ($this->_tpl_vars['umHaveAttachments']): ?>
								<tr bgcolor=silver>
									<td class="headerright"><?php echo $this->_config[0]['vars']['attach_hea']; ?>
 &nbsp;</td>
									<td>
										<table width="100%" border=0 cellspacing=1 cellpadding=0>
											<tr bgcolor="#f1f1f1">
												<td class="headers" width="60%"> &nbsp;<b><?php echo $this->_config[0]['vars']['attch_name_hea']; ?>
</b> (<?php echo $this->_config[0]['vars']['attch_force_hea']; ?>
)</td>
												<td class="headers"> &nbsp;<b><?php echo $this->_config[0]['vars']['attch_size_hea']; ?>
</b></td>
												<td class="headers"> &nbsp;<b><?php echo $this->_config[0]['vars']['attch_type_hea']; ?>
</b></td>

												<?php if (isset($this->_sections["i"])) unset($this->_sections["i"]);
$this->_sections["i"]['name'] = "i";
$this->_sections["i"]['loop'] = is_array($this->_tpl_vars['umAttachList']) ? count($this->_tpl_vars['umAttachList']) : max(0, (int)$this->_tpl_vars['umAttachList']);
$this->_sections["i"]['show'] = true;
$this->_sections["i"]['max'] = $this->_sections["i"]['loop'];
$this->_sections["i"]['step'] = 1;
$this->_sections["i"]['start'] = $this->_sections["i"]['step'] > 0 ? 0 : $this->_sections["i"]['loop']-1;
if ($this->_sections["i"]['show']) {
    $this->_sections["i"]['total'] = $this->_sections["i"]['loop'];
    if ($this->_sections["i"]['total'] == 0)
        $this->_sections["i"]['show'] = false;
} else
    $this->_sections["i"]['total'] = 0;
if ($this->_sections["i"]['show']):

            for ($this->_sections["i"]['index'] = $this->_sections["i"]['start'], $this->_sections["i"]['iteration'] = 1;
                 $this->_sections["i"]['iteration'] <= $this->_sections["i"]['total'];
                 $this->_sections["i"]['index'] += $this->_sections["i"]['step'], $this->_sections["i"]['iteration']++):
$this->_sections["i"]['rownum'] = $this->_sections["i"]['iteration'];
$this->_sections["i"]['index_prev'] = $this->_sections["i"]['index'] - $this->_sections["i"]['step'];
$this->_sections["i"]['index_next'] = $this->_sections["i"]['index'] + $this->_sections["i"]['step'];
$this->_sections["i"]['first']      = ($this->_sections["i"]['iteration'] == 1);
$this->_sections["i"]['last']       = ($this->_sections["i"]['iteration'] == $this->_sections["i"]['total']);
?>
												<tr bgcolor=white>
													<td class="default"> &nbsp;<?php echo $this->_tpl_vars['umAttachList'][$this->_sections['i']['index']]['normlink']; ?>
<?php echo $this->_run_mod_handler('escape', true, $this->_run_mod_handler('truncate', true, $this->_tpl_vars['umAttachList'][$this->_sections['i']['index']]['name'], 30, "...", true), "html"); ?>
</a> <?php echo $this->_tpl_vars['umAttachList'][$this->_sections['i']['index']]['downlink']; ?>
<img src="./images/download.gif" width="12" height="12" border="0" alt=""></a></td>
													<td class="right"><?php echo $this->_tpl_vars['umAttachList'][$this->_sections['i']['index']]['size']; ?>
Kb &nbsp;</td>
													<td class="default"> &nbsp;<?php echo $this->_tpl_vars['umAttachList'][$this->_sections['i']['index']]['type']; ?>
</td>
												<?php endfor; endif; ?>

											</td>
										</table>
									</td>
								</tr>	
							<?php endif; ?>
							<tr>
								<td colspan=2 class="default">
									<table width="100%" border=0 cellspacing=1 cellpadding=0>
										<tr bgcolor=white>
											<td width="60%"<?php echo $this->_tpl_vars['umBackImg']; ?>
<?php echo $this->_tpl_vars['umBackColor']; ?>
><font color=black><?php echo $this->_tpl_vars['umMessageBody']; ?>
</font></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<form name=move action="process.php" method=POST>
										<?php echo $this->_tpl_vars['umDeleteForm']; ?>

										<td class=default colspan=2  height="18">
										<a class="menu" href="javascript:deletemsg()"><?php echo $this->_config[0]['vars']['delete_mnu']; ?>
</a> ::
										<a class="menu" href="javascript:movemsg()"><?php echo $this->_config[0]['vars']['move_mnu']; ?>
 </a> 
										<select name="aval_folders">
											<?php if (isset($this->_sections["i"])) unset($this->_sections["i"]);
$this->_sections["i"]['name'] = "i";
$this->_sections["i"]['loop'] = is_array($this->_tpl_vars['umAvalFolders']) ? count($this->_tpl_vars['umAvalFolders']) : max(0, (int)$this->_tpl_vars['umAvalFolders']);
$this->_sections["i"]['show'] = true;
$this->_sections["i"]['max'] = $this->_sections["i"]['loop'];
$this->_sections["i"]['step'] = 1;
$this->_sections["i"]['start'] = $this->_sections["i"]['step'] > 0 ? 0 : $this->_sections["i"]['loop']-1;
if ($this->_sections["i"]['show']) {
    $this->_sections["i"]['total'] = $this->_sections["i"]['loop'];
    if ($this->_sections["i"]['total'] == 0)
        $this->_sections["i"]['show'] = false;
} else
    $this->_sections["i"]['total'] = 0;
if ($this->_sections["i"]['show']):

            for ($this->_sections["i"]['index'] = $this->_sections["i"]['start'], $this->_sections["i"]['iteration'] = 1;
                 $this->_sections["i"]['iteration'] <= $this->_sections["i"]['total'];
                 $this->_sections["i"]['index'] += $this->_sections["i"]['step'], $this->_sections["i"]['iteration']++):
$this->_sections["i"]['rownum'] = $this->_sections["i"]['iteration'];
$this->_sections["i"]['index_prev'] = $this->_sections["i"]['index'] - $this->_sections["i"]['step'];
$this->_sections["i"]['index_next'] = $this->_sections["i"]['index'] + $this->_sections["i"]['step'];
$this->_sections["i"]['first']      = ($this->_sections["i"]['iteration'] == 1);
$this->_sections["i"]['last']       = ($this->_sections["i"]['iteration'] == $this->_sections["i"]['total']);
?>
												<option value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAvalFolders'][$this->_sections['i']['index']]['path'], "html"); ?>
"><?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umAvalFolders'][$this->_sections['i']['index']]['display'], "html"); ?>

											<?php endfor; endif; ?>
										</select>
									</td>
								</form>
							</tr>
							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
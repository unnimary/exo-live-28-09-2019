<?php

$falseimage1 = $themeObject->DisplayImage('icons/system/false.gif','make default','','','systemicon');
$trueimage1 = $themeObject->DisplayImage('icons/system/true.gif','default','','','systemicon');

$alltemplates = $this->ListTemplates();
$rowarray = array();
$rowclass = 'row1';

foreach( $alltemplates as $onetemplate )
{
	$tmp = $onetemplate;
	$row = new StdClass();
	$row->name = $this->CreateLink( $id, 'edittemplate', $returnid,
				$tmp, array('template' => $tmp, 'mode'=>'edit'));
	$row->rowclass = $rowclass;
	
	$default = ($this->GetPreference('current_template') == $tmp) ? true : false;
	if( $default )
	{
		$row->default = $trueimage1;
	}
	else
	{
		$row->default = $this->CreateLink( $id, 'switchdefaulttemplate', $returnid,
				       $falseimage1,
				       array('template'=>$tmp));
	}

	$row->editlink = $this->CreateLink( $id, 'edittemplate', $returnid,
				    $gCms->variables['admintheme']->
				    DisplayImage ('icons/system/edit.gif',
						  $this->Lang ('edit'), '', '', 'systemicon'),
				    array ('template' => $tmp, 'mode'=>'edit'));
	
	$row->copylink = $this->CreateLink( $id, 'edittemplate', $returnid,
				    $gCms->variables['admintheme']->
				    DisplayImage ('icons/system/copy.gif',
						  $this->Lang ('copy'), '', '', 'systemicon'),
				    array ('template' => $tmp, 'mode'=>'add'));
	
	$tmp = "default";
	if( ($onetemplate == $tmp) || $default )
	{
		$row->deletelink = '&nbsp;';
	}
	else
	{
		$row->deletelink = $this->CreateLink( $id, 'deletetemplate', $returnid,
					  $gCms->variables['admintheme']->
					  DisplayImage ('icons/system/delete.gif',
							$this->Lang ('delete'), '', '', 'systemicon'),
					  array ('template' => $onetemplate),
					  $this->Lang ('areyousure'));
	}

	array_push ($rowarray, $row);
	($rowclass == "row1" ? $rowclass = "row2" : $rowclass = "row1");
}

$this->smarty->assign('items', $rowarray );
$this->smarty->assign('nameprompt', $this->Lang('prompt_name'));
$this->smarty->assign('defaultprompt', $this->Lang('prompt_default'));

$this->smarty->assign('newtemplatelink',
	  $this->CreateLink( $id, 'edittemplate', $returnid,
			     $gCms->variables['admintheme']->DisplayImage('icons/system/newobject.gif', $this->Lang('prompt_newtemplate'),'','','systemicon'),
			     array('mode' => 'add',
				   'defaulttemplatepref' => 'default_template_contents'),
			     '', false, false, '').' '.

	  $this->CreateLink( $id, 'edittemplate', $returnid,
			     $this->Lang('prompt_newtemplate'),
			     array('mode' => 'add',
				   'defaulttemplatepref' => 'default_template_contents'
				   )));
$this->smarty->assign($this->CreateFormEnd());
echo $this->ProcessTemplate('admintemplates.tpl');

?>
<!-- Fancybox  -  version 1.2.6   -  http://www.fancybox.net/ -->

<div class="gallery">
{if !empty($module_message)}<h4>{$module_message|escape}</h4>{/if}
{if !empty($gallerytitle)}<h3>{$gallerytitle}</h3>{/if}
{if !empty($gallerycomment)}<div class="gallerycomment">{$gallerycomment}</div>{/if}
<p>{$imagecount}</p>
<div class="pagenavigation">
{if $pages > 1}
<div class="prevpage">{$prevpage}</div>
<div class="nextpage">{$nextpage}</div>
{/if}
{if !$hideparentlink && !empty($parentlink)}<div class="parentlink">{$parentlink}</div>{/if}
{if $pages > 1}<div class="pagelinks">{$pagelinks}</div>{/if}
</div>

{foreach from=$images item=image}
	<div class="img">
	{if $image->isdir}
		<a href="{$image->file}" title="{$image->title}"><img src="{$image->thumb}" alt="{$image->title}" /></a><br />
		{$image->title}
	{else}
   <a class="group" href="{$image->file}" title="{$image->title}" rel="gallery"><img src="{$image->thumb}" alt="{$image->title}" /></a>
	{/if}
	</div>
{/foreach}
<div class="galleryclear">&nbsp;</div>
</div>


{*----------
.gallery .img {
	height: 120px;
	/*width: 120px;   Adjust as you see fit */
	float: left;
	margin: 10px;
	text-align: center;
}

.gallery .img a {
	display: inline-block;
	border: 2px solid #ddd;
	padding: 1px;
}

.gallery .img a:hover {
	border-color: #999;
}

.gallery .pagenavigation {
	height: 50px;
}

.gallery .prevpage a, .gallery .prevpage em {
	display: block;
	width: 50px;
	height: 39px;
	float: left;
	margin: 0;
	text-indent: -1000px;
	background: url(../../images/previous.png) transparent no-repeat 0 0;
}

.gallery .nextpage a, .gallery .nextpage em {
	display: block;
	width: 50px;
	height: 39px;
	float: left;
	margin: 0 6px 0 0;
	text-indent: -1000px;
	background: url(../../images/next.png) transparent no-repeat 0 0;
}

.gallery .parentlink a {
	display: block;
	width: 50px;
	height: 39px;
	float: left;
	text-indent: -1000px;
	background: url(../../images/uppage.png) transparent no-repeat 0 0;
}

.gallery .pagenavigation a:hover {
	background-position: 0 -40px;
}

.gallery .prevpage em, .gallery .nextpage em {
	background-position: 0 -80px;
}

.gallery .pagelinks {
	float: right;
	border-right: 2px solid #666;
}

.gallery .pagelinks a, .gallery .pagelinks em {
	margin-top: 6px;
	padding: 0 6px;
	border-left: 2px solid #666;
	text-align: center;
	font: bold 11px verdana; color: #666;
}

.gallery .pagelinks em {
	color: #000;
}

.galleryclear {
	clear: both;
}


/* FANCYBOX  -  version 1.2.6 */

div#fancy_overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: none;
	z-index: 30;
}

div#fancy_loading {
	position: absolute;
	height: 40px;
	width: 40px;
	cursor: pointer;
	display: none;
	overflow: hidden;
	background: transparent;
	z-index: 100;
}

div#fancy_loading div {
	position: absolute;
	top: 0;
	left: 0;
	width: 40px;
	height: 480px;
	background: transparent url('../fancybox/fancy_progress.png') no-repeat;
}

div#fancy_outer {
	position: absolute;
	top: 0;
	left: 0;
	z-index: 90;
	padding: 20px 20px 40px 20px;
	margin: 0;
	background: transparent;
	display: none;
}

div#fancy_inner {
	position: relative;
	width:100%;
	height:100%;
	background: #000;
}

div#fancy_content {
	margin: 0;
	z-index: 100;
	position: absolute;
}

div#fancy_div {
	background: #000;
	color: #FFF;
	height: 100%;
	width: 100%;
	z-index: 100;
}

img#fancy_img {
	position: absolute;
	top: 0;
	left: 0;
	border:0;
	padding: 0;
	margin: 0;
	z-index: 100;
	width: 100%;
	height: 100%;
}

div#fancy_close {
	position: absolute;
	top: -12px;
	right: -15px;
	height: 30px;
	width: 30px;
	background: url('../fancybox/fancy_closebox.png') top left no-repeat;
	cursor: pointer;
	z-index: 181;
	display: none;
}

#fancy_frame {
	position: relative;
	width: 100%;
	height: 100%;
	display: none;
}

#fancy_ajax {
	width: 100%;
	height: 100%;
	overflow: auto;
}

a#fancy_left, a#fancy_right {
	position: absolute;
	bottom: 0px;
	height: 100%;
	width: 35%;
	cursor: pointer;
	z-index: 111;
	display: none;
	background: transparent url("data:image/gif;base64,AAAA");
	outline: none;
	overflow: hidden;
}

a#fancy_left {
	left: 0px;
}

a#fancy_right {
	right: 0px;
}

span.fancy_ico {
	position: absolute;
	top: 50%;
	margin-top: -15px;
	width: 30px;
	height: 30px;
	z-index: 112;
	cursor: pointer;
	display: block;
}

span#fancy_left_ico {
	left: -9999px;
	background: transparent url('../fancybox/fancy_left.png') no-repeat;
}

span#fancy_right_ico {
	right: -9999px;
	background: transparent url('../fancybox/fancy_right.png') no-repeat;
}

a#fancy_left:hover, a#fancy_right:hover {
	visibility: visible;
	background-color: transparent;
}

a#fancy_left:hover span {
	left: 20px;
}

a#fancy_right:hover span {
	right: 20px;
}

#fancy_bigIframe {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: transparent;
}

div#fancy_bg {
	position: absolute;
	top: 0; left: 0;
	width: 100%;
	height: 100%;
	z-index: 70;
	border: 0;
	padding: 0;
	margin: 0;
}

div.fancy_bg {
	position: absolute;
	display: block;
	z-index: 70;
	border: 0;
	padding: 0;
	margin: 0;
}

div#fancy_bg_n {
	top: -20px;
	left: 0;
	width: 100%;
	height: 20px;
	background: transparent url('../fancybox/fancy_shadow_n.png') repeat-x;
}

div#fancy_bg_ne {
	top: -20px;
	right: -20px;
	width: 20px;
	height: 20px;
	background: transparent url('../fancybox/fancy_shadow_ne.png') no-repeat;
}

div#fancy_bg_e {
	right: -20px;
	height: 100%;
	width: 20px;
	background: transparent url('../fancybox/fancy_shadow_e.png') repeat-y;
}

div#fancy_bg_se {
	bottom: -20px;
	right: -20px;
	width: 20px;
	height: 20px;
	background: transparent url('../fancybox/fancy_shadow_se.png') no-repeat;
}

div#fancy_bg_s {
	bottom: -20px;
	left: 0;
	width: 100%;
	height: 20px;
	background: transparent url('../fancybox/fancy_shadow_s.png') repeat-x;
}

div#fancy_bg_sw {
	bottom: -20px;
	left: -20px;
	width: 20px;
	height: 20px;
	background: transparent url('../fancybox/fancy_shadow_sw.png') no-repeat;
}

div#fancy_bg_w {
	left: -20px;
	height: 100%;
	width: 20px;
	background: transparent url('../fancybox/fancy_shadow_w.png') repeat-y;
}

div#fancy_bg_nw {
	top: -20px;
	left: -20px;
	width: 20px;
	height: 20px;
	background: transparent url('../fancybox/fancy_shadow_nw.png') no-repeat;
}

div#fancy_title {
	position: absolute;
	z-index: 100;
	display: none;
}

div#fancy_title div {
	color: #FFF;
	font: bold 12px Arial;
	padding-bottom: 3px;
	white-space: nowrap;
}

div#fancy_title table {
	margin: 0 auto;
}

div#fancy_title table td {
	padding: 0;
	vertical-align: middle;
}

td#fancy_title_left {
	height: 32px;
	width: 15px;
	background: transparent url('../fancybox/fancy_title_left.png') repeat-x;
}

td#fancy_title_main {
	height: 32px;
	background: transparent url('../fancybox/fancy_title_main.png') repeat-x;
}

td#fancy_title_right {
	height: 32px;
	width: 15px;
	background: transparent url('../fancybox/fancy_title_right.png') repeat-x;
}

{*----------
<script type="text/javascript" src="modules/Gallery/templates/jquery/jquery.js"></script>
<script type="text/javascript" src="modules/Gallery/templates/fancybox/jquery.fancybox.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("a.group").fancybox({
		'zoomOpacity':	true,
		'zoomSpeedIn':	300,
		'zoomSpeedOut':	300,
		'overlayShow':	true,
		'overlayOpacity':	0.5
	});
});
</script>
*}
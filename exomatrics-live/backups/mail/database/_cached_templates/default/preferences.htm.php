<?php /* Smarty version 2.3.1, created on 2008-07-31 03:00:54
         compiled from default/preferences.htm */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'escape', 'default/preferences.htm', 42, false),)); ?><?php $this->_config_load($this->_tpl_vars['umLanguageFile'], "Preferences", 'local'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>UebiMiau - <?php echo $this->_config[0]['vars']['prf_title']; ?>
</title>
	<link rel="stylesheet" href="themes/default/webmail.css" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_config[0]['vars']['default_char_set']; ?>
">

<script language="JavaScript" src="themes/default/webmail.js" type="text/javascript"></script>

<?php echo $this->_tpl_vars['umJS']; ?>


</head>

<body bgcolor="#778899" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF" onLoad="dis()">


<table cellspacing=2 cellpadding=0 border=0 align=center bgcolor=White width="760">
	<tr>
		<td valign=top width="15%">
			<table cellspacing=1 cellpadding=3 width="100%" border=0 bgcolor=White>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"> <a class="menu" href="javascript:goinbox()"><?php echo $this->_config[0]['vars']['messages_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:newmsg()"><?php echo $this->_config[0]['vars']['compose_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:folderlist()"><?php echo $this->_config[0]['vars']['folders_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:search()"><?php echo $this->_config[0]['vars']['search_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:addresses()"><?php echo $this->_config[0]['vars']['address_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:emptytrash()"><?php echo $this->_config[0]['vars']['empty_trash_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:goend()"><?php echo $this->_config[0]['vars']['logoff_mnu']; ?>
</a></td></tr>
			</table>		
		</td>
		<td valign=top>
			<table cellspacing=1 cellpadding=1 width="100%" border=0 bgcolor=White>
				<form name=form1 action=preferences.php method=POST>
				<input type=hidden name=sid value="<?php echo $this->_tpl_vars['umSid']; ?>
">
				<input type=hidden name=lid value="<?php echo $this->_tpl_vars['umLid']; ?>
">
				<input type=hidden name=tid value="<?php echo $this->_tpl_vars['umTid']; ?>
">
				<tr>
					<td colspan=2 class=headers><b><?php echo $this->_config[0]['vars']['prf_general_title']; ?>
</b></td>
				</tr>
				<tr>
					<td class=default size="20%"> &nbsp;<?php echo $this->_config[0]['vars']['prf_name']; ?>
</td>
					<td class=default><input type=text name=f_real_name size=30 value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umRealName'], "html"); ?>
" class="textbox"></td>
				</tr>
				<tr>
					<td class=default> &nbsp;<?php echo $this->_config[0]['vars']['prf_reply_to']; ?>
</td>
					<td class=default><input type=text name=f_reply_to size=30 value="<?php echo $this->_run_mod_handler('escape', true, $this->_tpl_vars['umReplyTo'], "html"); ?>
" class="textbox"></td>
				</tr>
				<tr>
					<td class=default> &nbsp;<?php echo $this->_config[0]['vars']['prf_time_zone']; ?>
</td>
					<td class=default><?php echo $this->_tpl_vars['umTimezoneSelect']; ?>
</td>
				</tr>
				<tr>
					<td class=default> &nbsp;<?php echo $this->_config[0]['vars']['prf_default_editor_mode']; ?>
</td>
					<td class=default>
					<select name="f_editor_mode">
						<option value="text"<?php if ($this->_tpl_vars['umEditorMode'] == "text"): ?> selected<?php endif; ?>><?php echo $this->_config[0]['vars']['prf_default_editor_mode_text']; ?>

						<option value="html"<?php if ($this->_tpl_vars['umEditorMode'] == "html"): ?> selected<?php endif; ?>><?php echo $this->_config[0]['vars']['prf_default_editor_mode_html']; ?>

					</select>
					</td>
				</tr>
				
				<tr>
					<td colspan=2 class=headers><b><?php echo $this->_config[0]['vars']['prf_trash_title']; ?>
</b></td>
				</tr>
				<tr>
					<td class=default colspan=2><input type=checkbox name=f_save_trash onClick="dis()" value="1"<?php echo $this->_tpl_vars['umSaveTrash']; ?>
><acronym style="cursor: hand;" OnClick="f_save_trash.click()"><?php echo $this->_config[0]['vars']['prf_save_to_trash']; ?>
 "<b><?php echo $this->_config[0]['vars']['trash_extended']; ?>
</b>"</acronym></td>
				</tr>
				<tr>
					<td class=default colspan=2><input type=checkbox name=f_st_only_read onClick="return checkDis()" value="1"<?php echo $this->_tpl_vars['umSaveTrashOnlyRead']; ?>
><acronym style="cursor: hand;" OnClick="f_st_only_read.click()"><?php echo $this->_config[0]['vars']['prf_save_only_read']; ?>
 "<b><?php echo $this->_config[0]['vars']['trash_extended']; ?>
</b>"</acronym></td>
				</tr>
				<tr>
					<td class=default colspan=2><input type=checkbox name=f_empty_on_exit value="1"<?php echo $this->_tpl_vars['umEmptyTrashOnExit']; ?>
><acronym style="cursor: hand;" OnClick="f_empty_on_exit.click()"><?php echo $this->_config[0]['vars']['prf_empty_on_exit']; ?>
</acronym></td>
				</tr>
				<tr>
					<td colspan=2  class=headers><b><?php echo $this->_config[0]['vars']['prf_sent_title']; ?>
</b></td>
				</tr>
				<tr>
					<td class=default colspan=2><input type=checkbox name=f_save_sent value="1"<?php echo $this->_tpl_vars['umSaveSent']; ?>
><acronym style="cursor: hand;" OnClick="f_save_sent.click()"><?php echo $this->_config[0]['vars']['prf_save_sent']; ?>
 "<b><?php echo $this->_config[0]['vars']['sent_extended']; ?>
</b>"</acronym></td>
				</tr>
				<tr>
					<td colspan=2  class=headers><b><?php echo $this->_config[0]['vars']['prf_messages_title']; ?>
</b></td>
				</tr>
				<tr>
					<td class=default> &nbsp;<?php echo $this->_config[0]['vars']['prf_page_limit']; ?>
</td>
					<td class=default><?php echo $this->_tpl_vars['umRecordsPerPage']; ?>
</td>
				</tr>
				<tr>
					<td class=default> &nbsp;<?php echo $this->_config[0]['vars']['prf_time_to_refesh']; ?>
</td>
					<td class=default><?php echo $this->_tpl_vars['umTimeToRefresh']; ?>
</td>
				</tr>

				<tr>
					<td colspan=2  class=default><input type=checkbox name=f_display_images value="1"<?php echo $this->_tpl_vars['umDisplayImages']; ?>
><acronym style="cursor: hand;" OnClick="f_display_images.click()"><?php echo $this->_config[0]['vars']['prf_display_images']; ?>
</acronym></td>
				</tr>

				<tr>
					<td colspan=2  class=headers><b><?php echo $this->_config[0]['vars']['prf_signature_title']; ?>
</b></td>
				</tr>
				<tr>
					<td class=default valign=top> &nbsp;<?php echo $this->_config[0]['vars']['prf_signature']; ?>
</td>
					<td class=default><?php echo $this->_tpl_vars['umSignature']; ?>
</td>
				</tr>
				<tr>
					<td class=default colspan=2><input type=checkbox name=f_add_sig value="1"<?php echo $this->_tpl_vars['umAddSignature']; ?>
><acronym style="cursor: hand;" OnClick="f_add_sig.click()"><?php echo $this->_config[0]['vars']['prf_auto_add_sign']; ?>
</acronym> </td>
				</tr>
				<tr>
					<td bgcolor=white></td>
					<td bgcolor=white><br><input type=submit value="<?php echo $this->_config[0]['vars']['prf_save_button']; ?>
" class="button"></td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</body>
</html>

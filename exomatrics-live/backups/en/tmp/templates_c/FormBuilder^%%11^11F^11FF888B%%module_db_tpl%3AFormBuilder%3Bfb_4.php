<?php /* Smarty version 2.6.25, created on 2013-07-07 04:05:23
         compiled from module_db_tpl:FormBuilder%3Bfb_4 */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'module_db_tpl:FormBuilder;fb_4', 94, false),array('function', 'eval', 'module_db_tpl:FormBuilder;fb_4', 105, false),array('modifier', 'count', 'module_db_tpl:FormBuilder;fb_4', 94, false),)), $this); ?>
<?php $this->assign('cols', '3'); ?>
<?php echo '
<script type="text/javascript">
function fbht(htid)
	{
		var fbhtc=document.getElementById(htid);
		if (fbhtc)
			{
			if (fbhtc.style.display == \'none\')
				{
				fbhtc.style.display = \'inline\';
				}
			else
				{
				fbhtc.style.display = \'none\';
				}
			}
}
</script>
'; ?>

<?php echo $this->_tpl_vars['fb_form_header']; ?>

<?php if ($this->_tpl_vars['fb_form_done'] == 1): ?>
		<?php if ($this->_tpl_vars['fb_submission_error']): ?>
		<div class="error_message"><?php echo $this->_tpl_vars['fb_submission_error']; ?>
</div>
		<?php if ($this->_tpl_vars['fb_show_submission_errors']): ?>
			<table class="error">
			<?php $_from = $this->_tpl_vars['fb_submission_error_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['thisErr']):
?>
				<tr><td align="left"><?php echo $this->_tpl_vars['thisErr']; ?>
</td></tr>
			<?php endforeach; endif; unset($_from); ?>
			</table>
		<?php endif; ?>
	<?php endif; ?>
<?php else: ?>
			<?php if ($this->_tpl_vars['fb_form_has_validation_errors']): ?>
		<div class="error_message">
		<ul>
		<?php $_from = $this->_tpl_vars['fb_form_validation_errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['thisErr']):
?>
			<li><?php echo $this->_tpl_vars['thisErr']; ?>
</li>
		<?php endforeach; endif; unset($_from); ?>
		</ul>
		</div>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['captcha_error']): ?>
		<div class="error_message"><?php echo $this->_tpl_vars['captcha_error']; ?>
</div>
	<?php endif; ?>

		<?php echo $this->_tpl_vars['fb_form_start']; ?>

	<div><?php echo $this->_tpl_vars['fb_hidden']; ?>
</div>

	<table<?php if ($this->_tpl_vars['css_class'] != ''): ?> class="<?php echo $this->_tpl_vars['css_class']; ?>
"<?php endif; ?>>
	<?php if ($this->_tpl_vars['total_pages'] > 1): ?><tr><td colspan="2"><?php echo $this->_tpl_vars['title_page_x_of_y']; ?>
</td></tr><?php endif; ?>
	<?php $_from = $this->_tpl_vars['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['entry']):
?>
		<?php if ($this->_tpl_vars['entry']->display == 1 && $this->_tpl_vars['entry']->type != '-Fieldset Start' && $this->_tpl_vars['entry']->type != '-Fieldset End'): ?>
		<tr>
			<?php echo '<td align="left" valign="top"'; ?><?php if ($this->_tpl_vars['entry']->required == 1 || $this->_tpl_vars['entry']->css_class != ''): ?><?php echo ' class="'; ?><?php if ($this->_tpl_vars['entry']->required == 1): ?><?php echo 'required'; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->required == 1 && $this->_tpl_vars['entry']->css_class != ''): ?><?php echo ' '; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->css_class != ''): ?><?php echo ''; ?><?php echo $this->_tpl_vars['entry']->css_class; ?><?php echo ''; ?><?php endif; ?><?php echo '"'; ?><?php endif; ?><?php echo '>'; ?><?php if ($this->_tpl_vars['entry']->hide_name == 0): ?><?php echo ''; ?><?php echo $this->_tpl_vars['entry']->name; ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->required_symbol != ''): ?><?php echo ''; ?><?php echo $this->_tpl_vars['entry']->required_symbol; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php endif; ?><?php echo '</td><td align="left" valign="top"'; ?><?php if ($this->_tpl_vars['entry']->css_class != ''): ?><?php echo ' class="'; ?><?php echo $this->_tpl_vars['entry']->css_class; ?><?php echo '"'; ?><?php endif; ?><?php echo '>'; ?><?php if ($this->_tpl_vars['entry']->multiple_parts == 1): ?><?php echo '<table><tr>'; ?><?php unset($this->_sections['numloop']);
$this->_sections['numloop']['name'] = 'numloop';
$this->_sections['numloop']['loop'] = is_array($_loop=$this->_tpl_vars['entry']->input) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['numloop']['show'] = true;
$this->_sections['numloop']['max'] = $this->_sections['numloop']['loop'];
$this->_sections['numloop']['step'] = 1;
$this->_sections['numloop']['start'] = $this->_sections['numloop']['step'] > 0 ? 0 : $this->_sections['numloop']['loop']-1;
if ($this->_sections['numloop']['show']) {
    $this->_sections['numloop']['total'] = $this->_sections['numloop']['loop'];
    if ($this->_sections['numloop']['total'] == 0)
        $this->_sections['numloop']['show'] = false;
} else
    $this->_sections['numloop']['total'] = 0;
if ($this->_sections['numloop']['show']):

            for ($this->_sections['numloop']['index'] = $this->_sections['numloop']['start'], $this->_sections['numloop']['iteration'] = 1;
                 $this->_sections['numloop']['iteration'] <= $this->_sections['numloop']['total'];
                 $this->_sections['numloop']['index'] += $this->_sections['numloop']['step'], $this->_sections['numloop']['iteration']++):
$this->_sections['numloop']['rownum'] = $this->_sections['numloop']['iteration'];
$this->_sections['numloop']['index_prev'] = $this->_sections['numloop']['index'] - $this->_sections['numloop']['step'];
$this->_sections['numloop']['index_next'] = $this->_sections['numloop']['index'] + $this->_sections['numloop']['step'];
$this->_sections['numloop']['first']      = ($this->_sections['numloop']['iteration'] == 1);
$this->_sections['numloop']['last']       = ($this->_sections['numloop']['iteration'] == $this->_sections['numloop']['total']);
?><?php echo '<td>'; ?><?php echo $this->_tpl_vars['entry']->input[$this->_sections['numloop']['index']]->input; ?><?php echo '&nbsp;'; ?><?php echo $this->_tpl_vars['entry']->input[$this->_sections['numloop']['index']]->name; ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->input[$this->_sections['numloop']['index']]->op): ?><?php echo '&nbsp;'; ?><?php echo $this->_tpl_vars['entry']->input[$this->_sections['numloop']['index']]->op; ?><?php echo ''; ?><?php endif; ?><?php echo '</td>'; ?><?php if (! ( $this->_sections['numloop']['rownum'] % $this->_tpl_vars['cols'] )): ?><?php echo ''; ?><?php if (! $this->_sections['numloop']['last']): ?><?php echo '</tr><tr>'; ?><?php endif; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_sections['numloop']['last']): ?><?php echo ''; ?><?php echo smarty_function_math(array('equation' => "n - a % n",'n' => $this->_tpl_vars['cols'],'a' => count($this->_tpl_vars['entry']->input),'assign' => 'cells'), $this);?><?php echo ''; ?><?php if ($this->_tpl_vars['cells'] != $this->_tpl_vars['cols']): ?><?php echo ''; ?><?php unset($this->_sections['pad']);
$this->_sections['pad']['name'] = 'pad';
$this->_sections['pad']['loop'] = is_array($_loop=$this->_tpl_vars['cells']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['pad']['show'] = true;
$this->_sections['pad']['max'] = $this->_sections['pad']['loop'];
$this->_sections['pad']['step'] = 1;
$this->_sections['pad']['start'] = $this->_sections['pad']['step'] > 0 ? 0 : $this->_sections['pad']['loop']-1;
if ($this->_sections['pad']['show']) {
    $this->_sections['pad']['total'] = $this->_sections['pad']['loop'];
    if ($this->_sections['pad']['total'] == 0)
        $this->_sections['pad']['show'] = false;
} else
    $this->_sections['pad']['total'] = 0;
if ($this->_sections['pad']['show']):

            for ($this->_sections['pad']['index'] = $this->_sections['pad']['start'], $this->_sections['pad']['iteration'] = 1;
                 $this->_sections['pad']['iteration'] <= $this->_sections['pad']['total'];
                 $this->_sections['pad']['index'] += $this->_sections['pad']['step'], $this->_sections['pad']['iteration']++):
$this->_sections['pad']['rownum'] = $this->_sections['pad']['iteration'];
$this->_sections['pad']['index_prev'] = $this->_sections['pad']['index'] - $this->_sections['pad']['step'];
$this->_sections['pad']['index_next'] = $this->_sections['pad']['index'] + $this->_sections['pad']['step'];
$this->_sections['pad']['first']      = ($this->_sections['pad']['iteration'] == 1);
$this->_sections['pad']['last']       = ($this->_sections['pad']['iteration'] == $this->_sections['pad']['total']);
?><?php echo '<td>&nbsp;</td>'; ?><?php endfor; endif; ?><?php echo ''; ?><?php endif; ?><?php echo '</tr>'; ?><?php endif; ?><?php echo ''; ?><?php endfor; endif; ?><?php echo '</table>'; ?><?php else: ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->smarty_eval == '1'): ?><?php echo ''; ?><?php echo smarty_function_eval(array('var' => $this->_tpl_vars['entry']->input), $this);?><?php echo ''; ?><?php else: ?><?php echo ''; ?><?php echo $this->_tpl_vars['entry']->input; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->valid == 0): ?><?php echo ' &lt;--- '; ?><?php echo $this->_tpl_vars['entry']->error; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['entry']->helptext != ''): ?><?php echo '&nbsp;<a href="javascript:fbht(\''; ?><?php echo $this->_tpl_vars['entry']->field_helptext_id; ?><?php echo '\')"><img src="modules/FormBuilder/images/info-small.gif" alt="Help" /></a><span id="'; ?><?php echo $this->_tpl_vars['entry']->field_helptext_id; ?><?php echo '" style="display:none">'; ?><?php echo $this->_tpl_vars['entry']->helptext; ?><?php echo '</span>'; ?><?php endif; ?><?php echo '</td></tr>'; ?>

		<?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
	<?php if ($this->_tpl_vars['has_captcha'] == 1): ?>
		<tr><td><?php echo $this->_tpl_vars['graphic_captcha']; ?>
</td><td><?php echo $this->_tpl_vars['input_captcha']; ?>
<br /><?php echo $this->_tpl_vars['title_captcha']; ?>
</td></tr>
	<?php endif; ?>
		<tr><td><?php echo $this->_tpl_vars['prev']; ?>
</td><td><?php echo $this->_tpl_vars['submit']; ?>
</td></tr>
	</table>
	<?php echo $this->_tpl_vars['fb_form_end']; ?>

<?php endif; ?>
<?php echo $this->_tpl_vars['fb_form_footer']; ?>
<?php /* Smarty version 2.6.25, created on 2013-07-09 03:40:21
         compiled from content:content_en */ ?>
<p style="text-align: justify;">Customer satisfaction is our first priority, realizing this can only be accomplished by&nbsp; conducting our business with the safety of personnel and&nbsp; protection of the environment as guiding principles. The following basic principles shall guide relationships with our customers and serve as objectives for quality service:</p>
<ul style="color:#000;">
    <li style="text-align: justify;">We pledge to be innovative and responsive offering high quality services.The philosophy and values within our business are key to achieve customer satisfaction and success.</li>
    <li style="text-align: justify;">To demonstrate our commitment to quality through active involvement of the top Management</li>
    <li style="text-align: justify;">We will perform services for our customers in conformance with established procedures and requirements for continues improvement.</li>
    <li style="text-align: justify;">We will continually seek our customers input, listening for ways to improve customer satisfaction.</li>
    <li style="text-align: justify;">We will look for innovative ways to solve our customers problems, taking advantage of our reputation for ingenuity in devising unique equipment and operational methods.</li>
    <li style="text-align: justify;">We will team with our customers to share risks, improve communications, and maximize value. <br />
    &nbsp;</li>
</ul>
<p style="text-align: justify;">&nbsp;</p>
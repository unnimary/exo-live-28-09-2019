<?php /* Smarty version 2.6.25, created on 2013-07-16 00:13:40
         compiled from module_db_tpl:NMS%3Bconfirm_email_body */ ?>

Thank you <?php echo $this->_tpl_vars['username']; ?>
 for subscribing to the mailing list <?php echo $this->_tpl_vars['listname']; ?>
.  Your registration has been confirmed.  At any time you can unsubscribe from this list by following this link <?php echo $this->_tpl_vars['unsubscribeurl']; ?>
.  You may also change your preferences by following this link: <?php echo $this->_tpl_vars['preferencesurl']; ?>
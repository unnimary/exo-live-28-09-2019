<?php /* Smarty version 2.6.25, created on 2014-06-17 05:56:17
         compiled from content:content_en */ ?>
<!-- Add code here that should appear in the content block of all new pages -->
<p>&nbsp;</p>
<p>We import bulk liquids which includes base oils, bulk slurry and petrochemicials.&nbsp;&nbsp; Our services include:</p>
<ul style="color:#000;">
    <li>Bulk liquid storage</li>
    <li>IL Blending</li>
    <li>Quality Control</li>
    <li>Inspection</li>
    <li>Transportation</li>
</ul>
<p style="text-align: justify;">Wherever your goods must be moved or stored, we can develop the most efficient and effective way to meet your requirements. Our broad portfolio of services enables us to provide optimized and streamlined supply chain services, so you can focus on your business' core capabilities and on meeting your customers' expectations.</p>
<p style="text-align: justify;">Hexomatrixx Supply Chain Solutions have developed Customer management, Distribution, Order to Cash, Supplier Management service models to support supply chains from the point of order entry to cash receipt with optimal efficiency. We provide levels of service that exceed industry norms in many key performance measures, including order accuracy, shipment accuracy, and on-time shipping.<br />
&nbsp;</p>
<!--  http://www.dolem.com/lytebox/  -->

<div class="gallery">
{if !empty($module_message)}<h4>{$module_message|escape}</h4>{/if}
{if !empty($gallerytitle)}<h3>{$gallerytitle}</h3>{/if}
{if !empty($gallerycomment)}<div class="gallerycomment">{$gallerycomment}</div>{/if}
<p>{$imagecount}</p>
<div class="pagenavigation">
{if $pages > 1}
<div class="prevpage">{$prevpage}</div>
<div class="nextpage">{$nextpage}</div>
{/if}
{if !$hideparentlink && !empty($parentlink)}<div class="parentlink">{$parentlink}</div>{/if}
{if $pages > 1}<div class="pagelinks">{$pagelinks}</div>{/if}
</div>

{foreach from=$images item=image}
	<div class="img">
	{if $image->isdir}
		<a href="{$image->file}" title="{$image->title}"><img src="{$image->thumb}" alt="{$image->title}" /></a><br />
		{$image->title}
	{else}
   <a href="{$image->file}" title="{$image->title}" rel="lyteshow[gallery]"><img src="{$image->thumb}" alt="{$image->title}" /></a>
	{/if}
	</div>
{/foreach}
<div class="galleryclear">&nbsp;</div>
</div>


{*----------
.gallery .img {
	height: 120px;
	/*width: 120px;*/
	float: left;
	margin: 10px;
	text-align: center;
}

.gallery .img a {
	display: inline-block;
	border: 2px solid #ddd;
	padding: 1px;
}

.gallery .img a:hover {
	border-color: #999;
}

.gallery .pagenavigation {
	height: 50px;
}

.gallery .prevpage a, .gallery .prevpage em {
	display: block;
	width: 50px;
	height: 39px;
	float: left;
	margin: 0;
	text-indent: -1000px;
	background: url(../../images/previous.png) transparent no-repeat 0 0;
}

.gallery .nextpage a, .gallery .nextpage em {
	display: block;
	width: 50px;
	height: 39px;
	float: left;
	margin: 0 6px 0 0;
	text-indent: -1000px;
	background: url(../../images/next.png) transparent no-repeat 0 0;
}

.gallery .parentlink a {
	display: block;
	width: 50px;
	height: 39px;
	float: left;
	text-indent: -1000px;
	background: url(../../images/uppage.png) transparent no-repeat 0 0;
}

.gallery .pagenavigation a:hover {
	background-position: 0 -40px;
}

.gallery .prevpage em, .gallery .nextpage em {
	background-position: 0 -80px;
}

.gallery .pagelinks {
	float: right;
	border-right: 2px solid #666;
}

.gallery .pagelinks a, .gallery .pagelinks em {
	margin-top: 6px;
	padding: 0 6px;
	border-left: 2px solid #666;
	text-align: center;
	font: bold 11px verdana; color: #666;
}

.gallery .pagelinks em {
	color: #000;
}

.galleryclear {
	clear: both;
}


/* LYTEBOX */

#lbOverlay { position: fixed; top: 0; left: 0; z-index: 99998; width: 100%; height: 500px; }
	#lbOverlay.grey { background-color: #000000; }
	#lbOverlay.red { background-color: #330000; }
	#lbOverlay.green { background-color: #003300; }
	#lbOverlay.blue { background-color: #011D50; }
	#lbOverlay.gold { background-color: #666600; }

#lbMain { position: absolute; left: 0; width: 100%; z-index: 99999; text-align: center; line-height: 0; }
#lbMain a img { border: none; }

#lbOuterContainer { position: relative; background-color: #fff; width: 200px; height: 200px; margin: 0 auto; }
	#lbOuterContainer.grey { border: 3px solid #888888; }
	#lbOuterContainer.red { border: 3px solid #DD0000; }
	#lbOuterContainer.green { border: 3px solid #00B000; }
	#lbOuterContainer.blue { border: 3px solid #5F89D8; }
	#lbOuterContainer.gold { border: 3px solid #B0B000; }

#lbDetailsContainer {	font: 10px Verdana, Helvetica, sans-serif; background-color: #fff; width: 100%; line-height: 1.4em;	overflow: auto; margin: 0 auto; }
	#lbDetailsContainer.grey { border: 3px solid #888888; border-top: none; }
	#lbDetailsContainer.red { border: 3px solid #DD0000; border-top: none; }
	#lbDetailsContainer.green { border: 3px solid #00B000; border-top: none; }
	#lbDetailsContainer.blue { border: 3px solid #5F89D8; border-top: none; }
	#lbDetailsContainer.gold { border: 3px solid #B0B000; border-top: none; }

#lbImageContainer, #lbIframeContainer { padding: 10px; }
#lbLoading {
	position: absolute; top: 45%; left: 0%; height: 32px; width: 100%; text-align: center; line-height: 0; background: url(../lytebox/images/loading.gif) center no-repeat;
}

#lbHoverNav { position: absolute; top: 0; left: 0; height: 100%; width: 100%; z-index: 10; }
#lbImageContainer>#lbHoverNav { left: 0; }
#lbHoverNav a { outline: none; }

#lbPrev { width: 49%; height: 100%; background: transparent url(../lytebox/images/blank.gif) no-repeat; display: block; left: 0; float: left; }
	#lbPrev.grey:hover, #lbPrev.grey:visited:hover { background: url(../lytebox/images/prev_grey.gif) left 15% no-repeat; }
	#lbPrev.red:hover, #lbPrev.red:visited:hover { background: url(../lytebox/images/prev_red.gif) left 15% no-repeat; }
	#lbPrev.green:hover, #lbPrev.green:visited:hover { background: url(../lytebox/images/prev_green.gif) left 15% no-repeat; }
	#lbPrev.blue:hover, #lbPrev.blue:visited:hover { background: url(../lytebox/images/prev_blue.gif) left 15% no-repeat; }
	#lbPrev.gold:hover, #lbPrev.gold:visited:hover { background: url(../lytebox/images/prev_gold.gif) left 15% no-repeat; }

#lbNext { width: 49%; height: 100%; background: transparent url(../lytebox/images/blank.gif) no-repeat; display: block; right: 0; float: right; }
	#lbNext.grey:hover, #lbNext.grey:visited:hover { background: url(../lytebox/images/next_grey.gif) right 15% no-repeat; }
	#lbNext.red:hover, #lbNext.red:visited:hover { background: url(../lytebox/images/next_red.gif) right 15% no-repeat; }
	#lbNext.green:hover, #lbNext.green:visited:hover { background: url(../lytebox/images/next_green.gif) right 15% no-repeat; }
	#lbNext.blue:hover, #lbNext.blue:visited:hover { background: url(../lytebox/images/next_blue.gif) right 15% no-repeat; }
	#lbNext.gold:hover, #lbNext.gold:visited:hover { background: url(../lytebox/images/next_gold.gif) right 15% no-repeat; }

#lbPrev2, #lbNext2 { text-decoration: none; font-weight: bold; }
	#lbPrev2.grey, #lbNext2.grey, #lbSpacer.grey { color: #333333; }
	#lbPrev2.red, #lbNext2.red, #lbSpacer.red { color: #620000; }
	#lbPrev2.green, #lbNext2.green, #lbSpacer.green { color: #003300; }
	#lbPrev2.blue, #lbNext2.blue, #lbSpacer.blue { color: #01379E; }
	#lbPrev2.gold, #lbNext2.gold, #lbSpacer.gold { color: #666600; }

#lbPrev2_Off, #lbNext2_Off { font-weight: bold; }
	#lbPrev2_Off.grey, #lbNext2_Off.grey { color: #CCCCCC; }
	#lbPrev2_Off.red, #lbNext2_Off.red { color: #FFCCCC; }
	#lbPrev2_Off.green, #lbNext2_Off.green { color: #82FF82; }
	#lbPrev2_Off.blue, #lbNext2_Off.blue { color: #B7CAEE; }
	#lbPrev2_Off.gold, #lbNext2_Off.gold { color: #E1E100; }

#lbDetailsData { padding: 0 10px; }
	#lbDetailsData.grey { color: #333333; }
	#lbDetailsData.red { color: #620000; }
	#lbDetailsData.green { color: #003300; }
	#lbDetailsData.blue { color: #01379E; }
	#lbDetailsData.gold { color: #666600; }

#lbDetails { width: 60%; float: left; text-align: left; }
#lbCaption { display: block; font-weight: bold; }
#lbNumberDisplay { float: left; display: block; padding-bottom: 1.0em; }
#lbNavDisplay { float: left; display: block; padding-bottom: 1.0em; }

#lbClose { width: 64px; height: 28px; float: right; margin-bottom: 1px; }
	#lbClose.grey { background: url(../lytebox/images/close_grey.png) no-repeat; }
	#lbClose.red { background: url(../lytebox/images/close_red.png) no-repeat; }
	#lbClose.green { background: url(../lytebox/images/close_green.png) no-repeat; }
	#lbClose.blue { background: url(../lytebox/images/close_blue.png) no-repeat; }
	#lbClose.gold { background: url(../lytebox/images/close_gold.png) no-repeat; }

#lbPlay { width: 64px; height: 28px; float: right; margin-bottom: 1px; }
	#lbPlay.grey { background: url(../lytebox/images/play_grey.png) no-repeat; }
	#lbPlay.red { background: url(../lytebox/images/play_red.png) no-repeat; }
	#lbPlay.green { background: url(../lytebox/images/play_green.png) no-repeat; }
	#lbPlay.blue { background: url(../lytebox/images/play_blue.png) no-repeat; }
	#lbPlay.gold { background: url(../lytebox/images/play_gold.png) no-repeat; }

#lbPause { width: 64px; height: 28px; float: right; margin-bottom: 1px; }
	#lbPause.grey { background: url(../lytebox/images/pause_grey.png) no-repeat; }
	#lbPause.red { background: url(../lytebox/images/pause_red.png) no-repeat; }
	#lbPause.green { background: url(../lytebox/images/pause_green.png) no-repeat; }
	#lbPause.blue { background: url(../lytebox/images/pause_blue.png) no-repeat; }
	#lbPause.gold { background: url(../lytebox/images/pause_gold.png) no-repeat; }

{*----------
<script type="text/javascript" src="modules/Gallery/templates/lytebox/lytebox.js"></script>
*}
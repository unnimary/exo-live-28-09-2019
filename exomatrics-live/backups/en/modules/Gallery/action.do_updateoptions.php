<?php
if (!isset($gCms)) exit;

if( !$this->CheckPermission( 'Modify Site Preferences' ) )
{
  return $this->DisplayErrorPage($id, $params, $returnid,$this->Lang('accessdenied'));
}

$this->SetPreference('allowed_extensions', $params['allowed_extensions']);
$this->SetPreference('use_comment_wysiwyg', isset($params['use_comment_wysiwyg']) ? $params['use_comment_wysiwyg'] : 0);


$params = array('tab_message'=> 'optionsupdated', 'active_tab' => 'options');
$this->Redirect($id, 'defaultadmin', '', $params);
?>

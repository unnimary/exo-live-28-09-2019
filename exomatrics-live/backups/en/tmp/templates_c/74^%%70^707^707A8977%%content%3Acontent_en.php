<?php /* Smarty version 2.6.25, created on 2013-07-08 07:09:52
         compiled from content:content_en */ ?>
<p style="text-align: justify;"><strong>Mission, Vision &amp; Values </strong></p>
<p style="text-align: justify;">We believe the strength of our Company is not only within the functionality and quality of our innovation, but most importantly within the accumulated knowledge and skill in our professionalism. We pledge to be innovative and responsive, while offering high quality services at a competitive price. The philosophy and values within our business is to achieve customer satisfaction and success.</p>
<p style="text-align: justify;"><strong>Our Vision:</strong></p>
<p style="text-align: justify;">To be acknowledged as the leading logistics and industry specific supply chain service provider in the Middle East with innovative business solutions, nurturing long-term partnerships with clients and suppliers.</p>
<p style="text-align: justify;"><strong>Our Mission: </strong></p>
<p style="text-align: justify;">To respond  to industry’s needs on supply chain logistics services  &amp; product support  performing simple or complex services with total focus  on customer satisfaction and quality .<strong></strong></p>
<p style="text-align: justify;">To harness the intellectual talents of our staff in developing, deploying and rewarding for constructive innovations which will enhance the ultimate business performance and success.</p>
<p style="text-align: justify;"><strong>Our Values: </strong></p>
<ul style="color:#000;">
<li style="text-align: justify;">We will manage a relationship with our clients based on professionalism, honesty, integrity, and respect for their employees and culture.</li>
<li style="text-align: justify;">We will insure that all operational and business solutions are first and foremost strategically sound.</li>
<li style="text-align: justify;">We will deliver practical consulting advice and creative solutions that fit the company's financial situation and resources.</li>
<li style="text-align: justify;">We will strive to structure consulting projects so that they are self-funding and promptly deliver measurable value.</li>
</ul>
<p style="text-align: justify;"> </p>
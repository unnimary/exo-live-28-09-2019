<div class="gallery">
{if !empty($module_message)}<h4>{$module_message|escape}</h4>{/if}
{if !empty($gallerytitle)}<h3>{$gallerytitle}</h3>{/if}
{if !empty($gallerycomment)}<div class="gallerycomment">{$gallerycomment}</div>{/if}
<p>{$imagecount}</p>
<div class="pagenavigation">
{if $pages > 1}
<div class="prevpage">{$prevpage}</div>
<div class="nextpage">{$nextpage}</div>
{/if}
{if !$hideparentlink && !empty($parentlink)}<div class="parentlink">{$parentlink}</div>{/if}
{if $pages > 1}<div class="pagelinks">{$pagelinks}</div>{/if}
</div>

{foreach from=$images item=image}
	<div class="img">
	{if $image->isdir}
		<a href="{$image->file}" title="{$image->title}"><img src="{$image->thumb}" alt="{$image->title}" /></a><br />
		{$image->title}
	{else}
   <a href="{$image->file}" title="{$image->title}" class="thickbox" rel="gallery"><img src="{$image->thumb}" alt="{$image->title}" /></a>
	{/if}
	</div>
{/foreach}
<div class="galleryclear">&nbsp;</div>
</div>


{*----------
.gallery .img {
	height: 120px;
	/*width: 120px;   Adjust as you see fit */
	float: left;
	margin: 10px;
	text-align: center;
}

.gallery .img a {
	display: inline-block;
	border: 2px solid #ddd;
	padding: 1px;
}

.gallery .img a:hover {
	border-color: #999;
}

.gallery .pagenavigation {
	height: 50px;
}

.gallery .prevpage a, .gallery .prevpage em {
	display: block; 
	width: 50px; 
	height: 39px;
	float: left; 
	margin: 0;
	text-indent: -1000px;
	background: url(../../images/previous.png) transparent no-repeat 0 0;
}

.gallery .nextpage a, .gallery .nextpage em {
	display: block; 
	width: 50px; 
	height: 39px;
	float: left; 
	margin: 0 6px 0 0;
	text-indent: -1000px;
	background: url(../../images/next.png) transparent no-repeat 0 0;
}

.gallery .parentlink a {
	display: block; 
	width: 50px; 
	height: 39px;
	float: left; 
	text-indent: -1000px;
	background: url(../../images/uppage.png) transparent no-repeat 0 0;
}

.gallery .pagenavigation a:hover {
	background-position: 0 -40px;
}

.gallery .prevpage em, .gallery .nextpage em {
	background-position: 0 -80px;
}

.gallery .pagelinks {
	float: right;
	border-right: 2px solid #666;
}

.gallery .pagelinks a, .gallery .pagelinks em {
	margin-top: 6px;
	padding: 0 6px;
	border-left: 2px solid #666;
	text-align: center;
	font: bold 11px verdana; color: #666;
}

.gallery .pagelinks em {
	color: #000;
}

.galleryclear {
	clear: both;
}


/* THICKBOX */

/* ----------------------------------------------------------------------------------------------------------------*/
/* ---------->>> global settings needed for thickbox <<<-----------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------*/
*{padding: 0; margin: 0;}

/* ----------------------------------------------------------------------------------------------------------------*/
/* ---------->>> thickbox specific link and font settings <<<------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------*/
#TB_window {
	font: 12px Arial, Helvetica, sans-serif;
	color: #333333;
}

#TB_secondLine {
	font: 10px Arial, Helvetica, sans-serif;
	color:#666666;
}

#TB_window a:link {color: #666666;}
#TB_window a:visited {color: #666666;}
#TB_window a:hover {color: #000;}
#TB_window a:active {color: #666666;}
#TB_window a:focus{color: #666666;}

/* ----------------------------------------------------------------------------------------------------------------*/
/* ---------->>> thickbox settings <<<-----------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------*/
#TB_overlay {
	position: fixed;
	z-index:100;
	top: 0px;
	left: 0px;
	height:100%;
	width:100%;
}

.TB_overlayMacFFBGHack {background: url(modules/Gallery/templates/thickbox/macFFBgHack.png) repeat;}
.TB_overlayBG {
	background-color:#000;
	filter:alpha(opacity=75);
	-moz-opacity: 0.75;
	opacity: 0.75;
}

* html #TB_overlay { /* ie6 hack */
     position: absolute;
     height: expression(document.body.scrollHeight > document.body.offsetHeight ? document.body.scrollHeight : document.body.offsetHeight + 'px');
}

#TB_window {
	position: fixed;
	background: #ffffff;
	z-index: 102;
	color:#000000;
	display:none;
	border: 4px solid #525252;
	text-align:left;
	top:50%;
	left:50%;
}

* html #TB_window { /* ie6 hack */
position: absolute;
margin-top: expression(0 - parseInt(this.offsetHeight / 2) + (TBWindowMargin = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop) + 'px');
}

#TB_window img#TB_Image {
	display:block;
	margin: 15px 0 0 15px;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-top: 1px solid #666;
	border-left: 1px solid #666;
}

#TB_caption{
	height:25px;
	padding:7px 30px 10px 25px;
	float:left;
}

#TB_closeWindow{
	height:25px;
	padding:11px 25px 10px 0;
	float:right;
}

#TB_closeAjaxWindow{
	padding:7px 10px 5px 0;
	margin-bottom:1px;
	text-align:right;
	float:right;
}

#TB_ajaxWindowTitle{
	float:left;
	padding:7px 0 5px 10px;
	margin-bottom:1px;
}

#TB_title{
	background-color:#e8e8e8;
	height:27px;
}

#TB_ajaxContent{
	clear:both;
	padding:2px 15px 15px 15px;
	overflow:auto;
	text-align:left;
	line-height:1.4em;
}

#TB_ajaxContent.TB_modal{
	padding:15px;
}

#TB_ajaxContent p{
	padding:5px 0px 5px 0px;
}

#TB_load{
	position: fixed;
	display:none;
	height:13px;
	width:208px;
	z-index:103;
	top: 50%;
	left: 50%;
	margin: -6px 0 0 -104px; /* -height/2 0 0 -width/2 */
}

* html #TB_load { /* ie6 hack */
position: absolute;
margin-top: expression(0 - parseInt(this.offsetHeight / 2) + (TBWindowMargin = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop) + 'px');
}

#TB_HideSelect{
	z-index:99;
	position:fixed;
	top: 0;
	left: 0;
	background-color:#fff;
	border:none;
	filter:alpha(opacity=0);
	-moz-opacity: 0;
	opacity: 0;
	height:100%;
	width:100%;
}

* html #TB_HideSelect { /* ie6 hack */
     position: absolute;
     height: expression(document.body.scrollHeight > document.body.offsetHeight ? document.body.scrollHeight : document.body.offsetHeight + 'px');
}

#TB_iframeContent{
	clear:both;
	border:none;
	margin-bottom:-1px;
	margin-top:1px;
	_margin-bottom:1px;
}

{*----------
<script type="text/javascript" src="modules/Gallery/templates/jquery/jquery.js"></script>
<script type="text/javascript" src="modules/Gallery/templates/thickbox/thickbox.js"></script>
*}
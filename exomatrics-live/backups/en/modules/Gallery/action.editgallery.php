<?php

if( !$gCms ) exit();

if( !$this->CheckPermission('Use Gallery') ) 
{
	echo $this->Lang('accessdenied');
	return;
}

$themeObject =& $gCms->variables['admintheme'];


// check if we have a gallery name
if( !isset($params['gid']) )
{
	$params['errors'] = $this->Lang('error_insufficientparams');
	$this->Redirect($id,'defaultadmin','',$params);
	return;
}
if( !isset($params['mode']) )
{
	$params['errors'] = $this->Lang('error_insufficientparams');
	$this->Redirect($id,'defaultadmin','',$params);
	return;
}

$params['origaction'] = $params['action'];

$galleryinfo = $this->_Getgalleryinfobyid($params['gid']);
if ( $galleryinfo['templateid'] == 0 )
{
	// override template settings with default template
	$templateprops = $this->_GetTemplateprops($this->GetPreference('current_template'));
	$galleryinfo['thumbwidth'] = $templateprops['thumbwidth'];
	$galleryinfo['sortitems'] = $templateprops['sortitems'];
}
$gallerypath = $params['gid'] == 1 ? '' : trim($galleryinfo['filepath'] . '/' . $galleryinfo['filename'],'/');

$contents = "";
if( $params['mode'] == 'add' )
{
	$this->smarty->assign('formstart', $this->CreateFormStart ($id, 'do_addgallery',$returnid,'post','', false, '', $params));
	$this->smarty->assign('galleryname', $this->CreateInputText( $id, 'gallery', "", 40 ));
}
else 
{
	$this->_UpdateGalleryDB($gallerypath,$params['gid']);
	$gallery = $this->_Getgalleryfiles($gallerypath);

	$showgallery = array();
	$trueimage = $themeObject->DisplayImage('icons/system/true.gif', $this->Lang('setfalse'),'','','systemicon');
	$trueimage2 = $themeObject->DisplayImage('icons/system/true.gif', $this->Lang('default'),'','','systemicon');
	$falseimage = $themeObject->DisplayImage('icons/system/false.gif', $this->Lang('settrue'),'','','systemicon');

	foreach ($gallery as $file)
	{
		$onerow = new stdClass();

		$params['fid'] = $file['fileid'];

		$onerow->fileid = $file['fileid'];
		$onerow->fileorder = $file['fileorder'];
		$onerow->file = $file['filename'];

		if ( substr($file['filename'],-1) == "/" )
		{
			// record is a directory
			$onerow->thumb = '<img src="../modules/Gallery/images/foldersmall.png" alt="' . $file['filename'] . '" />';
			$onerow->thumburl = '../modules/Gallery/images/foldersmall.png';
			$onerow->filename = '';
			$onerow->title = $this->CreateLink($id, 'editgallery', $returnid, $file['filename'], array('gid'=>$file['fileid'],'mode'=>"edit")) . $this->CreateInputHidden($id, 'filetitle[' . $file['fileid'] . ']', '#dir');
			$onerow->comment = "";
			$onerow->filedate = "";
			$onerow->defaultlink = "";
			$onerow->isdir = 1;
		}
		else
		{
			$onerow->thumb = '<img src="../' . DEFAULT_GALLERY_PATH . $file['filepath'] . '/' . IM_PREFIX . $file['filename'] . '" alt="' . $file['filename'] . '" />';
			$onerow->thumburl = '../' . DEFAULT_GALLERY_PATH . str_replace('%2F','/',rawurlencode($file['filepath'] . '/' . IM_PREFIX . $file['filename']));
			$onerow->filename = $file['filename'];
			$onerow->title = $this->CreateInputText($id, 'filetitle[' . $file['fileid'] . ']', $file['title'], 30, 100);
			$onerow->comment = $this->CreateTextArea(0, $id, $file['comment'], 'filecomment[' . $file['fileid'] . ']', 'fake" style="width:400px; height:4em;', '', '', '', '40', '4');  // class filled with fake and style-info to overrule the theme-css
			$onerow->filedate = $file['filedate'];
			if( $file['fileid'] == $galleryinfo['defaultfile'] )
			{
				$onerow->defaultlink = $trueimage2;
			}
			else
			{
				$onerow->defaultlink = $this->CreateLink($id, 'switchdefault', $returnid, $falseimage, $params);
			}
			$onerow->isdir = 0;
			if ( !file_exists('../' . DEFAULT_GALLERY_PATH . $file['filepath'] . '/' . IM_PREFIX . $file['filename']) )
			{
				$this->_CreateThumbnail('../' . DEFAULT_GALLERY_PATH . $file['filepath'] . '/' . IM_PREFIX . $file['filename'],
										'../' . DEFAULT_GALLERY_PATH . $file['filepath'] . '/' . $file['filename'],
										96, 96, 'sc');
			}
		}

		if ( $file['active'] )
		{
			$activeimage = $trueimage;
		}
		else
		{
			$activeimage = $falseimage;
		}
		$onerow->activelink = $this->CreateLink($id, 'switchactive', $returnid, $activeimage, $params);

		if ( $file['fileid'] != 1 )
		{
			array_push($showgallery, $onerow);
		}
	}

	$sortarray = explode('/','n+fileorder/'.$galleryinfo['sortitems']);
	$showgallery = $this->_ArraySort($showgallery, $sortarray, false);

	$this->smarty->assign_by_ref('items', $showgallery);
	$this->smarty->assign('itemcount', count($showgallery));
	$this->smarty->assign('item', $this->Lang('item'));
	$this->smarty->assign('title', $this->Lang('title'));
	$this->smarty->assign('comment', $this->Lang('comment'));
	$this->smarty->assign('filedate', $this->Lang('date'));
	$this->smarty->assign('active', $this->Lang('active'));
	$this->smarty->assign('default', $this->Lang('default'));
	$this->smarty->assign('nofilestext', $this->lang("nofilestext"));
	$this->smarty->assign('formstart', $this->CreateFormStart ($id, 'do_editgallery',$returnid,'post','', false, '', $params));
}

$this->smarty->assign('pagetitle',$gallerypath);
$this->smarty->assign('prompt_gallerytitle',$this->Lang('gallerytitle'));
$this->smarty->assign('gallerytitle', $this->CreateInputText( $id, 'gallerytitle', $galleryinfo['title'], 40, 100 ));
$this->smarty->assign('prompt_comment',$this->Lang('comment'));
$this->smarty->assign('gallerycomment', $this->CreateTextArea($this->GetPreference('use_comment_wysiwyg',1), $id, $galleryinfo['comment'], 'gallerycomment', 'fake" style="height:6em;', '', '', '', '80', '3'));

$templatelist = array('- ' . $this->Lang('usedefault') . ' -'=>0);
$query = "SELECT templateid, template FROM ".cms_db_prefix()."module_gallery_templateprops ORDER BY template ASC";
$result = $db->Execute($query);
while ($result && $row = $result->FetchRow())
{
	$templatelist[$row['template']] = $row['templateid'];
}
$this->smarty->assign('prompt_template', $this->Lang('template'));
$this->smarty->assign('template', $this->CreateInputDropdown($id, 'templateid', $templatelist, -1, $galleryinfo['templateid']));

$smarty->assign('prompt_hideparentlink',$this->Lang('hideparentlink'));
$smarty->assign('hideparentlink', $this->CreateInputCheckbox($id, 'hideparentlink', '1', $galleryinfo['hideparentlink'], $params['gid'] == 1 ? 'disabled="disabled' : ''));

//$this->smarty->assign('prompt_createthumbs', (!empty($galleryinfo['thumbwidth']) ? $this->Lang('createthumbs') : ''));
//$this->smarty->assign('createthumbs', (!empty($galleryinfo['thumbwidth']) ? $this->CreateLink($id, 'createthumbs', $returnid, '<img src="../modules/Gallery/images/fpaint.gif" alt="' . $this->Lang('createthumbs') . '"/>', array('gid'=>$params['gid']), '', false, false) : ''));

$this->smarty->assign('sortbysettings',$this->CreateLink($id, 'do_editgallery', $returnid, $this->Lang('sortbysettings'), array('gid'=>$params['gid'],'mode'=>'unsort')));

$this->smarty->assign('hidden',$this->CreateInputHidden($id, 'sort', '', 'id="sort"'));

$this->smarty->assign('submit',$this->CreateInputSubmit ($id, 'submitbutton', $this->Lang ('submit')));
$this->smarty->assign('cancel',$this->CreateInputSubmit ($id, 'cancel', $this->Lang ('cancel')));
$this->smarty->assign('formend',$this->CreateFormEnd());
echo $this->ProcessTemplate('editgallery.tpl');

?>
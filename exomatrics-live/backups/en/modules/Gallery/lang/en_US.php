<?php
$lang['friendlyname'] = 'Gallery';
$lang['moddescription'] = 'The easiest way to manage and display photo galleries';
$lang['description'] = 'An easy to use gallery which automatically shows the images of a specified directory.';
$lang['postinstall'] = 'The Gallery module was successfully installed.';
$lang['installed'] = 'The Gallery module version %s was installed.';
$lang['upgraded'] = 'The Gallery module is upgraded to version %s.';
$lang['postuninstall'] = 'The Gallery  module was uninstalled';
$lang['uninstalled'] = 'The Gallery module was uninstalled';
$lang['really_uninstall'] = 'Are you sure you want to uninstall Gallery? This does not effect the images, but all comment-data and thumbnails that were created by Gallery will be lost.';


$lang['accessdenied'] = 'Access Denied. Please check your permissions.';
$lang['actions'] = 'Actions';
$lang['active'] = 'Active';
$lang['apply'] = 'Apply';
$lang['areyousure'] = 'Are you sure you want to delete?';
$lang['cancel'] = 'Cancel';
$lang['copy'] = 'Copy';
$lang['default'] = 'Default';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['error'] = 'Error!';
$lang['setfalse']= 'Set False';
$lang['settrue']= 'Set True';
$lang['submit'] = 'Save';


$lang['galleries'] = 'Galleries';
$lang['gallerypath'] = 'Gallery';
$lang['editgallery'] = 'Edit gallery';
$lang['addgallery'] = 'Add gallery';
$lang['nogalleriestext'] = 'No galleries available';
$lang['item'] = 'Image';
$lang['title'] = 'Title';
$lang['date'] = 'Date';
$lang['nofilestext'] = 'No images available';
$lang['gallerytitle'] = 'Gallery Title';
$lang['comment'] = 'Comment';
$lang['template'] = 'Template';
$lang['hideparentlink'] = 'Hide link to parent gallery';
$lang['usedefault'] = 'use default';
//$lang['createthumbs'] = '(Re)Create Thumbs';
//$lang['thumbscreated'] = 'Thumbnails created';
$lang['sortbysettings'] = 'Sort by template settings';
$lang['galleryupdated'] = 'The gallery was successfully updated.';
$lang['error_updategalleryfailed'] = 'Updating the gallery failed.';


$lang['templates'] = 'Templates';
$lang['title_available_templates'] = 'Available Templates';
$lang['prompt_name'] = 'Name';
$lang['prompt_default'] = 'Default';
$lang['prompt_newtemplate'] = 'Create a new template';
$lang['title_template'] = 'Template Editor';
$lang['prompt_templatename'] = 'Template Name';
$lang['thumbnailsize'] = 'Thumbnail Size (Frontend)';
$lang['leaveempty'] = '(leave this empty to use the default thumbnails from ImageManager)';
$lang['width'] = 'width';
$lang['height'] = 'height';
$lang['resizemethod'] = 'resize method';
$lang['crop'] = 'crop';
$lang['scale'] = 'scale';
$lang['zoomcrop'] = 'zoom &amp; crop';
$lang['zoomscale'] = 'zoom &amp; scale';
$lang['maxnumber'] = 'Maximum number of items per page';
$lang['showallimages'] = 'Leaving empty will show all images without pagelinks';
$lang['sortingoptions'] = 'Sorting Options';
$lang['specifysortfields'] = 'Specify the field(s) to sort on';
$lang['ascending'] = 'ascending';
$lang['descending'] = 'descending';
$lang['addfield'] = 'Add field';
$lang['deletefield'] = 'Delete last row';
$lang['prompt_template'] = 'Template Source';
$lang['prompt_templatejs'] = 'Template JavaScript';
$lang['prompt_templatecss'] = 'Template CSS-stylesheet';
$lang['resetoriginal'] = 'Reset to original';
$lang['resetoriginalwarning'] = 'Are you sure you want to reset the template code to its original code?';
$lang['templateupdated'] = 'The template was successfully updated.';
$lang['templateadded'] = 'The template was successfully added.';
$lang['error_templatenameexists'] = 'The template name already exists.';
$lang['templatedeleted'] = 'Template deleted';
$lang['availablevariables'] = 'Available Template Smarty Variables';
$lang['availablevariableslist'] = 'These are the variables you can use to customize your template:<br /><br />
<code>{$module_message}</code> - error message, is only set if there\'s a message<br />
<code>{$gallerytitle}</code> - title of the gallery. If there is no title, this will show the directory name<br />
<code>{$gallerycomment}</code> - comment of the gallery<br />
<code>{$parentlink}</code> - link to the parent folder<br />
<code>{$hideparentlink}</code> - true/false<br />
<code>{$imagecount}</code> - shows e.g. "6 images", depending on language<br />
<code>{$itemcount}</code> - number of items, images + folders<br />
<code>{$pages}</code> - number of pages<br />
<code>{$prevpage}</code> - link to previous page, if applicable<br />
<code>{$nextpage}</code> - link to next page, if applicable<br />
<code>{$pagelinks}</code> - links to each existing page<br />
<code>{$images}</code> - array with keys:<br />
 - <code>file</code> - relative path to the original image (or subgallery)<br />
 - <code>title</code> - title of the image. If there is no title, this will show the filename<br />
 - <code>comment</code> - comment to the image<br />
 - <code>filedate</code> - creation date/time<br />
 - <code>thumb</code> - relative path to the thumbnail. In case of a subgallery, this wil revert to the image which is set as default in that subgallery. If no image is set as default, a standard folder-icon will be used.<br />
 - <code>fileid</code> - unique id for the image or subgallery<br />
 - <code>isdir</code> - true if it\'s a subgallery<br />
';


$lang['options'] = 'Options';
$lang['allowed_extensions'] = 'Allowed Extensions';
$lang['use_comment_wysiwyg'] = 'Use a WYSIWYG editor on the Gallery comment field';
$lang['optionsupdated'] = 'The options were successfully updated.';


$lang['image'] = 'image';
$lang['images'] = 'images';
$lang['prevpage'] = 'previous';
$lang['nextpage'] = 'next';
$lang['defaultgallerycomment'] = 'Thank you for installing the Gallery module. If you have uploaded some images to the \'uploads/images/Gallery\' folder, you will see them below. You can edit titles, descriptions and thumbnail sizes in the admin section. Check out all the other features this module offers in the Module Help.';


$lang['help_dir'] = 'Parameter to specify a directory, relative to uploads/images/Gallery/';
$lang['help_template'] = 'Use a separate database template for displaying the photo gallery. This template must exist and be visible in the template tab of the Gallery admin, though it does not need to be the default. If this parameter is not specified, then the template which is assigned to the directory will be used, else the default template.';
$lang['help_number'] = 'Maximum number of imagethumbs to display (per page). Leaving empty will show all images.';
$lang['help_start'] = 'Start at the nth image. Leaving empty will start at the first image.';
$lang['help_show'] = 'Overide which images have to be shown. Possible values are:
<ul>
<li>\'active\' - to display the images marked as active (default)</li>
<li>\'inactive\' - to display only the images marked as inactive </li>
<li>\'all\' - to display all images</li>
</ul>
';
$lang['help_category'] = 'To display only images/galleries assigned to the specified categories. Use * after the name to show children.  Multiple categories can be used if separated with a comma. Leaving empty, will show all categories.';
$lang['help_action'] = 'Override the default action. Use it in combination with the above parameters. Possible values are:
<ul>
<li>\'showrandom\' - to display a set of random thumb-images (applies only to the images which are stored in the database, defaults to a number of 6 images). Use \'/*\' after the directoryname in the dir parameter to include images from subdirectories</li>
<li>\'showlatest\' - to display the most recently added images (applies only to the images which are stored in the database, defaults to a number of 6 images) The dir parameter can be set, or the default Gallery-root will be used. Subdirectories are automatically included</li>
<li>\'showlatestdir\' - to display a set of random thumb-images from the most recently added directory (applies only to the images which are stored in the database, defaults to a number of 6 images)</li>
</ul>
Note that images are only stored in the database when the gallery is visited in the admin.
';

$lang['changelog'] = '<ul>
<li>Version 1.1.2  5 December 2009. Update Fancybox system to version 1.2.6, Fix for Bugreport #4422: Error when sortingoption is empty, Built in check for templates/css directory (BR #4305).</li>
<li>Version 1.1.1. 12 November 2009. Fix for Bugreport #4294: Thumbfolder not created when upgrading.</li>
<li>Version 1.1. 8 November 2009. Added sorting and drag&drop feature (FR #3907 #4210), Moved thumbnailsettings to templatestab, Make galleries searchable (FR #3956), Create backend thumbs if they don\'t exist in IM (FR #4101), Fix for BR #4027, Added fileid to image-object (FR #4259)</li>
<li>Version 1.0.3. 8 September 2009. Bugfix for [#4001], Improved synchronizing the database to files, and other minor improvements.</li>
<li>Version 1.0.2. 5 September 2009. Bugfix for [#3983] regarding spaces in directory names, Added templates Fancybox and Lytebox_slideshow as in Feature requests [#3944] and [#3967]</li>
<li>Version 1.0.1. 23 August 2009. Apply thumbnail-settings on \'Album-covers\' [#3924], Bugfix for [#3927], Added missing /div to thickbox-template, fixed an issue with generating thumbnails when no thumbs available in ImageManager, improved the naming of thumbs, Bugfix for [#3931], Prevent display the content of galleries that are set as inactive.</li>
<li>Version 1.0. 17 August 2009. Pagination parameters added, Standard templates changed accordingly, Extended the module-help, Added functions to show random or latest images, Support of pretty-urls, Various improvements</li>
<li>Version 0.6. 9 August 2009. Initial Beta Release.</li>
</ul>';
$lang['help'] = '<h3>What Does This Do?</h3>
<p>The Gallery module is an easy to use photo gallery which automatically shows the images of a specified directory. Subdirectories will be shown as subgalleries. It has lots of features, such as automatic thumbnailing, the use of multiple Lightbox-like templates or any css/javascript template you like, and you can give titles and descriptions to your galleries as well as your photos.</p>
<h3>How Do I Use It</h3>
<p>First, insert the module with the <code>{Gallery}</code> tag into your page or template anywhere you whish. Then upload some images with the Image Manager, File Manager (e.g. multiple images in zipfile) or FTP to the uploads/images/Gallery/ directory.</p>
<p>That\'s it!</p>
<p>If you want more photo galleries, simply create a subdirectory and upload your photos as described. By adding parameters to the <code>{Gallery}</code> tag, you can easily manipulate which subgallery will be shown in which template, e.g. <code>{Gallery dir="holidays/Netherlands2009" template="Lightbox"}</code></p>
<p>By default the thumbnails from the Image Manager are used to display the photo galleries.</p>
<h3>Advanced Options, but still easy to use</h3>
<p>In the admin section you have lots of other options:</p>
<ul>
<li>Set a photo as the default for a gallery, so it will show in the parent gallery as a \'cover\' in stead of the default folder-icon.</li>
<li>Give titles and descriptions to galleries.</li>
<li>Set a default template for each gallery.</li>
<li>Set thumbnail sizes for each template, with posibilities to scale, crop and/or zoom the images.</li>
<li>Specify for each template in which order the photos have to be sorted.</li>
<li>Overrule the sorting manually by dragging&dropping the photos.
<li>Give titles and descriptions to photos.</li>
<li>Switch a specific photo or gallery to inactive, preventing it from display.</li>
<li>Edit/copy templates or create new ones. Check the info-icon beneath the template-code for the available variables.</li>
</ul>
<p>In order to edit templates, the user must belong to a group with the \'Modify Templates\' permission. To edit the global Gallery options, the user must belong to a group with the \'Modify Site Preferences\' permission.</p>
<p>All titles, descriptions and settings are stored in the database. The database will synchronize with the filedirectory each time the according gallery is visited in the Gallery-admin. A little warning: when you move an image or subdirectory to another directory, you will loose its title, description and settings.</p>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Feature Request or Bug Report, please visit the Module Forge
<a href="http://dev.cmsmadesimple.org/projects/gallery/">Gallery Page</a>.</li>
<li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.</li>
<li>Lastly, you may have some success emailing the author directly.</li>
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>

<h3>Copyright and License</h3>
<p>Copyright &copy; 2009, Jos -<a href="mailto:josvd@live.nl">josvd@live.nl</a>-. All Rights Are Reserved.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>
';
?>
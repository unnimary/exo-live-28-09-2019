<?php

  // CreateFormStart sets up a proper form tag that will cause the submit to
  // return control to this module for processing.
$smarty->assign('startform', $this->CreateFormStart ($id, 'do_updateoptions', $returnid));
$smarty->assign('endform', $this->CreateFormEnd ());

$smarty->assign('allowed_extensions',$this->Lang('allowed_extensions'));
$smarty->assign('input_allowed_extensions',$this->CreateInputText($id,'allowed_extensions',$this->GetPreference('allowed_extensions',''),50,255));

$smarty->assign('use_comment_wysiwyg',$this->Lang('use_comment_wysiwyg'));
$smarty->assign('input_use_comment_wysiwyg', $this->CreateInputCheckbox($id, 'use_comment_wysiwyg', '1', $this->GetPreference('use_comment_wysiwyg',1)));

$smarty->assign('submit', $this->CreateInputSubmit ($id, 'optionssubmitbutton', $this->Lang('submit')));

// Display the populated template
echo $this->ProcessTemplate ('adminoptions.tpl');

?>
<?php
#-------------------------------------------------------------------------
# Module: Gallery
# Method: Upgrade
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/gallery/
#
#-------------------------------------------------------------------------

if (!isset($gCms)) exit;


$current_version = $oldversion;
$db =& $this->GetDb();

switch($current_version)
{
	case "0.6.0-beta":
		$dict = NewDataDictionary($db);
		$sqlarray = $dict->AddColumnSQL(cms_db_prefix()."module_gallery", "galleryid I KEY");
		$dict->ExecuteSQLArray($sqlarray);
		
		$query = "UPDATE ".cms_db_prefix()."module_gallery SET galleryid=IF(fileid=1,0,1) WHERE filepath=''";
		$db->Execute($query);
		
		$query = "SELECT DISTINCT filepath FROM ".cms_db_prefix()."module_gallery WHERE (galleryid IS NULL OR galleryid = 0) AND fileid <> 1";
		$result = $db->Execute($query);
		while ($result && $row = $result->FetchRow())
		{
			$galleryinfo = $this->_Getgalleryinfo($row['filepath']);
			$query = "UPDATE ".cms_db_prefix()."module_gallery SET galleryid=? where filepath=?";
			$db->Execute($query,array($galleryinfo['fileid'],$row['filepath']));
		}
		$query = "UPDATE ".cms_db_prefix()."module_gallery_props SET resizemethod='sc' WHERE resizemethod='scale'";
		$db->Execute($query);
		$query = "UPDATE ".cms_db_prefix()."module_gallery_props SET resizemethod='cr' WHERE resizemethod='crop'";
		$db->Execute($query);
		$query = "UPDATE ".cms_db_prefix()."module_gallery_props SET resizemethod='zs' WHERE resizemethod='zoomscale'";
		$db->Execute($query);
		$query = "UPDATE ".cms_db_prefix()."module_gallery_props SET resizemethod='zc' WHERE resizemethod='zoomcrop'";
		$db->Execute($query);
		$current_version = "1.0";

	case "1.0":
	case "1.0.1":
		$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'template_fancybox.tpl';
		if( file_exists( $fn ) )
			{
				$template = @file_get_contents($fn);
				$this->SetTemplate('Fancybox',$template);
			}
		$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'template_lytebox_slideshow.tpl';
		if( file_exists( $fn ) )
			{
				$template = @file_get_contents($fn);
				$this->SetTemplate('Lytebox_slideshow',$template);
			}
		$current_version = "1.0.2";

	case "1.0.2":
	case "1.0.3":
// delete all thumbnails and old thumb directory
		$thumbsfolder = '..'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'Gallery'.DIRECTORY_SEPARATOR.'thumbs';
		if( $dh = @opendir($thumbsfolder) )
		{
			while( false !== ($obj = readdir($dh)) )
			{
				if( $obj != '.' && $obj != '..' )
				{
					@unlink($thumbsfolder.DIRECTORY_SEPARATOR.$obj);
				}
			}
			closedir($dh);
		}
		@rmdir($thumbsfolder);
// create new database table for template properties
		$taboptarray = array( 'mysql' => 'TYPE=MyISAM' );
		$dict = NewDataDictionary( $db );
		$flds = "
			templateid I KEY AUTO,
			template C(255),
			thumbwidth I,
			thumbheight I,
			resizemethod C(10),
			maxnumber I,
			sortitems C(255)
		";
		$sqlarray = $dict->CreateTableSQL( cms_db_prefix()."module_gallery_templateprops", $flds, $taboptarray);
		$dict->ExecuteSQLArray($sqlarray);

// save templateproperties for every Gallery template
		$query = "SELECT template_name FROM " . cms_db_prefix() . "module_templates WHERE module_name=?";
		$result = $db->Execute($query, array('Gallery'));
		if ( $result && $result->RecordCount() > 0 )
		{
		  while ( $row=$result->FetchRow() )
		  {
				$query2 = "INSERT INTO " . cms_db_prefix() . "module_gallery_templateprops (template,sortitems) VALUES (?,?)";
				$result2 = $db->Execute($query2, array($row['template_name'],'n-isdir/s+file'));
				if ( $result2 )
				{
					$tplid[$row['template_name']] = $db->Insert_ID();
				}
				else
				{
					echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
					exit();
				}
		  }
		}
		if ( !$result )
		{
			echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
			exit();
		}
// update default template content
		$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'template_fancybox.tpl';
		if( file_exists( $fn ) )
		{
			$templatecode = @file_get_contents($fn);
			$this->SetPreference('default_template_contents',$templatecode);
		}
// pass over thumbsettings from galleryprops to templateprops
		$tpl = array();
		$query = "SELECT fileid, template, thumbwidth, thumbheight, resizemethod FROM " . cms_db_prefix() . "module_gallery_props";
		$result = $db->Execute($query);
		if ( $result && $result->RecordCount() > 0 )
		{
			$defaulttemplate = $this->GetPreference('current_template');
		  while ( $row=$result->FetchRow() )
		  {
				$tpl[$row['fileid']] = isset($tplid[$row['template']]) ? $tplid[$row['template']] : 0;
				$row['template'] = ($row['template'] == 'usedefaulttemplate') ? $defaulttemplate : $row['template'];
				$query2 = "UPDATE " . cms_db_prefix() . "module_gallery_templateprops  SET thumbwidth=?, thumbheight=?, resizemethod=? WHERE template=?";
				$result2 = $db->Execute($query2, array($row['thumbwidth'],$row['thumbheight'],$row['resizemethod'],$row['template']));
				if ( !$result2 )
				{
					echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
					exit();
				}
		  }
		}
		if ( !$result )
		{
			echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
			exit();
		}
// drop table galleryprops
		$sqlarray = $dict->DropTableSQL( cms_db_prefix()."module_gallery_props" );
		$dict->ExecuteSQLArray($sqlarray);
		
// create new table galleryprops and fill it for every subgallery
		$flds = "
			fileid I KEY,
			templateid I,
			hideparentlink I
		";
		$sqlarray = $dict->CreateTableSQL( cms_db_prefix()."module_gallery_props", $flds, $taboptarray);
		$dict->ExecuteSQLArray($sqlarray);
		$query = "SELECT fileid FROM " . cms_db_prefix() . "module_gallery WHERE filename LIKE '%/'";
		$result = $db->Execute($query);
		if ( $result && $result->RecordCount() > 0 )
		{
		  while ( $row=$result->FetchRow() )
		  {
				$tpl[$row['fileid']] = isset($tpl[$row['fileid']]) ? $tpl[$row['fileid']] : 0;
				$hideparentlink = $row['fileid'] == 1 ? 1 : 0;
				$query2 = "INSERT INTO " . cms_db_prefix() . "module_gallery_props (fileid,templateid,hideparentlink) VALUES (?,?,?)";
				$result2 = $db->Execute($query2, array($row['fileid'], $tpl[$row['fileid']], $hideparentlink));
				if ( !$result2 )
				{
					echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
					exit();
				}
		  }
		}
		if ( !$result )
		{
			echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
			exit();
		}
		$current_version = "1.1";

	case "1.1":
		// create new thumbsfolder
		$thumbsfolder = '..'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'GalleryThumbs'.DIRECTORY_SEPARATOR;
		if( !is_dir($thumbsfolder) )
		{
			mkdir($thumbsfolder);
		}
		$current_version = "1.1.1";

	case "1.1.1":
		// update Fancybox css
		$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'fancybox'.DIRECTORY_SEPARATOR.'jquery.fancybox.css';
		$query = "SELECT * FROM " . cms_db_prefix() . "module_templates WHERE module_name=? AND content LIKE ?";
		$result = $db->Execute($query,array('Gallery', '%/* FANCYBOX %'));
		if ( file_exists($fn) && $result && $result->RecordCount() > 0 )
		{
			$templatecss = @file_get_contents($fn);
		  while ( $row=$result->FetchRow() )
		  {
				$templatearr = explode(TEMPLATE_SEPARATOR, $row['content']);
				$templatearr[1] = substr($templatearr[1], 0, strpos($templatearr[1], '/* FANCYBOX ')) . $templatecss;
				$templatecode = implode(TEMPLATE_SEPARATOR, $templatearr);
				$this->SetTemplate($row['template_name'],$templatecode);
				$handle = fopen('..'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'Gallery'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR. $row['template_name'] . '.css','w');
				fwrite($handle,$templatearr[1]);
				fclose($handle);
		  }
		}
		if ( !$result )
		{
			echo 'ERROR ' . __LINE__ . ': ' . mysql_error();
			exit();
		}
		$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'template_fancybox.tpl';
		if( file_exists($fn) )
		{
			$templatecode = @file_get_contents($fn);
			$this->SetPreference('default_template_contents',$templatecode);
		}
		$current_version = "1.1.2";
}

// put mention into the admin log
$this->Audit( 0, 
	      $this->Lang('friendlyname'), 
	      $this->Lang('upgraded', $this->GetVersion()));

?>
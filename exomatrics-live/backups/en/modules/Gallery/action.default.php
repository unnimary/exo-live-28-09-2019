<?php
if (!isset($gCms)) exit;

$params['dir'] = isset($params['dir']) ? rawurldecode(cms_html_entity_decode(trim($params['dir'],"/"))) : '';
$start = (isset($params['start']) && is_numeric($params['start'])) ? $params['start'] : 1;
$number = (isset($params['number']) && is_numeric($params['number'])) ? $params['number'] : 1000;
$show = (isset($params['show']) && in_array($params['show'], array('active','inactive','all'))) ? $params['show'] : 'active';


$imgcount = 0;
$itemcount = 0;
$images = array();

if ( is_dir(DEFAULT_GALLERY_PATH . $params['dir']) )
{
	$this->smarty->assign('gallerytitle', htmlspecialchars(trim(substr($params['dir'], strrpos($params['dir'], '/')),"/")));

	// get gallery info
	$galleryinfo = $this->_Getgalleryinfo($params['dir']);

	if ( isset($params['template']) )
	{
		// override template settings with param template
		$templateprops = $this->_GetTemplateprops($params['template']);
		$galleryinfo['templateid'] = $templateprops['templateid'];
		$galleryinfo['template'] = $templateprops['template'];
		$galleryinfo['thumbwidth'] = $templateprops['thumbwidth'];
		$galleryinfo['thumbheight'] = $templateprops['thumbheight'];
		$galleryinfo['resizemethod'] = $templateprops['resizemethod'];
		$galleryinfo['maxnumber'] = $templateprops['maxnumber'];
		$galleryinfo['sortitems'] = $templateprops['sortitems'];
	}
	if ( empty($galleryinfo['templateid']) )
	{
		// override template settings with default template
		$templateprops = $this->_GetTemplateprops($this->GetPreference('current_template'));
		$galleryinfo['templateid'] = $templateprops['templateid'];
		$galleryinfo['template'] = $templateprops['template'];
		$galleryinfo['thumbwidth'] = $templateprops['thumbwidth'];
		$galleryinfo['thumbheight'] = $templateprops['thumbheight'];
		$galleryinfo['resizemethod'] = $templateprops['resizemethod'];
		$galleryinfo['maxnumber'] = $templateprops['maxnumber'];
		$galleryinfo['sortitems'] = $templateprops['sortitems'];
	}
	//params['number'] override $number
	if ( $number == 1000 && !empty($galleryinfo['maxnumber']) )
	{
		$number = $galleryinfo['maxnumber'];
		$params['number'] = $number;
	}

	$paramslink = array();
	if ( isset($params['start']) || isset($params['number']) )
	{
		$paramslink['start'] = 1;
		$paramslink['number'] = $number;
	}
	if ( isset($params['show']) )
	{
		$paramslink['show'] = $params['show'];
	}
	
	if ( $show == "all" || !array_key_exists('active',$galleryinfo) || ($galleryinfo['active'] == 1 && $show == "active") || ($galleryinfo['active'] == 0 && $show == "inactive") )
	{
		if ( !empty($galleryinfo) )
		{
			if ( !empty($galleryinfo['title']) )
			{
				$this->smarty->assign('gallerytitle', $galleryinfo['title']);
			}
			$this->smarty->assign('gallerycomment', array_key_exists('comment',$galleryinfo) ? $galleryinfo['comment'] : '');
		}
			
		$paramslink['dir'] = strpos($params['dir'], '/') === FALSE ? '' : '/' . str_replace('%2F','/',rawurlencode(preg_replace('/^(.*)(\/.+)$/', '$1', $params['dir'])));
		$prettyurl = 'gallery' . $paramslink['dir'] . '/' .
					(isset($paramslink['start']) ? $paramslink['start'] . '-' . $paramslink['number'] . '-' : '') .
					(isset($paramslink['show']) ? $paramslink['show'] . '-' : '') .
					$returnid;
		$this->smarty->assign('parentlink', empty($params['dir']) ? '' : $this->CreateFrontendLink($id, $returnid, 'default', 'Parent', $paramslink, '', false, true, '', false, $prettyurl));
		$this->smarty->assign('hideparentlink', $galleryinfo['hideparentlink']);
	
	
		// build gallery
		$dirfiles = $this->_Getdirfiles($params['dir'],FALSE);
		$galeryfiles = $this->_Getgalleryfiles($params['dir']);
	
		// Walk through all items found in the directory, because they don't nescesarily exist in the database
		foreach($dirfiles as $key=>$item)
		{
			if ( !$galeryfiles || !array_key_exists($key, $galeryfiles) || $show == 'all' || ($show == 'active' && $galeryfiles[$key]['active'] != '0') || ($show == 'inactive' && $galeryfiles[$key]['active'] == '0') )
			{
				// create a new object for every record that we retrieve
				$rec = new stdClass();
				$rec->fileid = ($galeryfiles && array_key_exists($key, $galeryfiles)) ? $galeryfiles[$key]['fileid'] : $this->_AddFileToDB($item['filename'],$item['filepath'],$item['filemdate'],$galleryinfo['fileid']);
				$rec->file = DEFAULT_GALLERY_PATH . str_replace('%2F','/',rawurlencode($key));
				$rec->filedate = ($galeryfiles && array_key_exists($key, $galeryfiles)) ? $galeryfiles[$key]['filedate'] : '';
				$rec->title = ($galeryfiles && array_key_exists($key, $galeryfiles) && !empty($galeryfiles[$key]['title'])) ? $galeryfiles[$key]['title'] : trim($item['filename'],"/");
				$rec->comment = ($galeryfiles && array_key_exists($key, $galeryfiles)) ? $galeryfiles[$key]['comment'] : '';
				$rec->fileorder = ($galeryfiles && array_key_exists($key, $galeryfiles)) ? $galeryfiles[$key]['fileorder'] : 1000;
				$rec->active = ($galeryfiles && array_key_exists($key, $galeryfiles)) ? $galeryfiles[$key]['active'] : 1;
				$rec->isdir = false;
				$rec->folderlink = '';
				if ( is_dir(DEFAULT_GALLERY_PATH . $key) )
				{
					if ( !$galeryfiles || !array_key_exists($key, $galeryfiles) || empty($galeryfiles[$key]['thumb']) )
					{
						$rec->thumb = 'modules/Gallery/images/folder.png';
					}
					elseif ( $galleryinfo['thumbwidth'] > 0 )
					{
						$rec->thumb = DEFAULT_GALLERYTHUMBS_PATH . $galeryfiles[$key]['defaultfile'] . '-' . $galleryinfo['templateid'] . substr($galeryfiles[$key]['thumb'], strrpos($galeryfiles[$key]['thumb'], '.'));
						$originalimage = DEFAULT_GALLERY_PATH . str_replace(IM_PREFIX,'',$galeryfiles[$key]['thumb']);
					}
					else
					{
						$rec->thumb = DEFAULT_GALLERY_PATH . $galeryfiles[$key]['thumb'];
						$originalimage = DEFAULT_GALLERY_PATH . str_replace(IM_PREFIX,'',$galeryfiles[$key]['thumb']);
					}
					$paramslink['dir'] = str_replace('%2F','/',rawurlencode($key));
					$prettyurl = 'gallery/' . $paramslink['dir'] . '/' .
							(isset($paramslink['start']) ? $paramslink['start'] . '-' . $paramslink['number'] . '-' : '') .
							(isset($paramslink['show']) ? $paramslink['show'] . '-' : '') .
							$returnid;
					$rec->file = $this->CreateFrontendLink($id, $returnid, 'default',
						'', $paramslink, '', true, true, '', false, $prettyurl );
					$rec->isdir = true;

					// folderlink is deprecated, use $rec->file and $rec->thumb instead
					/* '<a href="' . $rec->file . '"><img src="' . $rec->thumb . '" alt="' . $rec->title . '" /></a>' */
					$rec->folderlink = 'deprecated';
				}
				elseif ( $galleryinfo['thumbwidth'] > 0 )
				{
					$rec->thumb = DEFAULT_GALLERYTHUMBS_PATH . $galeryfiles[$key]['fileid'] . '-' . $galleryinfo['templateid'] . substr($item['filename'], strrpos($item['filename'], '.'));
					$originalimage = DEFAULT_GALLERY_PATH . $key;
					$imgcount++;
				}
				else
				{
					$rec->thumb = DEFAULT_GALLERY_PATH . (empty($item['filepath']) ? '' : $item['filepath'] . '/') . IM_PREFIX . $item['filename'];
					$originalimage = DEFAULT_GALLERY_PATH . $key;
					$imgcount++;
				}

				if ( $rec->thumb != 'modules/Gallery/images/folder.png' )
				{
					$this->_CreateThumbnail($rec->thumb,
											$originalimage,
											($galleryinfo['thumbwidth'] > 0) ? $galleryinfo['thumbwidth'] : 96,
											($galleryinfo['thumbwidth'] > 0) ? $galleryinfo['thumbheight'] : 96,
											($galleryinfo['thumbwidth'] > 0) ? $galleryinfo['resizemethod'] : 'sc');
				}
				array_push($images,$rec);
			}
		}

		// Sort array $images
		// second parameter of _ArraySort is an array of strings, which contains:
		// n for number, s for string
		// + for ascending, - for descending
		// fieldname
		$sortarray = explode('/','n+fileorder/'.$galleryinfo['sortitems']);
		$images = $this->_ArraySort($images, $sortarray, false);
		$itemcount = count($images);

		// Get the images we want
		$images = array_splice($images, $start-1, $number);
	}
}
else
{
	$params['module_message'] = "The directory '" . htmlspecialchars($params['dir']) . "' does not exist. Check your parameters.";
}


// Expose the list to smarty. Use "by_ref" to save memory.
$this->smarty->assign_by_ref('images',$images);

// and a count of records
$this->smarty->assign('imagecount', $imgcount . ' ' . ($imgcount == 1 ? $this->Lang('image') : $this->Lang('images')));
$this->smarty->assign('itemcount', $itemcount);
$this->smarty->assign('number', $number);
$this->smarty->assign('pages', ceil($itemcount/$number));

// pagenavigation
$paramslink['dir'] = empty($params['dir']) ? '' : '/' . str_replace('%2F','/',rawurlencode($params['dir']));
$paramslink['start'] = $start - $number;		
$prettyurl = 'gallery' . $paramslink['dir'] . '/' . 
					(isset($paramslink['number']) ? $paramslink['start'] . '-' . $paramslink['number'] . '-' : '') .
					(isset($paramslink['show']) ? $paramslink['show'] . '-' : '') .
					$returnid;
$this->smarty->assign('prevpage', ($paramslink['start'] >= 1 ? $this->CreateFrontendLink($id, $returnid, 'default', $this->Lang('prevpage'), $paramslink, '', false, true, '', false, $prettyurl) : ('<em>' . $this->Lang('prevpage') . '</em>')));

$paramslink['start'] = $start + $number;		
$prettyurl = 'gallery' . $paramslink['dir'] . '/' . 
					(isset($paramslink['number']) ? $paramslink['start'] . '-' . $paramslink['number'] . '-' : '') .
					(isset($paramslink['show']) ? $paramslink['show'] . '-' : '') .
					$returnid;
$this->smarty->assign('nextpage', ($itemcount >= $paramslink['start'] ? $this->CreateFrontendLink($id, $returnid, 'default', $this->Lang('nextpage'), $paramslink, '', false, true, '', false, $prettyurl) : ('<em>' . $this->Lang('nextpage') . '</em>')));
$pagelinks = '';
for ($i = 0; $i < ceil($itemcount/$number); $i++) 
{
	$paramslink['start'] = ($i * $number) + 1;
	$prettyurl = 'gallery' . $paramslink['dir'] . '/' . 
						(isset($paramslink['number']) ? $paramslink['start'] . '-' . $paramslink['number'] . '-' : '') .
						(isset($paramslink['show']) ? $paramslink['show'] . '-' : '') .
						$returnid;
	$pagelinks .= ($paramslink['start'] != $start ? $this->CreateFrontendLink($id, $returnid, 'default', $i + 1, $paramslink, '', false, true, '', false, $prettyurl) : ('<em>' . ($i + 1) . '</em>'));
}
$this->smarty->assign('pagelinks', $pagelinks);


if (isset($params['module_message']))
{
	$this->smarty->assign('module_message',$params['module_message']);
}
else
{
	$this->smarty->assign('module_message','');
}


// Display template
echo $this->ProcessTemplateFromDatabase($galleryinfo['template']);


// pass data to head section.

// get template-specific JavaScript and echo
$templatecode = $this->GetTemplate($galleryinfo['template']);
$templatecodearr = explode(TEMPLATE_SEPARATOR, $templatecode);
$template_metadata = substr($templatecodearr[2],0,-2);

// check if a css file exists and echo
if ( file_exists("modules/Gallery/templates/css/" . $galleryinfo['template'] . ".css") )
{
	$template_metadata .= '<link rel="stylesheet" href="modules/Gallery/templates/css/' . $galleryinfo['template'] . '.css" type="text/css" media="screen" />
';
}
$this->GalleryMetadata = $template_metadata;

?>
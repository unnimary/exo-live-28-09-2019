<?xml version="1.0" encoding="iso-8859-1"?>
<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
' *** Edit Operations: declare variables

Dim MM_editAction
Dim MM_abortEdit
Dim MM_editQuery
Dim MM_editCmd

Dim MM_editConnection
Dim MM_editTable
Dim MM_editRedirectUrl
Dim MM_editColumn
Dim MM_recordId

Dim MM_fieldsStr
Dim MM_columnsStr
Dim MM_fields
Dim MM_columns
Dim MM_typeArray
Dim MM_formVal
Dim MM_delim
Dim MM_altVal
Dim MM_emptyVal
Dim MM_i

MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: set variables

If (CStr(Request("MM_insert")) = "form1") Then

  MM_editConnection = "Driver={Microsoft Access Driver (*.mdb)};DBQ="+ Server.MapPath("../db/thisis.mdb")
  MM_editTable = "Table1"
  MM_editRedirectUrl = "subscribed.html"
  MM_fieldsStr  = "name|value|mail|value|address|value|country|value|phone|value|Use1|value|Pas1|value"
  MM_columnsStr = "name|',none,''|mail|',none,''|address|',none,''|country|',none,''|phone|',none,''|Use1|',none,''|Pas1|',none,''"

  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  
  ' set the form values
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(MM_i+1) = CStr(Request.Form(MM_fields(MM_i)))
  Next

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If

End If
%>
<%
' *** Insert Record: construct a sql insert statement and execute it

Dim MM_tableValues
Dim MM_dbValues

If (CStr(Request("MM_insert")) <> "") Then

  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_formVal = MM_fields(MM_i+1)
    MM_typeArray = Split(MM_columns(MM_i+1),",")
    MM_delim = MM_typeArray(0)
    If (MM_delim = "none") Then MM_delim = ""
    MM_altVal = MM_typeArray(1)
    If (MM_altVal = "none") Then MM_altVal = ""
    MM_emptyVal = MM_typeArray(2)
    If (MM_emptyVal = "none") Then MM_emptyVal = ""
    If (MM_formVal = "") Then
      MM_formVal = MM_emptyVal
    Else
      If (MM_altVal <> "") Then
        MM_formVal = MM_altVal
      ElseIf (MM_delim = "'") Then  ' escape quotes
        MM_formVal = "'" & Replace(MM_formVal,"'","''") & "'"
      Else
        MM_formVal = MM_delim + MM_formVal + MM_delim
      End If
    End If
    If (MM_i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End If
    MM_tableValues = MM_tableValues & MM_columns(MM_i)
    MM_dbValues = MM_dbValues & MM_formVal
  Next
  MM_editQuery = "insert into " & MM_editTable & " (" & MM_tableValues & ") values (" & MM_dbValues & ")"

  If (Not MM_abortEdit) Then
    ' execute the insert
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    If (MM_editRedirectUrl <> "") Then
      Response.Redirect(MM_editRedirectUrl)
    End If
  End If

End If
%>
<%
Dim essur
Dim essur_numRows

Set essur = Server.CreateObject("ADODB.Recordset")
essur.ActiveConnection = "Driver={Microsoft Access Driver (*.mdb)};DBQ="+ Server.MapPath("../db/thisis.mdb")
essur.Source = "SELECT * FROM Table1"
essur.CursorType = 0
essur.CursorLocation = 2
essur.LockType = 1
essur.Open()

essur_numRows = 0
%>


<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

<body bgcolor="#E5E5E5" leftmargin="0" topmargin="0">
<form method="post" action="<%=MM_editAction%>" name="form1">
  <table width="365" align="center" cellpadding="2" cellspacing="5">
    <tr valign="baseline"> 
      <td width="125" align="right" nowrap><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">Name:</font></strong></td>
      <td width="281"> <input type="text" name="name" value="" size="22"> </td>
    </tr>
    <tr valign="baseline"> 
      <td nowrap align="right"><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">E-mail:</font></strong></td>
      <td> <input type="text" name="mail" value="" size="22"> </td>
    </tr>
    <tr valign="baseline"> 
      <td align="right" valign="middle" nowrap><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">Address:</font></strong></td>
      <td> <textarea name="address" cols="22" rows="4"></textarea> 
      </td>
    </tr>
    <tr valign="baseline"> 
      <td nowrap align="right"><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">Country:</font></strong></td>
      <td> <input type="text" name="country" value="" size="22"> </td>
    </tr>
    <tr valign="baseline"> 
      <td align="right" nowrap><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">Phone:</font></strong></td>
      <td> <input type="text" name="phone" value="" size="22"> </td>
    </tr>
    <tr valign="baseline" bgcolor="#CCCCCC"> 
      <td colspan="2" align="right" nowrap><div align="center"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Login 
          Information</strong></font></div></td>
    </tr>
    <tr valign="baseline"> 
      <td align="right" nowrap><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">User 
        Name:</font></strong></td>
      <td> <input type="text" name="Use1" value="" size="22"> </td>
    </tr>
    <tr valign="baseline"> 
      <td nowrap align="right"><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">Password:</font></strong></td>
      <td> <input type="password" name="Pas1" value="" size="22"> </td>
    </tr>
    <tr valign="baseline"> 
      <td nowrap align="right"><strong><font color="#0033FF" size="2" face="Arial, Helvetica, sans-serif">Confirm&nbsp;Password:</font></strong></td>
      <td><input name="textfield" type="password" size="22" /></td>
    </tr>
    <tr valign="baseline"> 
      <td nowrap align="right">&nbsp;</td>
      <td> <input type="submit" value="Subscribe"> </td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1">
</form>
<p>&nbsp;</p>
</body>
</html>
<%
essur.Close()
Set essur = Nothing
%>

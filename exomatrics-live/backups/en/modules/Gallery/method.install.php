<?php
#-------------------------------------------------------------------------
# Module: Gallery
# Method: Install
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/gallery/
#
#-------------------------------------------------------------------------

if (!isset($gCms)) exit;


$db =& $gCms->GetDb();

// mysql-specific, but ignored by other database
$taboptarray = array( 'mysql' => 'TYPE=MyISAM' );

$dict = NewDataDictionary( $db );

// table schema description
$flds = "
	fileid I KEY AUTO,
	filename C(255),
	filepath C(255),
	filedate " . CMS_ADODB_DT . ",
	fileorder I,
	active I,
	defaultfile I,
	galleryid I KEY,
	title C(255),
	comment X
";

$sqlarray = $dict->CreateTableSQL( cms_db_prefix()."module_gallery",
	$flds, 
	$taboptarray);
$dict->ExecuteSQLArray($sqlarray);
/*
// create a sequence
$db->CreateSequence(cms_db_prefix()."module_gallery_seq");
*/

$flds = "
	fileid I KEY,
	templateid I,
	hideparentlink I
";

$sqlarray = $dict->CreateTableSQL( cms_db_prefix()."module_gallery_props",
	$flds,
	$taboptarray);
$dict->ExecuteSQLArray($sqlarray);

$flds = "
	templateid I KEY AUTO,
	template C(255),
	thumbwidth I,
	thumbheight I,
	resizemethod C(10),
	maxnumber I,
	sortitems C(255)
";

$sqlarray = $dict->CreateTableSQL( cms_db_prefix()."module_gallery_templateprops",
	$flds,
	$taboptarray);
$dict->ExecuteSQLArray($sqlarray);

// create a permission
$this->CreatePermission('Use Gallery', 'Use Gallery');
$this->CreatePermission('Set Gallery Prefs','Set Gallery Prefs');

// check if css directory exists
$css_dir = '..'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'Gallery'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'css';
if( !is_dir($css_dir) )
{
	mkdir($css_dir);
}

// setup templates
$templates = array( array( 'tplname' => 'Fancybox' , 'tplfile' => 'template_fancybox.tpl' ),
										array( 'tplname' => 'Lightbox' , 'tplfile' => 'template_lightbox.tpl' ),
										array( 'tplname' => 'Lytebox_slideshow' , 'tplfile' => 'template_lytebox_slideshow.tpl' ),
										array( 'tplname' => 'Slimbox' , 'tplfile' => 'template_slimbox.tpl' ),
										array( 'tplname' => 'Thickbox' , 'tplfile' => 'template_thickbox.tpl' ));

foreach ( $templates as $template )
{
	$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template['tplfile'];
	if( file_exists( $fn ) )
	{
		$templatecode = @file_get_contents($fn);
		$this->SetTemplate($template['tplname'],$templatecode);
		$templatearr = explode(TEMPLATE_SEPARATOR, $templatecode);
		$templatecss = $templatearr[1];
		$handle = fopen($css_dir . DIRECTORY_SEPARATOR . $template['tplname'] . '.css','w');
		fwrite($handle,$templatecss);
		fclose($handle);
		$query = "INSERT INTO " . cms_db_prefix() . "module_gallery_templateprops (template,sortitems) VALUES (?,?)";
		$db->Execute($query, array($template['tplname'],'n-isdir/s+file'));
		if ( $template['tplname'] == 'Fancybox' )
		{
			$this->SetPreference('default_template_contents',$templatecode);
			$this->SetPreference('current_template',$template['tplname']);
		}
	}
}

// create a preference
$this->SetPreference("allowed_extensions", "jpg,jpeg,gif,png");
$this->SetPreference("use_comment_wysiwyg", false);

// register an event that the Gallery will issue. Other modules
// or user tags will be able to subscribe to this event, and trigger
// other actions when it gets called.
// $this->CreateEvent( 'OnGalleryPreferenceChange' );

// create default imagefolder and thumbsfolder
if( !is_dir('..' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, DEFAULT_GALLERY_PATH)) )
{
	mkdir('..' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, DEFAULT_GALLERY_PATH));
}
if( !is_dir('..' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, DEFAULT_GALLERYTHUMBS_PATH)) )
{
	mkdir('..' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, DEFAULT_GALLERYTHUMBS_PATH));
}

$query = "INSERT INTO " . cms_db_prefix() . "module_gallery (filename, filepath, filedate, fileorder, active, defaultfile, galleryid, title, comment) VALUES (?,?,?,-1,1,0,0,?,?)";
$db->Execute($query, array('Gallery/', '', date("Y-m-d H:i:s", filemtime('../uploads/images/Gallery')), $this->Lang('friendlyname'), $this->Lang('defaultgallerycomment')));
$query = "INSERT INTO " . cms_db_prefix() . "module_gallery_props (fileid,templateid,hideparentlink) VALUES (?,?,?)";
$db->Execute($query, array(1,0,1));


// put mention into the admin log
$this->Audit( 0, 
	$this->Lang('friendlyname'), 
	$this->Lang('installed', $this->GetVersion()) );
	      
?>
<?php
if (!isset($gCms)) exit;

$params['dir'] = isset($params['dir']) ? rawurldecode(cms_html_entity_decode(trim($params['dir'],"/"))) : '';
$number = (isset($params['number']) && is_numeric($params['number'])) ? $params['number'] : 6;
$show = (isset($params['show']) && in_array($params['show'], array('active','inactive','all'))) ? $params['show'] : 'active';

$imgcount = 0;
$itemcount = 0;
$images = array();

if ( is_dir(DEFAULT_GALLERY_PATH . $params['dir']) )
{
	$this->smarty->assign('gallerytitle', htmlspecialchars(trim(substr($params['dir'], strrpos($params['dir'], '/')),"/")));

	// get gallery info
	$galleryinfo = $this->_Getgalleryinfo($params['dir']);

	if ( isset($params['template']) )
	{
		// override template settings with param template
		$templateprops = $this->_GetTemplateprops($params['template']);
		$galleryinfo['templateid'] = $templateprops['templateid'];
		$galleryinfo['template'] = $templateprops['template'];
		$galleryinfo['thumbwidth'] = $templateprops['thumbwidth'];
		$galleryinfo['thumbheight'] = $templateprops['thumbheight'];
		$galleryinfo['resizemethod'] = $templateprops['resizemethod'];
	}
	if ( empty($galleryinfo['templateid']) )
	{
		// override template settings with default template
		$templateprops = $this->_GetTemplateprops($this->GetPreference('current_template'));
		$galleryinfo['templateid'] = $templateprops['templateid'];
		$galleryinfo['template'] = $templateprops['template'];
		$galleryinfo['thumbwidth'] = $templateprops['thumbwidth'];
		$galleryinfo['thumbheight'] = $templateprops['thumbheight'];
		$galleryinfo['resizemethod'] = $templateprops['resizemethod'];
	}

	if ( $galleryinfo['active'] == 1 )
	{
		if ( !empty($galleryinfo['title']) )
		{
			$this->smarty->assign('gallerytitle', $galleryinfo['title']);
		}
		$this->smarty->assign('gallerycomment', $galleryinfo['comment']);
		$this->smarty->assign('parentlink', '');
	}


	// build gallery with latest images
	
	$db =& $this->GetDB();	
	$query = "SELECT
							g1.*, g2.active
						FROM 
							" . cms_db_prefix() . "module_gallery g1
						LEFT JOIN
							" . cms_db_prefix() . "module_gallery g2
						ON
							g1.galleryid = g2.fileid
						WHERE
							g1.filename NOT LIKE '%/'
							AND (g1.galleryid=? OR g1.filepath LIKE ?)";
	Switch ($show)
	{
		Case 'active': 
			$query .= " AND g1.active=1 AND g2.active=1";
			break;
		Case 'inactive':
			$query .= " AND g1.active=0";
			break;
	}
	$query .= "
						ORDER BY
							filedate DESC
						LIMIT 0,?";
	$result = $db->Execute($query, array($galleryinfo['fileid'], (empty($params['dir']) ? '%' : $params['dir'] . "/%"), $number));
	if ( $result && $result->RecordCount() > 0 ) 
	{
	  $output = array();
	  while ( $row=$result->FetchRow() ) 
	  {
	    $output[trim($row['filepath'] . '/' . $row['filename'],'/')] = $row;
	    
		// create a new object for every record that we retrieve
		$rec = new stdClass();
		$rec->fileid = $row['fileid'];
		$rec->file = DEFAULT_GALLERY_PATH . str_replace('%2F','/',rawurlencode(trim($row['filepath'] . '/' . $row['filename'],'/')));
		$rec->filedate = $row['filedate'];
		$rec->title = empty($row['title']) ? $row['filename'] : $row['title'];
		$rec->comment = $row['comment'];
		if ( $galleryinfo['thumbwidth'] > 0 )
		{
			$rec->thumb = DEFAULT_GALLERYTHUMBS_PATH . $row['fileid'] . '-' . $galleryinfo['templateid'] . substr($row['filename'], strrpos($row['filename'], '.')) ;
		}
		else
		{
			$rec->thumb = DEFAULT_GALLERY_PATH . (empty($row['filepath']) ? '' : $row['filepath'] . '/') . IM_PREFIX . $row['filename'];
		}
		$rec->isdir = false;
		$itemcount++;
		$imgcount++;

		$this->_CreateThumbnail($rec->thumb,
														DEFAULT_GALLERY_PATH . trim($row['filepath'] . '/' . $row['filename'],'/'),
														($galleryinfo['thumbwidth'] > 0) ? $galleryinfo['thumbwidth'] : 96,
														($galleryinfo['thumbwidth'] > 0) ? $galleryinfo['thumbheight'] : 96,
														($galleryinfo['thumbwidth'] > 0) ? $galleryinfo['resizemethod'] : 'sc');
		array_push($images,$rec);
	  }
	}
}
else
{
	$params['module_message'] = "The directory '" . htmlspecialchars($params['dir']) . "' does not exist. Check your parameters.";
}


// Expose the list to smarty. Use "by_ref" to save memory.
$this->smarty->assign_by_ref('images',$images);

// and a count of records
$this->smarty->assign('imagecount', $imgcount . ' ' . ($imgcount == 1 ? $this->Lang('image') : $this->Lang('images')));
$this->smarty->assign('itemcount', $itemcount);
$this->smarty->assign('pages', 1);

// navigationlinks not nescesary, but define smarty variables for templates that use them
$this->smarty->assign('prevpage', '');
$this->smarty->assign('nextpage', '');
$this->smarty->assign('pagelinks', '');


if (isset($params['module_message']))
{
	$this->smarty->assign('module_message',$params['module_message']);
}
else
{
	$this->smarty->assign('module_message','');
}


// Display template
echo $this->ProcessTemplateFromDatabase($galleryinfo['template']);


// pass data to head section.

// get template-specific JavaScript and echo
$templatecode = $this->GetTemplate($galleryinfo['template']);
$templatecodearr = explode(TEMPLATE_SEPARATOR, $templatecode);
$template_metadata = substr($templatecodearr[2],0,-2);

// check if a css file exists and echo
if ( file_exists("modules/Gallery/templates/css/" . $galleryinfo['template'] . ".css") )
{
	$template_metadata .= '<link rel="stylesheet" href="modules/Gallery/templates/css/' . $galleryinfo['template'] . '.css" type="text/css" media="screen" />
';
}
$this->GalleryMetadata = $template_metadata;

?>
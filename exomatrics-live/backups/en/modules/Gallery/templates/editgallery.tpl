<div class="pageoverflow">
<h3>{$pagetitle}</h3>
</div>
{$formstart}{$hidden}

<div class="pageoverflow">
  <p class="pagetext">{$prompt_gallerytitle}:</p>
  <p class="pageinput">{$gallerytitle}</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$prompt_comment}:</p>
  <p class="pageinput">{$gallerycomment}</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$prompt_template}:</p>
  <p class="pageinput">{$template}</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$prompt_hideparentlink}:</p>
  <p class="pageinput">{$hideparentlink}</p>
</div>

{*if !empty($createthumbs)}
<div class="pageoverflow">
  <p class="pagetext">{$prompt_createthumbs}:</p>
  <p class="pageinput">{$createthumbs}</p>
</div>
{/if*}

{if !empty($sortbysettings)}
<div class="pageoverflow">
  <p class="pagetext">{$sortbysettings}</p>
</div>
{/if}

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">{$submit}{$cancel}</p>
</div>

	<table id="gtable" cellspacing="0" class="pagetable">
		<thead>
		<tr>
			<th class="pageicon">&nbsp;</th>
			<th>{$item}</th>
			<th>{$title}</th>
			<th>{$comment}</th>
			<th>{$filedate}</th>
			<th class="pageicon">{$active}</th>
			<th class="pageicon">{$default}</th>
		</tr>
		</thead>
		<tbody>
	{foreach from=$items item=entry}
		{cycle values="row1,row2" assign=rowclass}
		<tr id="{$entry->fileid}" class="{$rowclass}">
			<td>&nbsp;</td>
			<td><div style="width:96px; height:72px; background: url({$entry->thumburl}) no-repeat center; overflow:hidden; cursor:default;">&nbsp;</div></td>
			<td>{$entry->filename}<br />{$entry->title}</td>
			<td>{$entry->comment}</td>
			<td>{$entry->filedate}</td>
			<td class="pagepos" style="text-align:center">{$entry->activelink}</td>
			<td class="pagepos" style="text-align:center">{$entry->defaultlink}</td>
		</tr>
	{/foreach}
		</tbody>
	</table>

{if $itemcount > 0}
	<div class="pageoverflow">
	  <p class="pagetext">&nbsp;</p>
	  <p class="pageinput">{$submit}{$cancel}</p>
	</div>
{else}
	<h4>{$nofilestext}</h4>
{/if}

{$formend}

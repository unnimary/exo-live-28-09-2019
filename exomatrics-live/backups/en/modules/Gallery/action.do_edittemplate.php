<?php
if( !isset( $gCms ) ) exit();

if( isset($params['cancel']) )
{
	$params = array('active_tab' => 'templates');
	$this->Redirect($id, 'defaultadmin', '', $params);
}

if ( !$this->CheckPermission('Modify Templates') )
{
	return $this->DisplayErrorPage($id, $params, $returnid,$this->Lang('accessdenied'));
}

if ( empty($params['template']) || empty($params['templatecontent']) )
{
	$params = array('errors' => $this->Lang('error'), 'active_tab' => 'templates');
	$this->Redirect($id, 'defaultadmin', '', $params);
	return;
}

if ( isset($params['resetbutton']) )
{
	$fn = dirname(__FILE__).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'template_'.strtolower($params['template']).'.tpl';
	if( file_exists( $fn ) )
	{
		$templatecode = @file_get_contents($fn);
		list($params['templatecontent'], $params['templatecss'], $params['templatejs']) = explode(TEMPLATE_SEPARATOR, substr($templatecode,0,-2));
	}
}

// save template
$this->SetTemplate($params['template'], $params['templatecontent'].TEMPLATE_SEPARATOR.$params['templatecss'].TEMPLATE_SEPARATOR.$params['templatejs'].'*}' );

// save css-file
if ( empty($params['templatecss']) )
{
	unlink('../modules/Gallery/templates/css/' . $params['template'] . '.css');
}
else
{
	$handle = fopen('../modules/Gallery/templates/css/' . $params['template'] . '.css','w');
	fwrite($handle,$params['templatecss']);
	fclose($handle);
}

// save templateproperties
if ( $params['thumbwidth'] <= 0 || $params['thumbheight'] <= 0 )
{
	$params['thumbwidth'] = NULL;
	$params['thumbheight'] = NULL;
	$params['resizemethod'] = NULL;
}
$params['maxnumber'] = $params['maxnumber'] > 0 ? $params['maxnumber'] : NULL;

// join sortpreferences to string
$params['sortitems'] = '';
foreach($params['sortfield'] as $key=>$sortfield)
{
	$params['sortitems'] .= !empty($sortfield) ? str_replace('#',$params['sorttype'][$key],$sortfield).'/' : '';
}
$params['sortitems'] = substr($params['sortitems'],0,-1);

$query = "UPDATE " . cms_db_prefix() . "module_gallery_templateprops
		SET thumbwidth=?,thumbheight=?,resizemethod=?,maxnumber=?,sortitems=?
		WHERE template=?";
$result = $db->Execute($query, array($params['thumbwidth'],$params['thumbheight'],$params['resizemethod'],$params['maxnumber'],$params['sortitems'],$params['template']));
if ( !$result )
{
	echo 'ERROR: ' . mysql_error();
	exit();
}

if ( isset($params['applybutton']) || isset($params['resetbutton']) )
{
	$params = array('template' => $params['template'], 'mode' => "edit", 'module_message' => $this->Lang('templateupdated'));
	$this->Redirect($id, 'edittemplate', '', $params);
}
else
{
	$params = array('tab_message'=> 'templateupdated', 'active_tab' => 'templates');
	$this->Redirect($id, 'defaultadmin', '', $params);
}
?>
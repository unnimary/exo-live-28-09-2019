<?php /* Smarty version 2.6.25, created on 2013-07-12 00:38:34
         compiled from module_db_tpl:NMS%3Bsubscribe_email_body */ ?>
Hello <?php echo $this->_tpl_vars['username']; ?>
.
You have subscribed your email address to the following newsletter:
<?php $_from = $this->_tpl_vars['lists']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['list']):
?>
* <?php echo $this->_tpl_vars['list']; ?>

<?php endforeach; endif; unset($_from); ?>
If this is correct, please click the following link to confirm your subscription or copy and paste the link into your web browser. Without this confirmation, you will not receive any newsletters. <?php echo $this->_tpl_vars['confirmurl']; ?>
 If you did not sign up for this newsletter you do not need to do anything, simply delete this message. Thank you!
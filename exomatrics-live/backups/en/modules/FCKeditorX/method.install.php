<?php
$this->CreatePermission('Modify FCKeditorX', 'Modify FCKeditorX');

		$this->SetPreference('FCKConfig.SkinSelected', 'default');
		$this->SetPreference('FCKConfig.CMSMSLanguage', '1');
		$this->SetPreference('FCKConfig.EnableXHTML', '1');
		$this->SetPreference('FCKConfig.ContextMenu', '1');
		$this->SetPreference('FCKConfig.BgColor', 'default');
		$this->SetPreference('FCKConfig.BodyCSS', 'default');
		$this->SetPreference('FCKConfig.Width', 'default');
		$this->SetPreference('FCKConfig.Height', 'default');
		$this->SetPreference('FCKConfig.ToolbarSet', 'default');

		$basic = "
FCKConfig.ToolbarSets[\"Basic\"] = [
	['Bold','Italic','-','OrderedList','UnorderedList','-','Link','Unlink','-','About']
] ;

FCKConfig.ToolbarSets[\"Full\"] = [
	['Source','-','Templates'],
	['Cut','Copy','Paste','PasteText','PasteWord','-','Print','SpellCheck'],
	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
	['OrderedList','UnorderedList','-','Outdent','Indent'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],'/',
	['CMSModules','CMSContent','Link','Unlink','Anchor'],
	['Image','Flash','Table','Rule','SpecialChar'],
	['Form','Checkbox','Radio','TextField','Textarea','Select','Button'],
	['ImageButton','HiddenField'],
	'/',
	['Style','FontFormat','FontName','FontSize'],
	['TextColor','BGColor'],
	['About']
] ;
";
		$this->SetPreference('FCKConfig.CustomToolbar', $basic);
		$this->SetPreference('FCKConfig.Styles', '');
		//     $this->SetPreference('FCKConfig.ToolbarSets', '');

		///////////////////////////////////////
/*		global $gCms;
		$db =& $gCms->GetDb();

		$dict = NewDataDictionary($db);
		$flds = "
			fckx_id I KEY,
			data X
		";

		$taboptarray = array('mysql' => 'TYPE=MyISAM');
		$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_FCKX",
		$flds, $taboptarray);
		$dict->ExecuteSQLArray($sqlarray);


		$query  = "INSERT INTO " . cms_db_prefix() . "module_FCKX ( fckx_id , data ) ";
		$query .= "VALUES ('1', '')";
		$result = $db->Execute($query);

		$query  = "INSERT INTO " . cms_db_prefix() . "module_FCKX ( fckx_id , data ) ";
		$query .= "VALUES ('2', '')";
		$result = $db->Execute($query);
*/


		/* This was redeclared here again, but doesn't need to be here?  /westis

		$basic = "
		FCKConfig.ToolbarSets[\"Basic\"] = [
		['Bold','Italic','-','OrderedList','UnorderedList','-','Link','Unlink','-','About']
		] ;
		";
		*/


		//$db->Execute("UPDATE " . cms_db_prefix() . "module_FCKX SET data = ? WHERE fckx_id = ? LIMIT 1", array($basic, 2));


		///////////////////////////////////////

		// put mention into the admin log
		$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('installed',$this->GetVersion()));
?>
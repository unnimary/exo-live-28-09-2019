<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGExtensions (c) 2008 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to provide useful functions
#  and commonly used gui capabilities to other modules.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGDebug
{
  var $_output;
  var $_instant;
  var $_filename;
  var $_html;

  function CGDebug()
    {
      $this->_html = 0;
      $this->_instant = 0;
      $this->_filename = '';
    }
  
  function html($var = null)
  {
    if( !is_null($var) )
      {
	$this->_html = $var;
	return;
      }
    return $this->_html;
  }

  function instant($var = null)
  {
    if( !is_null($var) )
      {
	$this->_instant = $var;
      }
    return $this->_html;
  }

  function output_to_file()
  {
    return $this->output(TMP_CACHE_LOCATION.'/cgdebug.log');
  }

  function output($filename = '')
  {
    global $gCms;
    if( !isset($gCms->config['debug']) || !$gCms->config['debug'] )
      return;

    if( !count($this->_output) ) return;

    if( empty($filename) )
      {
	$fh = @fopen($filename,'w');
	if( !$fh ) return;

	foreach( $this->_output as $one )
	  {
	    fputs($fh,$one);
	  }
	fclose($fh);
	return;
      }

    foreach( $this->_output as $one )
      {
	echo $one;
      }
  }

  function add($var,$title = '')
  {
    global $gCms;
    if( !isset($gCms->config['debug']) || !$gCms->config['debug'] )
      return;


    $out = '';
    if( !$var ) return;

    if( empty($title) )
      {
	$title = 'DEBUG: ';
      }
    if( $this->html() )
      {
	$out .= '<b>$title</b>';
      }
    else
      {
	$out .= $title;
      }

    ob_start();
    if( $this->html() )
      {
	echo '<pre>';
      }
    if( is_array($var) )
      {
	echo "Number of elements: " . count($var) . "\n";
	print_r($var);
      }
    elseif(is_object($var))
      {
	print_r($var);
      }
    elseif(is_string($var))
      {
	print_r(htmlentities(str_replace("\t", '  ', $var)));
      }
    elseif(is_bool($var))
      {
	echo $var === true ? 'true' : 'false';
      }
    else
      {
	print_r($var);
      }
    if( $this->html() )
      {
	echo '</pre>';
      }

    $out .= ob_get_contents();
    ob_end_clean();
    $out .= "\n";

    if( $this->instant() )
      {
	echo $out;
      }
    else
      {
	$this->_output[] = $out;
      }
  }
}

#
# EOF
#
?>
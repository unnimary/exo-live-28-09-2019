<?php /* Smarty version 2.6.25, created on 2013-07-16 00:13:40
         compiled from module_db_tpl:NMS%3Bpost_email_confirm_message */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cms_date_format', 'module_db_tpl:NMS;post_email_confirm_message', 2, false),)), $this); ?>

Thank you <?php echo $this->_tpl_vars['username']; ?>
.  Your email address <?php echo $this->_tpl_vars['email']; ?>
 has been marked as confirmed as of <?php echo ((is_array($_tmp=$this->_tpl_vars['dateconfirmed'])) ? $this->_run_mod_handler('cms_date_format', true, $_tmp) : smarty_cms_modifier_cms_date_format($_tmp)); ?>
.  Your uniquid (for future reference) is <?php echo $this->_tpl_vars['uniqueid']; ?>
<?php
$lang['cleantemptable'] = 'Vyčistit dočasnou tabulku';
$lang['error_problemwithmessage'] = 'Unspecified message problem.';
$lang['prompt_usersettings_page'] = 'Return page for Usersettings form';
$lang['info_usersettings_page'] = 'Page to return to when an email link is clicked on to edit user preferences.<br/>The (*) Indicates the default page.';
$lang['jobname'] = '&Uacute;loha vytvořena v';
$lang['msg_jobprocessing'] = 'Batch Processing Window';
$lang['prompt_page'] = 'Return Page';
$lang['error_insufficientparams'] = 'One or more required parameters are missing from the request.  This action cannot proceed';
$lang['prompt_from'] = 'Od';
$lang['prompt_replyto'] = 'Adresa odpovědi';
$lang['prompt_subject'] = 'Předmět';
$lang['prompt_template'] = '&Scaron;ablona';
$lang['prompt_selectstatus'] = 'Vybrat stav';
$lang['prompt_message'] = 'Zpr&aacute;va';
$lang['prompt_userfilter'] = 'E-mailov&yacute; filtr (regul&aacute;rn&iacute; v&yacute;razy)';
$lang['prompt_listfilter'] = 'Filter on list Membership';
$lang['info_listfilter'] = '<strong>Note:</strong> Hold the CTRL key down and click on items to select and deselect them.
';
$lang['message_help'] = 'When typing a message, the following <em>smarty</em> variables are available:<br/>
<em>{$username}</em> - The users name<br/>
<em>{$email}</em> - The users email address<br/>
<em>{$unsubscribe}</em> - A URL that can be used to display a page for unsubscribing<br/>
<em>{$preferences}</em> - A URL that can be used to display a user preferences page<br/>
<em>{$confirmurl}</em> - A URL that can be used to confirm subscriptions<br/>
<strong>Note:</strong>&nbsp; When composing an HTML message and <em>not</em> using a page template, you will need to supply the appropriate HTML tags (html,head,body,etc).<br/>
<strong>Note:</strong>&nbsp; When using a page template as a message template, you need to use {$message_text} in place of {content} in your template.
';
$lang['applyfilter'] = 'Filtr';
$lang['info_event_OnNewUser'] = 'Generated when a new user subscribes to one or more mailing lists';
$lang['info_event_OnEditUser'] = 'Generated when a user is modified either in the admin or via the front end';
$lang['info_event_OnDeleteUser'] = 'Generated when a user is deleted';
$lang['info_event_OnNewList'] = 'Generated when a mailing list is modified';
$lang['info_event_OnDeleteList'] = 'Generated when a mailing list is deleted';
$lang['info_event_OnCreateMessage'] = 'Generated when a message is created';
$lang['info_event_OnEditMessage'] = 'Generated when a message is modified';
$lang['info_event_OnDeleteMessage'] = 'Generated when a message is deleted';
$lang['info_event_OnCreateJob'] = 'Generated when a job is created';
$lang['info_event_OnDeleteJob'] = 'Generated when a job is deleted';
$lang['info_event_OnStartJob'] = 'Generated when the processing of a job begins';
$lang['info_event_OnFinishJob'] = 'Generated when the processing of a job finishes';
$lang['help_OnNewUser'] = '<p>An event generated when a new user subscribes to one or more mailing lists</p>
<h4>Parameters</h4>
<ul>
<li><em>email</em> - Users email address</li>
<li><em>username</em> - Username</li>
<li><em>lists</em> - Array of member lists</li>
</ul>
';
$lang['help_OnEditUser'] = '<p>An event generated when a user is modified either in the admin or via the front end</p>
<h4>Parameters</h4>
<ul>
<li><em>email</em> - Users email address</li>
<li><em>username</em> - Username</li>
<li><em>lists</em> - Array of member lists</li>
<li><em>id</em> - The user id</li>
</ul>
';
$lang['help_OnDeleteUser'] = '<p>An event generated when a user is deleted</p>
<h4>Parameters</h4>
<ul>
<li><em>id</em> - The user id</li>
</ul>
';
$lang['help_OnNewList'] = '<p>An event generated when a new mailing list is created</p>
<h4>Parameters</h4>
<ul>
<li><em>name</em> - The list name</li>
<li><em>description</em> - List description</li>
<li><em>public</em> - Public flag</li>
</ul>
';
$lang['help_OnEditList'] = '<p>An event generated when a mailing list is modified</p>
<h4>Parameters</h4>
<ul>
<li><em>name</em> - The list name</li>
<li><em>description</em> - List description</li>
<li><em>public</em> - Public flag</li>
<li><em>listid</em> - The list ID</li>
</ul>
';
$lang['help_OnDeleteList'] = '<p>An event generated when a mailing list is deleted</p>
<h4>Parameters</h4>
<ul>
<li><em>listid</em> - The list ID</li>
</ul>
';
$lang['help_OnCreateMessage'] = '<p>An event generated when a message is created</p>
<h4>Parameters</h4>
<ul>
<li><em>fromwho</em> - The Name of the person this message is from</li>
<li><em>reply_to</em> - The Email address for the reply to field</li>
<li><em>subject</em> - The subject of the message</li>
<li><em>message</em> - The body of the message <em>(may contain smarty tags)</em></li>
<li><em>entered</em> - The date the message was entered</li>
<li><em>uniqueid</em> - The unique id of this message</em>
</ul>
';
$lang['help_OnEditMessage'] = '<p>An event generated when a message is modified</p>
<h4>Parameters</h4>
<ul>
<li><em>fromwho</em> - The Name of the person this message is from</li>
<li><em>reply_to</em> - The Email address for the reply to field</li>
<li><em>subject</em> - The subject of the message</li>
<li><em>message</em> - The body of the message <em>(may contain smarty tags)</em></li>
<li><em>messageid</em> - The ID of this message</id>
</ul>
';
$lang['help_OnDeleteMessage'] = '<p>An event generated when a message is deleted</p>
<h4>Parameters</h4>
<ul>
<li><em>messageid</em> - The ID of the message that has been deleted</li>
</ul>
';
$lang['help_OnCreateJob'] = '<p>An event generated when a job is created</p>
<h4>Parameters</h4>
<ul>
<li><em>jobid</em> - The ID of this job</li>
<li><em>jobname</em> - The name of this job</li>
<li><em>lists</em> - An array of listids</li>
</ul>
';
$lang['help_OnDeleteJob'] = '<p>An event generated when a job is deleted</p>
<h4>Parameters</h4>
<ul>
<li><em>jobid</em> - The ID of this job</li>
</ul>
';
$lang['help_OnStartJob'] = '<p>An event generated when the processing of a job begins</p>
<h4>Parameters</h4>
<ul>
<li><em>jobid</em> - The ID of this job</li>
<li><em>jobname</em> - The name of this job</li>
</ul>
';
$lang['help_OnFinishJob'] = '<p>An event generated when the processing of a job finishes</p>
<h4>Parameters</h4>
<ul>
<li><em>jobid</em> - The ID of this job</li>
<li><em>jobname</em> - The name of this job</li>
</ul>
';
$lang['error_needreplyto'] = 'You must enter a valid reply to email address';
$lang['error_needfrom'] = 'You must enter a valid name for the originator of the message';
$lang['error_needsubject'] = 'You must enter a valid email subject';
$lang['error_needmessagetext'] = 'You must enter something for a message body';
$lang['error_formerror'] = 'Form error';
$lang['error_dberror'] = 'Database error';
$lang['invalidparam'] = 'Invalid param in the NMS module tag';
$lang['prompt_users_per_page'] = 'Number of users to display on each page of the user list';
$lang['disabled'] = 'Disabled';
$lang['confirmed'] = 'Potvrzeno';
$lang['prompt_usersettings_text2'] = 'Text displayed after the user settings form is submitted';
$lang['prompt_usersettings_form2'] = 'Form displayed to users wishing to change settings';
$lang['prompt_usersettings_text'] = 'Text displayed after the user settings message is sent';
$lang['prompt_usersettings_email_body'] = 'Body of the user settings email';
$lang['prompt_usersettings_subject'] = 'Subject of user settings email';
$lang['prompt_usersettings_form'] = 'Form asking users who whish to change settings, their email address';
$lang['prompt_post_unsubscribe_text'] = 'Text displayed after the unsubscribe process is complete';
$lang['user_settings'] = 'Uživatelsk&aacute; nastaven&iacute;';
$lang['prompt_unsubscribe_prompt'] = 'Prompt asking the user for his email address to unsubscribe';
$lang['prompt_unsubscribe_text'] = 'Text displayed after unsubscribe form is completed';
$lang['prompt_unsubscribe_subject'] = 'Subject of the email sent to unsubscribe';
$lang['prompt_unsubscribe_email_body'] = 'Body of the email sent to unsubscribe';
$lang['prompt_unsubscribe_form'] = 'Template for the unsubscribe form';
$lang['error_accountdisabled'] = 'We are sorry, but your account has been disabled and this action is not possible at this time';
$lang['prompt_post_confirm_text'] = 'Zpr&aacute;va zobrazen&aacute; po potvrzen&iacute; e-mailem.';
$lang['prompt_confirm_email_body'] = 'Text potvrzovac&iacute;ho emailu';
$lang['prompt_confirm_subject'] = 'Předmět potvrzovac&iacute;ho emailu';
$lang['confirm_subscribe'] = 'Potvrďte přihl&aacute;&scaron;en&iacute;';
$lang['confirm_unsubscribe'] = 'Potvrďte odhl&aacute;&scaron;en&iacute;';
$lang['nolists'] = 'Nebyly nalezeny ž&aacute;dn&eacute; rozes&iacute;lac&iacute; seznamy k přihl&aacute;&scaron;en&iacute;';
$lang['public'] = 'Veřejn&yacute;';
$lang['yes'] = 'Ano';
$lang['no'] = 'Ne';
$lang['prompt_postsubscribetext'] = 'Zpr&aacute;va zobrazen&aacute; po přihl&aacute;&scaron;en&iacute; k odeb&iacute;r&aacute;n&iacute;';
$lang['subscription_confirmed'] = 'Přihl&aacute;&scaron;en&iacute; potvrzeno';
$lang['reset'] = 'Resetovat';
$lang['suspend'] = 'Pozastavit';
$lang['resume'] = 'Obnovit';
$lang['action'] = 'Akce';
$lang['processing_records'] = 'Zpracov&aacute;v&aacute;n&iacute; z&aacute;znamů od %s do %s';
$lang['initializing_job'] = 'Inicializace jobu';
$lang['processing_job'] = 'Zpracov&aacute;v&aacute;n&iacute; jobu';
$lang['deletecompleted'] = 'Smazat ukončen&eacute; joby';
$lang['error_importerrorcount'] = 'Chyba sč&iacute;t&aacute;n&iacute;';
$lang['lines'] = 'Ř&aacute;dků';
$lang['users_added'] = 'Uživatel&eacute; přid&aacute;ni';
$lang['memberships'] = 'Členstv&iacute;';
$lang['subscribe'] = 'Přihl&aacute;sit se k odběru';
$lang['resetdefaults'] = 'Reset na z&aacute;kladn&iacute; nastaven&iacute;';
$lang['confirm_adjustsettings'] = 'Jste si jisti, že chcete změnit tato nastaven&iacute;?';
$lang['confirm_resetdefaults'] = 'Opravu chcete vr&aacute;tit tento formul&aacute;ř do jeho v&yacute;choz&iacute; podoby??';
$lang['prompt_subscribetext'] = 'Subscribe Text';
$lang['prompt_subscribesubject'] = 'Subscribe Subject';
$lang['prompt_subscribe_email_body'] = 'Subscribe Email Body';
$lang['prompt_subscribe_form'] = 'Template for the Subscribe Form';
$lang['subscribe_form'] = 'Přihl&aacute;sit';
$lang['unsubscribe_form'] = 'Odhl&aacute;sit';
$lang['error_insertinglist'] = 'Problem creating the new list';
$lang['error_emailexists'] = 'That email address already exists in the user list';
$lang['error_invalidusername'] = 'Neplatn&eacute; uživatelsk&eacute; jm&eacute;no';
$lang['error_invalidid'] = 'Neplatn&yacute; jedinečn&yacute; identifik&aacute;tor';
$lang['username'] = 'Uživatelsk&eacute; jm&eacute;no';
$lang['name'] = 'Jm&eacute;no';
$lang['prompt_public'] = 'Toto je veřejn&yacute; rozes&iacute;lac&iacute; seznam';
$lang['error_usernotfound'] = 'Nebylo možno nal&eacute;zt uživatele se specifikovan&yacute;m id';
$lang['prompt_ms_between_message_sleep'] = 'Delay (in milliseconds) between sending each message';
$lang['prompt_between_batch_sleep'] = 'Delay (in seconds) between each batch';
$lang['prompt_messages_per_batch'] = 'The maximum number of messages to send in a batch';
$lang['okclosewindow'] = 'Můžete zavř&iacute;t toto okno';
$lang['queuefinished'] = 'Zpracov&aacute;n&iacute; fronty v&yacute;stupu ukončeno';
$lang['totaltime'] = 'Total processing time:';
$lang['seconds'] = 'Vteřin';
$lang['totalmails'] = 'Celkem odesl&aacute;no emailů (včetně emailu adminovi):';
$lang['page'] = 'Strana';
$lang['of'] = 'z';
$lang['info_csvformat'] = 'The import file must be in CSV (comma separated value) format, one entry per line.<br />
The columns are:<br/><br/>
uniqueid, username, email address, list name, active<br /> <br />
Email addresses may be repeated on subsequent lines (particularly for different lists).';
$lang['prompt_lines'] = 'Lines Imported';
$lang['prompt_usersadded'] = 'Users Added';
$lang['prompt_membershipsadded'] = 'Memberships added';
$lang['prompt_errorcount'] = 'Počet chyb';
$lang['importerror_cantgetuserid'] = 'Error at line %s, could not get userid %s';
$lang['importerror_cantcreateuser'] = 'Error at line %s, could not create user %s';
$lang['importerror_nosuchlist'] = 'Error at line %s, no such list %s';
$lang['importerror_nofields'] = 'Error at line %s, not enough fields';
$lang['import_users'] = 'Import Users from CSV';
$lang['error_emptyfile'] = 'ERROR- An empty file was uploaded';
$lang['error_nofilesuploaded'] = 'ERROR- no files were uploaded';
$lang['filename'] = 'Import Filename';
$lang['title_users_import'] = 'Import Users from CSV';
$lang['title_users_export'] = 'Export Users from Database';
$lang['nummessages'] = 'počet zpr&aacute;v';
$lang['created'] = 'Created';
$lang['started'] = 'Started';
$lang['finished'] = 'Finished';
$lang['processjobs'] = 'Process Jobs';
$lang['status_error'] = 'CHYBA';
$lang['status_unstarted'] = 'Not started';
$lang['status_inprogress'] = 'In Progress';
$lang['status_paused'] = 'Paused';
$lang['status_complete'] = 'Complete';
$lang['status_unknown'] = 'Unknown';
$lang['error_jobnameexists'] = 'ERROR- A job with that name already exists';
$lang['createjob'] = 'Vytvořit job';
$lang['prompt_email_user_on_admin_subscribe'] = 'Send users an email when the administrator manually adds membership to a list';
$lang['error_nomessagesselected'] = 'No messages selected';
$lang['error_nolistsselected'] = 'No lists selected';
$lang['createjobmsg'] = 'Select one message, and one ore more lists to send the message to';
$lang['error_nojobname'] = 'ERROR- No job name specified';
$lang['error_nolists'] = 'ERROR- No lists!';
$lang['error_nomessages'] = 'CHYBA - ž&aacute;dn&eacute; zpr&aacute;vy!';
$lang['jobs'] = 'Joby';
$lang['status'] = 'Status';
$lang['jobsfoundtext'] = 'Nalezen&eacute; joby';
$lang['messagesfoundtext'] = 'Nalezen&eacute; zpr&aacute;vy';
$lang['entered'] = 'Entered';
$lang['subject'] = 'Předmět';
$lang['from'] = 'Od';
$lang['delete_user_confirm'] = 'Are you sure you want to delete this user';
$lang['info_singlelist'] = 'This user will be added to the sole mailing list';
$lang['error_selectonelist'] = 'ERROR - You must select atleast one list';
$lang['error_invaliduniqueid'] = 'CHYBA - neplatn&yacute; jedinečn&yacute; identifik&aacute;tor';
$lang['error_invalidemail'] = 'CHYBA - neplatn&aacute; emailov&aacute; adresa';
$lang['error_couldnotfindjobpart'] = 'ERROR- Could not find a requested job part in the database.  This may indicate that another user (you?) is editing/deleting jobs whilst a job is processing.';
$lang['error_couldnotfindmessage'] = 'ERROR- Could not find a requested message.  This may indicate that another user (you?) is editing/deleting messages whilst a job is processing.';
$lang['error_couldnotfindtemplate'] = 'ERROR- Could not find a requested page template.  This may indicate that the template was deleted since the mail message was created.';
$lang['error_temporarytableexists'] = 'ERROR- The temporary database table, used for job processing already exists.  This probably indicates that an error occurred when trying to process a job previously.';
$lang['error_buildingtemptable'] = 'ERROR- A problem occurred when filling the temporary table';
$lang['error_otherprocessingerror'] = 'ERROR: An error occurred during processing';
$lang['userid'] = 'ID uživatele';
$lang['emailaddress'] = 'Emailov&aacute; adresa';
$lang['usersfoundtext'] = 'Nalezen&iacute; uživatel&eacute;';
$lang['title_user_createnew'] = 'Přidat uživatele';
$lang['error_invalidlistname'] = 'ERROR- Invalid list name';
$lang['editlist_text'] = 'Edit List';
$lang['id'] = 'ID';
$lang['listsfoundtext'] = 'Lists found';
$lang['users'] = 'Uživatel&eacute;';
$lang['preferences'] = 'Nastaven&iacute;';
$lang['messages'] = 'Zpr&aacute;vy';
$lang['queue'] = 'Fronta';
$lang['submit'] = 'Odeslat';
$lang['cancel'] = 'Storno';
$lang['description'] = 'Popis';
$lang['createnewlist_text'] = 'Create List';
$lang['lists'] = 'Lists';
$lang['friendlyname'] = 'Newsletter Made Simple';
$lang['postinstall'] = 'Thank you for installing NMS. Be sure to set \&quot;Use NMS\&quot; permissions to use this module!';
$lang['postuninstall'] = 'Newsletter Made Simple Uninstalled.';
$lang['uninstalled'] = 'Module Uninstalled.';
$lang['installed'] = 'Module version %s installed.';
$lang['prefsupdated'] = 'Newsletter Made Simple Module preferences updated.';
$lang['newslettercreated'] = 'A new newsletter has been created.';
$lang['no_email_error'] = 'You must fill in an email address.';
$lang['subscribe_thankyou'] = 'Thank you for subscribing. A confirmation e-mail has been sent to your inbox.';
$lang['enter_valid_email'] = 'You must enter a valid e-mail address and select at least one list.';
$lang['newslettercreatederror'] = 'You must enter a name and description.';
$lang['accessdenied'] = 'Access Denied. Please check your permissions.';
$lang['error'] = 'Chyba!';
$lang['sent'] = 'Odesl&aacute;no';
$lang['inqueue'] = 'Ve frontě';
$lang['send_next_batch'] = 'Sending the next batch of %s emails; starting at: %s';
$lang['messages_sent'] = '%s byl posl&aacute;n';
$lang['closewindow'] = 'Zavř&iacute;t okno';
$lang['testmode'] = 'You are in test mode. No e-mails will be sent.';
$lang['confirmdeletejob'] = 'Are you sure you want to delete this job';
$lang['confirmsend'] = 'Are you sure you want to send all messages?';
$lang['confirmdelete'] = 'Are you sure you want to delete this message?';
$lang['confirmdeletelist'] = 'Are you sure you want to delete this list?';
$lang['keepwindowopen'] = 'Keep this window open till all messages have been sent.<br>';
$lang['profileupdated'] = 'Your profile has been updated.';
$lang['upgraded'] = 'Module upgraded to version %s.';
$lang['title_mod_prefs'] = 'Nastaven&iacute;';
$lang['title_mod_messages'] = 'Fronta zpr&aacute;v';
$lang['title_mod_createnew'] = 'Create a List';
$lang['title_mod_manage_user'] = 'Uživatel&eacute;';
$lang['title_mod_compose_job'] = 'Create Job';
$lang['title_mod_compose_message'] = 'Compose Message';
$lang['title_mod_process_queue'] = 'Process Queue';
$lang['title_mod_admin'] = 'Manage Lists';
$lang['user_delete_confirm'] = 'Are you sure you want to delete this user?';
$lang['userdeleted'] = 'Uživatel vymaz&aacute;n';
$lang['newsletterdeleted'] = 'Newsletter Deleted';
$lang['delete'] = 'Smazat';
$lang['edit'] = 'Edit';
$lang['previous'] = 'Předchoz&iacute;';
$lang['next'] = 'N&aacute;sleduj&iacute;c&iacute;';
$lang['unsubscribemessage'] = 'You have been unsubscribed. Thank you.';
$lang['title_admin_panel'] = 'Newsletter Made Simple';
$lang['moddescription'] = 'This module is a module that allows you to create a newsletter system.';
$lang['welcome_text'] = '<p>Welcome to Newsletter Made Simple (MNS) Module admin section. </p>';
$lang['changelog'] = '<ul>
<li>Todo:
<ul>
<li>Upgrading</li>
<li>Lists Tab - show disabled vs active lists</li>
<li>Lists - ability to Mark a list as inactive</li>
<li>Users - Ability to filter on confirmed or unconfirmed
<p>Admin should be able to check all unconfirmed users and either confirm them all, or send them another confirmation email message</p>
<li>Users - Ability to mark a user as disabled</li>
<li>Users - Ability to prefer text or html mail (much later)</li>
<li>Preferences - Ability to mark output messages as bulk or not</li>
<li>Something to allow a user to re-get the confirmation email</li>
<li>Docs, docs, and more docs</li>
<li><b>The frontend</b></li>
<ul>
  <li>complete the two stage unsubscribe process</li>
  <li>complete the two stage change preferences process</li>
  <li>Add a preference as to wether or not users should get a confirmation email after subscribing and unsubscribing</p>
</ul>
<li>Styling the progress page</li>
<li>Check for hardcoded strings</li>
<li>HTML vs Non HTML mail</li>
<li>Export users<li>
</ul>
</li>
<li>Version 1.0.1 - December, 2006</li>
<p>Fixes to import users, and to a ternery expression when creating a message. I also fixed some stupid problems with process_queue resulting from me doing this too quickly.</p>
<li>Version 1.0 - December, 2006</li>
<p>This <b>is</b> essentially a complete rewrite of the old NMS module.  Everything has been cleaned up and attemtpts have been made to bring it up to proper standards, and lots of new features added. here is a list of the major improvements:</p>
<ul>
<li>Cleanup of the lang strings, etc.</li>
<li>Param-ize the queries for security</li>
<li>Added the concept of Jobs, so that messages can be re-used</li>
<li>Added the concept of username (optional)</li>
<li>Added smarty processing on templates</li>
<li>Added bulk import</li>
<li>Added the concept of a \&#039;private list\&#039;</p>
<li>Devided the admin panel into tabs</li>
<li>Show progress nicely when processing large jobs</li>
<li>Uses CMSMailer module</li>
<li>Events that can be trapped to add additional behaviour</li>
</ul>
<p><strong>Note</strong>, upgrade from the previous version <em>(Including previous betas)</em>is not possible.  A complete uninstall of the old version is required before installing this version of NMS.</p>
</li>
<li>Version .74 27 November 2005. Alpha 3 Release. Fixed bug with confirmation message returnid, image urls, windows php, and a few other small issues.</li>
<li>Version .73 23 November 2005. Alpha 2 Release. Fixed bug with adding messages.</li>
<li>Version .71 22 November 2005. Alpha 1 Release.</li>
<li>Version .5. 17 September 2005. Internal Release.</li>
</ul>';
$lang['helpselect'] = 'Specify a comma separated list of mailing lists to display.  <em>(Valid only in subscribe mode)</em>';
$lang['helpmode'] = 'Specify the mode of operation. options are:
<ul>
<li>subscribe (default) - display the subscribe form</li>
<li>unsubscribe - display the unsubscribe form</li>
<li>usersettings - display the usersettings form</li>
</ul>
';
$lang['help'] = '<h3>Newsletter Made Simple (NMS) </h3>
<h3><strong>What does it do?  </strong></h3>
<p>Enables you to have a e-mail newsletter system inside CMS Made Simple. </p>

<h3>How do I use it?</h3>
<ol>
<li>Install the module (which you have probably done by this point).</li>
<li>Grant appropriate permissions to the groups that will manage your lists. these include:
	<ul>
	<li>Manage NMS Lists &mdash; Add, remove mailing lists</li>	
	<li>Manage NMS Users &mdash; Add, remove users in the database</li>
	<li>Manage NMS Messages &mdash; Add, edit, remove messages but not send them</li> 	
	<li>Manage NMS Jobs &mdash; Send messages and perform other \&#039;job\&#039; functions</li>	
	</ul>
</li>
<li>Create a list</li>
<li>(Optional) Add the {cms_module module=\&#039;NMS\&#039;} to a page to enable web site visitors to sign up for your list.</li>
<li>(Optional) Add users to your list manually with the Users tab in the admin system</li>
<li>Create a message to be sent as part of a job</li>
<li>Create a job. You will select a message to send and a list to send it to.</li>
<li>Process the job and your message will be sent</li>
</ol>

<h3>Basic Syntax</h3>
<p>	{cms_module module=\&#039;NMS\&#039;}  </p>

<h3>Further options</h3>

<h4>Display unsubscribe form:</h4>
<p>{cms_module module=\&#039;NMS\&#039; mode=\&#039;unsubscribe\&#039;}</p>

<h4>Display the user settings page:</h4>
<p>{cms_module module=\&#039;NMS\&#039; mode=\&#039;usersettings\&#039;}</p>

<h4>Display an archive of past newsletters</h4>
<p>{cms_module module=\&#039;NMS\&#039; mode=\&#039;archive\&#039;}</p>
<ul>
<lh>Optional parameters for the archive:</lh>
<li>show=\&#039;#\&#039; where <code>#</code> is the number of past newsletters to show. If omitted, all are shown</li>
<li>sortby=\&#039;date\&#039; &mdash; <code>date</code> (default), <code>id</code> (message ID), or <code>subject</code></li>
<li>sortorder=\&#039;DESC\&#039; &mdash; either <code>DESC</code> (descending order, default) or <code>ASC</code> (ascending order)</li>
<li>dateformat=\&#039;%Y-%m-%d\&#039; &mdash; sets the date output format following the PHP strftime() syntax</li>
</ul>		

<h4>Import users from the Front End Users database</h4>
<p>Note: You must have a property defined in FEU that stores the users\&#039; email address. It can be named whatever you want.</p>
<ol>
<li>Choose Extensions > Newsletter Made Simple</li>
<li>Activate the Users tab</li>
<li>At the bottom, click Import Users from FrontEndUsers</li>
<li>Select the FEU field that contains the users\&#039; email addresses</li>
<li>Specify whether to copy the FEU user name to the NMS user name field. This is what would be output in your message if you included the {<span>$</span>username} placeholder.</li>
<li>Select the list to import to.</li>
<li>Click Submit.</li>
</ol>
<p>You will receive a report of the users processed, those already in your NMS database, those already on the list, and those that were added and subscribed. Users are not duplicated so there should be no harm in running an import periodically.</p>

<h3>Message variables</h3>
<p>These are covered elsewhere, but added here for completeness. If you add these to your message, they will be replaced at sending time with the appropriate true values.</p>

<ul>
<li>{<span>$</span>username}  &mdash;  The user\&#039;s name</li>
<li>{<span>$</span>email}  &mdash;  The user\&#039;s email address</li>
<li>{<span>$</span>unsubscribe}  &mdash;  A URL that can be used to display a page for unsubscribing</li>
<li>{<span>$</span>preferences}  &mdash;  A URL that can be used to display a user preferences page</li>
<li>{<span>$</span>confirmurl}  &mdash;  A URL that can be used to confirm subscriptions</li>
</ul>

<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Bug Report  please visit <a href=\&#039;http://dev.cmsmadesimple.org/projects/newsletter/\&#039; target=\&#039;_blank\&#039;>http://dev.cmsmadesimple.org/projects/newsletter/</a>.</li>
<li>Additional discussion of this module may also be found in the <a href=\&#039;http://forum.cmsmadesimple.org\&#039; target=\&#039;_blank\&#039;>CMS Made Simple Forums</a>.</li>
<li>The author, Paul Lemke, can often be found in the <a href=\&#039;irc://irc.freenode.net/#cms\&#039;>CMS IRC Channel</a> (username: pyrox).</li>
<li>Lastly, you may have some success emailing the author directly.</li>  
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text
of the license for the full disclaimer.</p>
<h3>Copyright and License</h3>
<p>Copyright &copy; 2005, Paul Lemke <a href=\&#039;mailto:lemkepf@gmx.net\&#039;><lemkepf@gmx.net></a>. All Rights Are Reserved.</p>
<p>This module has been released under the <a href=\&#039;http://www.gnu.org/licenses/licenses.html#GPL\&#039;>GNU Public License</a>. You must agree to this license before using the module.</p>
';
$lang['import_feu_title'] = 'Import Users from FrontEndUsers';
$lang['import_feu_info'] = 'To import users from the Front End Users tables, you must specify which FEU field contains the email address and whether to copy the contents of the FEU username field to the NMS username field.';
$lang['import_feu_noproperties'] = 'You have not defined any properties in the Front End Users module. You must define at least one property to store the users\&#039;s email addresses.';
$lang['import_feu_prompt_groupname'] = 'FrontEndUsers skupina';
$lang['import_feu_prompt_copyusername'] = 'Copy FEU user name to NMS?';
$lang['import_feu_info_copyusername'] = '<em>If No is selected, the username will be left blank.</em>';
$lang['import_feu_selectlists'] = 'Import users to (list name)';
$lang['import_feu_feunotinstalled'] = 'The Front End Users module is not installed or the properties definition table does not exist. Cannot import users from FEU.';
$lang['processedAddressesTitle'] = 'Processed addresses';
$lang['inDatabaseTitle'] = 'Already in NMS';
$lang['onListAlreadyTitle'] = 'Already subscribed';
$lang['addressSubscribedTitle'] = 'Subscribed';
$lang['listidInfo'] = 'Importing to list ID: ';
$lang['processedAddressesCountInfo'] = 'Addresses processed: ';
$lang['archive_heading'] = '<h1>Archiv zpr&aacute;v</h1>';
$lang['archivedmessage'] = '<h1>Archivovan&aacute; zpr&aacute;va</h1>';
$lang['archive_tbl_msgID'] = 'ID zpr&aacute;vy';
$lang['archive_tbl_subject'] = 'Předmět';
$lang['archive_tbl_fullurl'] = 'Message link';
$lang['archive_tbl_href'] = 'C&iacute;l odkazu';
$lang['archive_tbl_date'] = 'Datum';
$lang['utma'] = '156861353.1608855210.1167908109.1190792514.1190798663.130';
$lang['utmz'] = '156861353.1190793650.129.10.utmccn=(organic)|utmcsr=google|utmctr=cmsmadesimple translations|utmcmd=organic';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353';
?>
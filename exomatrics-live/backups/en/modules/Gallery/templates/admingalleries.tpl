{if $itemcount > 0}
{*	<div class="pageoptions">
		<p class="pageoptions">{$addlink}</p>
	</div>*}
	<table id="gtree" cellspacing="0" class="pagetable">
		<thead>
			<tr>
				<th>{$gallerypath}</th>
				<th class="pageicon">{$active}</th>			
				<th class="pageicon">{$actions}</th>
			</tr>
		</thead>
		<tbody>
	{foreach from=$items item=entry}
		{cycle values="row1,row2" assign=rowclass}
			{*
			<tr id="node-{$entry->id}" class="{$rowclass}{$entry->gidclass}" onmouseover="this.className='{$rowclass}hover{$entry->gidclass}';" onmouseout="this.className='{$rowclass}{$entry->gidclass}';">
			*}
			<tr id="node-{$entry->id}"{if !empty($entry->gidclass)} class="{$entry->gidclass|trim}"{/if}>
				<td class="tfile">{$entry->file}</td>
				<td class="pagepos" style="text-align:center">{$entry->activelink}</td>			
				<td class="pagepos" style="text-align:center">{$entry->editlink}</td>
			</tr>
	{/foreach}
		</tbody>
	</table>
{else}
	<h4>{$nogalleriestext}</h4>
{/if}
{*
<div class="pageoptions">
	<p class="pageoptions">{$addlink}</p>
</div>*}
<?php
if (!isset($gCms)) exit;

// Check permissions
if( !$this->CheckPermission('Use Gallery') )
{
	echo $this->ShowErrors($this->Lang('accessdenied'));
	return;
}

$file_id = '';
if ( isset($params['fid']) )
{
	$file_id = $params['fid'];
}


$query = "UPDATE " . cms_db_prefix() . "module_gallery SET active = active^1 WHERE fileid = ?";
$db->Execute($query, array($file_id));

$this->Redirect($id, $params['origaction'] , $returnid, $params);

?>
<?php

class cgextensions_datastore
{
  var $_module;
  var $_default_expiry;

  function cgextensions_datastore(&$mod)
  {
    $this->_module =& $mod;
    $this->_default_expiry = -1000;
  }

  function _calculate_expiry($seconds)
  {
    if( $seconds <= 0 )
      {
	return NULL;
      }
    
    global $gCms;
    $db =& $gCms->GetDb();
    return $db->DbTimeStamp(time()+$seconds);
  }

  function remove_expired()
  {
    global $gCms;
    $db =& $gCms->GetDb();
    $query = 'DELETE FROM '.CGEXTENSIONS_TABLE_ASSOCDATA.'
               WHERE expiry < NOW() AND expiry != -1';
    $db->Execute($query);
  }


  function erase($key1,$key2 = '',$key3 = '', $key4 = '')
  {
    global $gCms;
    $db =& $gCms->GetDb();
    $query = 'DELETE FROM '.CGEXTENSIONS_TABLE_ASSOCDATA.'
               WHERE key1 = ? AND key2 = ? AND key3 = ? AND key4 = ?';
    $db->Execute($query,array($key1,$key2,$key3,$key4));
  }


  function store($data,$expiry,$key1,$key2='',$key3='',$key4='')
  {
    if( empty($data) ) return FALSE;
    $exp = $this->_calculate_expiry($expiry);

    $this->erase($key1,$key2,$key3,$key4);

    $query = 'INSERT INTO '.CGEXTENSIONS_TABLE_ASSOCDATA."
                (key1,key2,key3,key4,data,expiry,create_date,modified_date)
              VALUES (?,?,?,?,?,$exp,NOW(),NOW())";
    global $gCms;
    $db =& $gCms->GetDb();
    $dbr = $db->Execute($query,array($key1,$key2,$key3,$key4,$data));
    if( !$dbr )
      {
	echo "FATAL: ".$db->sql."<br/>".$db->ErrorMsg().'<br/>'; die();
      }
  }
  

  function get($key1,$key2 = '',$key3 = '', $key4 = '')
  {
    $query = 'SELECT data FROM '.CGEXTENSIONS_TABLE_ASSOCDATA.'
               WHERE key1 = ? AND key2 = ? AND key3 = ? AND key4 = ?
                 AND expiry > NOW() ORDER BY modified_date LIMIT 1';
    global $gCms;
    $db =& $gCms->GetDb();
    $tmp = $db->GetOne($query,array($key1,$key2,$key3,$key4));
    if( !$tmp ) return FALSE;
    return $tmp;
  }


  function listall($key1,$key2 = '',$key3 = '')
  {
    $parms = array();
    $where[] = array();

    $where[] = 'key1 = ?';
    $parms[] = $key1;

    $query = 'SELECT key1,key2,key3,key4 FROM '.CGEXTENSIONS_TABLE_ASSOCDATA;
    if( !empty($key2) )
      {
	$where[] = 'key2 = ?';
	$parms[] = $key2;

	if( !empty($key3) )
	  {
	    $where[] = 'key3 = ?';
	    $parms[] = $key3;
	  }
      }

    if( count($where) )
      {
	$query .= ' WHERE ' + implode(' AND ',$where);
      }
    global $gCms;
    $db =& $gCms->GetDb();
    $data = $db->GetArray($query,$parms);
    if( !$data ) return FALSE;
    return $data;
  }

  
}

?>
<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: Newsletter Made Simple (c) 2008 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to provide a flexible
#  mailing list solution.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE


/*---------------------------------------------------------
 _DisplayAdminConfirmTab($id, $params, $returnid, $message)
 ---------------------------------------------------------*/
function _DisplayAdminConfirmFormTab(& $module, $id, & $params, $returnid)
{
	$module->smarty->assign('startform', $module->CreateFormStart($id, 'save_confirm_prefs', $returnid));
	$module->smarty->assign('prompt_post_email_confirm_text', $module->Lang('prompt_post_confirm_text'));
	$module->smarty->assign('post_email_confirm_text', $module->CreateTextArea(false, $id, $module->GetTemplate('post_email_confirm_message'), 'post_email_confirm_message'));
	$module->smarty->assign('prompt_confirm_email_body', $module->Lang('prompt_confirm_email_body'));
	$module->smarty->assign('confirm_email_body', $module->CreateTextArea(false, $id, $module->GetTemplate('confirm_email_body'), 'confirm_email_body'));
	$module->smarty->assign('prompt_confirm_subject', $module->Lang('prompt_confirm_subject'));
	$module->smarty->assign('confirm_subject', $module->CreateInputText($id, 'confirm_subject', $module->GetPreference('confirm_subject', '1'), 30));
	$module->smarty->assign('submit', $module->CreateInputSubmit($id, 'submit', $module->Lang('submit'), '', '', $module->Lang('confirm_adjustsettings')));
	$module->smarty->assign('reset', $module->CreateInputSubmit($id, 'input_reset', $module->Lang('resetdefaults'), '', '', $module->Lang('confirm_resetdefaults')));
	$module->smarty->assign('endform', $module->CreateFormEnd());
	echo $module->ProcessTemplate('confirmform.tpl');
}


/*---------------------------------------------------------
 _DisplayAdminPrefsTab($id, $params, $returnid, $message)
 ---------------------------------------------------------*/
function _DisplayAdminPrefsTab(& $module, $id, & $params, $returnid, $message= '')
{
	// simple way to have common nav available to all admin pages
	$module->smarty->assign('startform', $module->CreateFormStart($id, 'save_admin_prefs', $returnid));
	$module->smarty->assign('endform', $module->CreateFormEnd());
	$module->smarty->assign('submit', $module->CreateInputSubmit($id, 'submit', $module->Lang('submit')));
	if ($message != '')
	{
		$module->smarty->assign('message', $message);
	}
	$module->smarty->assign('prompt_users_per_page', $module->Lang('prompt_users_per_page'));
	$module->smarty->assign('users_per_page', $module->CreateInputText($id, 'users_per_page', $module->GetPreference('users_per_page', 25), 3, 3));
	$module->smarty->assign('prompt_messages_per_batch', $module->Lang('prompt_messages_per_batch'));
	$module->smarty->assign('messages_per_batch', $module->CreateInputText($id, 'messages_per_batch', $module->GetPreference('messages_per_batch', 50), 4, 4));
	$module->smarty->assign('prompt_ms_between_message_sleep', $module->Lang('prompt_ms_between_message_sleep'));
	$module->smarty->assign('ms_between_message_sleep', $module->CreateInputText($id, 'ms_between_message_sleep', $module->GetPreference('ms_between_message_sleep', 100), 4, 4));
	$module->smarty->assign('prompt_between_batch_sleep', $module->Lang('prompt_between_batch_sleep'));
	$module->smarty->assign('between_batch_sleep', $module->CreateInputText($id, 'between_batch_sleep', $module->GetPreference('between_batch_sleep', 5), 3, 3));
	$module->smarty->assign('prompt_email_user_on_admin_subscribe', $module->Lang('prompt_email_user_on_admin_subscribe'));
	$module->smarty->assign('email_user_on_admin_subscribe', $module->CreateInputCheckbox($id, 'email_user_on_admin_subscribe', 1, $module->GetPreference('email_user_on_admin_subscribe', 0)));
	$module->smarty->assign('prompt_send_admin_copies',
				$module->Lang('send_admin_copy'));
	$module->smarty->assign('send_admin_copies', $module->CreateInputCheckbox($id, 'send_admin_copies', 'true', $module->GetPreference('send_admin_copies', 'true')));

	$module->smarty->assign('prompt_admin_email',$module->Lang('admin_email'));
	$module->smarty->assign('admin_email', $module->CreateInputText($id, 'admin_email', $module->GetPreference('admin_email', ''), 50));
	$module->smarty->assign('prompt_admin_name',$module->Lang('admin_name'));
	$module->smarty->assign('prompt_admin_replyto',$module->Lang('admin_replyto'));
	$module->smarty->assign('admin_name', $module->CreateInputText($id, 'admin_name', $module->GetPreference('admin_name', ''), 50));
	$module->smarty->assign('admin_replyto', $module->CreateInputText($id, 'admin_replyto', $module->GetPreference('admin_replyto'), 50));

	$module->smarty->assign('prompt_pop3_server',$module->Lang('pop3_server'));
	$module->smarty->assign('pop3_server',
				$module->CreateInputText($id,'pop3_server',
						       $module->GetPreference('pop3_server'),50));
	$module->smarty->assign('prompt_pop3_username',$module->Lang('pop3_username'));
	$module->smarty->assign('pop3_username',
				$module->CreateInputText($id,'pop3_username',
						       $module->GetPreference('pop3_username'),50));
	$module->smarty->assign('prompt_pop3_password',$module->Lang('pop3_password'));
	$module->smarty->assign('pop3_password',
				$module->CreateInputPassword($id,'pop3_password',
						       $module->GetPreference('pop3_password')));

	$module->smarty->assign('prompt_bounce_limit',
				$module->Lang('bounce_limit'));
	$module->smarty->assign('bounce_limit',
				$module->CreateInputText($id,'bounce_limit',
						       $module->GetPreference('bounce_limit',10),3,3));
	$module->smarty->assign('info_bounce_limit',
				$module->Lang('info_bounce_limit'));
	$module->smarty->assign('prompt_bounce_messagelimit',
				$module->Lang('bounce_messagelimit'));
	$module->smarty->assign('info_bounce_messagelimit',
				$module->Lang('info_bounce_messagelimit'));
	$module->smarty->assign('bounce_messagelimit',
				$module->CreateInputText($id,'bounce_messagelimit',
						       $module->GetPreference('bounce_messagelimit',500),4,4));

	$module->smarty->assign('prompt_message_charset',
				$module->Lang('message_charset'));
	$module->smarty->assign('message_charset', $module->CreateInputText($id, 'message_charset', $module->GetPreference('message_charset', '1'), 50));
	// Display the populated template
	echo $module->ProcessTemplate('adminprefs.tpl');
}
/*---------------------------------------------------------
 _DisplayAdminSubscribeFormTab($id, $params, $returnid, $message)
 ---------------------------------------------------------*/
function _DisplayAdminSubscribeFormTab(& $module, $id, & $params, $returnid, $message= '')
{
	$module->smarty->assign('startform', $module->CreateFormStart($id, 'save_subscribe_prefs', $returnid));
	$module->smarty->assign('endform', $module->CreateFormEnd());
	$module->smarty->assign('submit', $module->CreateInputSubmit($id, 'submit', $module->Lang('submit'), '', '', $module->Lang('confirm_adjustsettings')));
	$module->smarty->assign('reset', $module->CreateInputSubmit($id, 'input_reset', $module->Lang('resetdefaults'), '', '', $module->Lang('confirm_resetdefaults')));
	$module->smarty->assign('prompt_postsubscribetext', $module->Lang('prompt_postsubscribetext'));
	$module->smarty->assign('prompt_subscribesubject', $module->Lang('prompt_subscribesubject'));
	$module->smarty->assign('prompt_subscribe_email_body', $module->Lang('prompt_subscribe_email_body'));
	$module->smarty->assign('prompt_subscribe_form', $module->Lang('prompt_subscribe_form'));
	$module->smarty->assign('postsubscribetext', $module->CreateTextArea(false, $id, $module->GetPreference('subscribe_posttext', ''), 'postsubscribetext', "", "", 10, 10));
	$module->smarty->assign('subscribe_subject', $module->CreateInputText($id, 'subscribe_subject', $module->GetPreference('subscribe_subject', '1'), 30));
	$module->smarty->assign('subscribe_email_body', $module->CreateTextArea(false, $id, $module->GetTemplate('subscribe_email_body'), 'subscribe_email_body', "", "", "", "", 10, 10));
	$module->smarty->assign('subscribe_form', $module->CreateTextArea(false, $id, $module->GetTemplate('nms_subscribeform'), 'subscribe_form'));
	echo $module->ProcessTemplate('subscribeform.tpl');
}

/*---------------------------------------------------------
 _DisplayAdminUnsubscribeFormTab($id, $params, $returnid, $message)
 ---------------------------------------------------------*/
function _DisplayAdminUnsubscribeFormTab(& $module, $id, & $params, $returnid, $message= '')
{
	$module->smarty->assign('startform', $module->CreateFormStart($id, 'save_unsubscribe_prefs', $returnid));
	$module->smarty->assign('endform', $module->CreateFormEnd());
	$module->smarty->assign('submit', $module->CreateInputSubmit($id, 'submit', $module->Lang('submit'), '', '', $module->Lang('confirm_adjustsettings')));
	$module->smarty->assign('reset', $module->CreateInputSubmit($id, 'input_reset', $module->Lang('resetdefaults'), '', '', $module->Lang('confirm_resetdefaults')));
	$module->smarty->assign('prompt_unsubscribe_prompt', $module->Lang('prompt_unsubscribe_prompt'));
	$module->smarty->assign('prompt_unsubscribe_text', $module->Lang('prompt_unsubscribe_text'));
	$module->smarty->assign('prompt_unsubscribe_subject', $module->Lang('prompt_unsubscribe_subject'));
	$module->smarty->assign('prompt_unsubscribe_email_body', $module->Lang('prompt_unsubscribe_email_body'));
	$module->smarty->assign('prompt_unsubscribe_form', $module->Lang('prompt_unsubscribe_form'));
	$module->smarty->assign('prompt_unsubscribe_text', $module->Lang('prompt_unsubscribe_text'));
	$module->smarty->assign('prompt_post_unsubscribe_text', $module->Lang('prompt_post_unsubscribe_text'));
	$module->smarty->assign('unsubscribe_prompt', $module->CreateInputText($id, 'unsubscribe_prompt', $module->GetPreference('nms_unsubscribe_prompt'), 30));
	$module->smarty->assign('unsubscribe_subject', $module->CreateInputText($id, 'unsubscribe_subject', $module->GetPreference('unsubscribe_subject'), 30));
	$module->smarty->assign('unsubscribe_email_body', $module->CreateTextArea(false, $id, $module->GetTemplate('unsubscribe_email_body'), 'unsubscribe_email_body'));
	$module->smarty->assign('unsubscribe_form', $module->CreateTextArea(false, $id, $module->GetTemplate('nms_unsubscribeform'), 'unsubscribe_form'));
	$module->smarty->assign('unsubscribe_text', $module->CreateTextArea(false, $id, $module->GetTemplate('unsubscribe_text'), 'unsubscribe_text'));
	$module->smarty->assign('post_unsubscribe_text', $module->CreateTextArea(false, $id, $module->GetTemplate('post_unsubscribe_text'), 'post_unsubscribe_text'));
	echo $module->ProcessTemplate('unsubscribeform.tpl');
}
/*---------------------------------------------------------
 _DisplayAdminUserSettingsTab($id, $params, $returnid, $message)
 ---------------------------------------------------------*/
function _DisplayAdminUserSettingsTab(& $module, $id, & $params, $returnid)
{
	$module->smarty->assign('startform', $module->CreateFormStart($id, 'save_usersettings_prefs', $returnid));
	$module->smarty->assign('prompt_usersettings_form', $module->Lang('prompt_usersettings_form'));
	$module->smarty->assign('usersettings_form', $module->CreateTextArea(false, $id, $module->GetTemplate('usersettings_form'), 'usersettings_form'));
	global $gCms;
	$contentops= & $gCms->GetContentOperations();
	$module->smarty->assign('prompt_usersettings_page', $module->Lang('prompt_usersettings_page'));
	$module->smarty->assign('info_usersettings_page', $module->Lang('info_usersettings_page'));
	$module->smarty->assign('usersettings_page', create_page_dropdown($module, $id, 'usersettings_page', $module->GetPreference('usersettings_page')));
	$module->smarty->assign('prompt_usersettings_form2', $module->Lang('prompt_usersettings_form2'));
	$module->smarty->assign('usersettings_form2', $module->CreateTextArea(false, $id, $module->GetTemplate('usersettings_form2'), 'usersettings_form2'));
	$module->smarty->assign('prompt_usersettings_text', $module->Lang('prompt_usersettings_text'));
	$module->smarty->assign('usersettings_text', $module->CreateTextArea(false, $id, $module->GetTemplate('usersettings_text'), 'usersettings_text'));
	$module->smarty->assign('prompt_usersettings_text2', $module->Lang('prompt_usersettings_text2'));
	$module->smarty->assign('usersettings_text2', $module->CreateTextArea(false, $id, $module->GetTemplate('usersettings_text2'), 'usersettings_text2'));
	$module->smarty->assign('prompt_usersettings_subject', $module->Lang('prompt_usersettings_subject'));
	$module->smarty->assign('usersettings_subject', $module->CreateInputText($id, 'usersettings_subject', $module->GetPreference('usersettings_subject', ''), 30));
	$module->smarty->assign('prompt_usersettings_email_body', $module->Lang('prompt_usersettings_email_body'));
	$module->smarty->assign('usersettings_email_body', $module->CreateTextArea(false, $id, $module->GetTemplate('usersettings_email_body'), 'usersettings_email_body'));
	$module->smarty->assign('submit', $module->CreateInputSubmit($id, 'submit', $module->Lang('submit'), '', '', $module->Lang('confirm_adjustsettings')));
	$module->smarty->assign('reset', $module->CreateInputSubmit($id, 'input_reset', $module->Lang('resetdefaults'), '', '', $module->Lang('confirm_resetdefaults')));
	$module->smarty->assign('endform', $module->CreateFormEnd());
	echo $module->ProcessTemplate('usersettingsform.tpl');
}



?>

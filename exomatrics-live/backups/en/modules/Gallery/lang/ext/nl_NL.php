<?php
$lang['friendlyname'] = 'Gallery ';
$lang['moddescription'] = 'De eenvoudigste manier om foto-galerie&euml;n te beheren en weer te geven.';
$lang['description'] = 'Een gebruikersvriendelijke foto-galerie die automatisch de foto&#039;s uit een specifieke directory weergeeft.';
$lang['postinstall'] = 'De Gallery module is ge&iuml;nstalleerd.';
$lang['installed'] = 'De Gallery module versie %s is ge&iuml;nstalleerd.';
$lang['upgraded'] = 'De Gallery module is bijgewerkt naar versie %s.';
$lang['postuninstall'] = 'De Gallery module is verwijderd';
$lang['uninstalled'] = 'De Gallery module is verwijderd';
$lang['really_uninstall'] = 'Weet u zeker dat u de Gallery module wilt verwijderen? Dit heeft geen invloed op foto&#039;s, maar titels, omschrijvingen en alle miniaturen die door Gallery waren gegenereerd zullen verloren gaan.';
$lang['accessdenied'] = 'Toegang geweigerd. Controleer uw rechten.';
$lang['actions'] = 'Acties';
$lang['active'] = 'Actief';
$lang['apply'] = 'Toepassen';
$lang['areyousure'] = 'Weet u zeker dat u wilt verwijderen?';
$lang['cancel'] = 'Annuleer';
$lang['copy'] = 'Kopieer';
$lang['default'] = 'Standaard';
$lang['delete'] = 'Verwijder';
$lang['edit'] = 'Wijzig';
$lang['error'] = 'Fout!';
$lang['setfalse'] = 'Zet op Onwaar (false)';
$lang['settrue'] = 'Zet op Waar (true)';
$lang['submit'] = 'Opslaan';
$lang['galleries'] = 'Galerie&euml;n';
$lang['gallerypath'] = 'Galerie';
$lang['editgallery'] = 'Bewerk galerie';
$lang['addgallery'] = 'Voeg een galerie toe';
$lang['nogalleriestext'] = 'Geen galerie&euml;n beschikbaar';
$lang['item'] = 'Afbeelding';
$lang['title'] = 'Titel';
$lang['date'] = 'Datum';
$lang['nofilestext'] = 'Geen foto&#039;s aanwezig';
$lang['gallerytitle'] = 'Galerie Titel';
$lang['comment'] = 'Omschrijving';
$lang['template'] = 'Sjabloon';
$lang['hideparentlink'] = 'Verberg link naar bovenliggende galerie';
$lang['usedefault'] = 'gebruik standaard';
$lang['sortbysettings'] = 'Sorteer conform sjablooninstellingen';
$lang['galleryupdated'] = 'De galerie is succesvol bijgewerkt.';
$lang['error_updategalleryfailed'] = 'Bijwerken van de galerie is mislukt.';
$lang['templates'] = 'Sjablonen';
$lang['title_available_templates'] = 'Beschikbare Sjablonen';
$lang['prompt_name'] = 'Naam';
$lang['prompt_default'] = 'Standaard';
$lang['prompt_newtemplate'] = 'Maak een nieuw sjabloon';
$lang['title_template'] = 'Sjabloon Editor';
$lang['prompt_templatename'] = 'Sjabloon Naam';
$lang['thumbnailsize'] = 'Miniatuur afmetingen (Frontend)';
$lang['leaveempty'] = '(laat dit leeg om de standaard miniaturen uit Image Manager te gebruiken)';
$lang['width'] = 'breedte';
$lang['height'] = 'hoogte';
$lang['resizemethod'] = 'herschaal methode';
$lang['crop'] = 'bijsnijden';
$lang['scale'] = 'schalen';
$lang['zoomcrop'] = 'inzoomen &amp; bijsnijden';
$lang['zoomscale'] = 'inzoomen &amp; schalen';
$lang['maxnumber'] = 'Maximum aantal afbeeldingen per pagina';
$lang['showallimages'] = 'Laat dit leeg om alle afbeeldingen te tonen, zonder paginalinks';
$lang['sortingoptions'] = 'Sorteer Opties';
$lang['specifysortfields'] = 'Geef de velden aan waarop gesorteerd moet worden';
$lang['ascending'] = 'oplopend';
$lang['descending'] = 'aflopend';
$lang['addfield'] = 'Voeg veld toe';
$lang['deletefield'] = 'Verwijder laatste regel';
$lang['prompt_template'] = 'Sjabloon Code';
$lang['prompt_templatejs'] = 'Sjabloon JavaScript';
$lang['prompt_templatecss'] = 'Sjabloon CSS-stylesheet';
$lang['resetoriginal'] = 'Reset naar origineel';
$lang['resetoriginalwarning'] = 'Weet u zeker dat u de template-code wilt resetten naar de oorspronkelijke code?';
$lang['templateupdated'] = 'Het sjabloon is succesvol bijgewerkt.';
$lang['templateadded'] = 'Het sjabloon is toegevoegd.';
$lang['error_templatenameexists'] = 'De sjabloon-naam bestaat reeds.';
$lang['templatedeleted'] = 'Sjabloon verwijderd';
$lang['availablevariables'] = 'Beschikbare Sjabloon Smarty Variabelen';
$lang['availablevariableslist'] = 'Onderstaande variabelen kunnen gebruikt worden in de sjablonen:<br /><br />
<code>{$module_message}</code> - foutmelding, heeft alleen inhoud als er wat te melden is<br />
<code>{$gallerytitle}</code> - titel van de galerie. Als er geen titel is ingevuld, dan wordt de naam van de directory getoond<br />
<code>{$gallerycomment}</code> - omschrijving van de galerie<br />
<code>{$parentlink}</code> - link naar de bovenliggende galerie<br />
<code>{$hideparentlink}</code> - waar/onwaar<br />
<code>{$imagecount}</code> - aantal, toont bijvoorbeeld &quot;6 foto&#039;s&quot;, afhankelijk van de taal<br />
<code>{$itemcount}</code> - totaal aantal items, dus foto&#039;s + subgalerie&euml;n<br />
<code>{$pages}</code> - aantal pagina&#039;s<br />
<code>{$prevpage}</code> - link naar de vorige pagina, indien van toepassing<br />
<code>{$nextpage}</code> - link naar de volgende pagina, indien van toepassing<br />
<code>{$pagelinks}</code> - links naar elke beschikbare pagina<br />
<code>{$images}</code> - array met de volgende keys:<br />
 - <code>file</code> - relatief pad naar de originele foto (of subgalerie)<br />
 - <code>title</code> - titel van de foto. Als er geen titel is ingevuld, dan wordt de file-naam getoond<br />
 - <code>comment</code> - omschrijving van de foto<br />
 - <code>filedate</code> - aanmaakdatum/tijd<br />
 - <code>thumb</code> - relatief pad naar de miniatuur. In geval van een subgalerie, dan verwijst dit naar de miniatuur van de foto die gemarkeerd is als default in de betreffende subgalerie. Als er geen defaultwaarde aan een foto is toegekend, dan wordt een standaard folder-icon gebruikt.<br />
 - <code>fileid</code> - unieke id voor de foto of subgalerie<br />
 - <code>isdir</code> - is waar als het item een subgalerie betreft<br />';
$lang['options'] = 'Opties';
$lang['allowed_extensions'] = 'Toegestane Extensies';
$lang['use_comment_wysiwyg'] = 'Gebruik een WYSIWYG editor voor het Gallery omschrijving veld';
$lang['optionsupdated'] = 'De opties zijn succesvol bijgewerkt.';
$lang['image'] = 'afbeelding';
$lang['images'] = 'foto&#039;s';
$lang['prevpage'] = 'vorige';
$lang['nextpage'] = 'volgende';
$lang['defaultgallerycomment'] = 'Bedankt voor het installeren van de Gallery module. Als u wat foto&#039;s ge&uuml;pload heeft naar de map &#039;uploads/images/Gallery&#039;, dan zijn deze foto&#039;s hieronder zichtbaar. Via de admin-sectie kunnen titels, omschrijvingen en afmetingen voor miniatuur-afbeeldingen worden ingesteld. Ontdek alle overige mogelijkheden die deze module te bieden heeft in de module-help.';
$lang['help_dir'] = 'Parameter om een directory te specificeren, het pad is relatief t.o.v. uploads/images/Gallery/';
$lang['help_template'] = 'Gebruik een aparte database sjabloon voor de weergave van de foto galerie. Dit sjabloon moet bestaan en zichtbaar zijn in de tab Sjablonen in de Gallery admin, maar het hoeft niet de standaard sjabloon te zijn. Als deze parameter niet opgegeven is, wordt het sjabloon gebruikt welke is toegekend aan de galerie, anders wordt het standaard sjabloon gebruikt';
$lang['help_number'] = 'Maximaal aantal foto&#039;s dat getoond wordt (per pagina). Standaard worden alle foto&#039;s getoond.';
$lang['help_start'] = 'Begin weergave bij de zoveelste foto. Standaard wordt begonnen met de eerste foto';
$lang['help_show'] = 'Bepaal welke foto&#039;s moeten worden getoond. Mogelijke waarden zijn:
<ul>
<li>&#039;active&#039; - om alle foto&#039;s weer te geven die zijn gemarkeerd als actief (standaard)</li>
<li>&#039;inactive&#039; - om alleen de foto&#039;s die gemarkeerd zijn als inactief weer te geven</li>
<li>&#039;all&#039; - laat alle foto&#039;s zien</li>
</ul>
';
$lang['help_category'] = 'Toon alleen foto&#039;s/galerie&euml;n die toegekend zijn aan de opgegeven categori&euml;n. Door een * achter de directory-naam te plaatsen, worden ook subcategorie&euml;n weergegeven. Meerdere categorie&euml;n kunnen worden opgegeven, mits gescheiden met een komma. Standaard worden alle categorie&euml;n getoond.';
$lang['help_action'] = 'Bepaal de uit te voeren actie. Gebruik dit in combinatie met de bovengenoemde parameters. Mogelijke waarden zijn:
<ul>
<li>&#039;showrandom&#039; - Toont een opgegeven aantal willekeurige miniaturen. (Alleen van toepassing op foto&#039;s die zijn opgeslagen in de database, standaard ingesteld op 6 foto&#039;s). Door bij de dir parameter &#039;/*&#039; achter de directory-naam te plaatsen, worden ook de foto&#039;s uit subdirectories meegenomen</li>
<li>&#039;showlatest&#039; - Toont de laatst toegevoegde foto&#039;s. (Alleen van toepassing op foto&#039;s die zijn opgeslagen in de database, standaard ingesteld op 6 foto&#039;s) Als de dir parameter niet opgegeven is, dan wordt de Gallery-root gebruikt. Subdirectories worden automatisch meegenomen</li>
<li>&#039;showlatestdir&#039; - Toont een opgegeven aantal willekeurige miniaturen uit de laatst toegevoegde directory (Alleen van toepassing op foto&#039;s die zijn opgeslagen in de database, standaard ingesteld op 6 foto&#039;s)</li>
</ul>
Merk op dat afbeeldingsgegevens pas in de database worden opgeslagen op het moment dat de Gallery module in de admin-sectie bezocht wordt.';
$lang['changelog'] = '<ul> 
<li>Version 1.1.2  5 December 2009. Update Fancybox system to version 1.2.6, Fix for Bugreport #4422: Error when sortingoption is empty, Built in check for templates/css directory (BR #4305).</li>
<li>Version 1.1.1. 12 November 2009. Fix for Bugreport #4294: Thumbfolder not created when upgrading.</li>
<li>Version 1.1. 8 November 2009. Added sorting and drag&amp;drop feature (FR #3907 #4210), Moved thumbnailsettings to templatestab, Make galleries searchable (FR #3956), Create backend thumbs if they don&#039;t exist in IM (FR #4101), Fix for BR #4027, Added fileid to image-object (FR #4259)</li>
<li>Version 1.0.3. 8 September 2009. Bugfix for [#4001], Improved synchronizing the database to files, and other minor improvements.</li>
<li>Version 1.0.2. 5 September 2009. Bugfix for [#3983] regarding spaces in directory names, Added templates Fancybox and Lytebox_slideshow as in Feature requests [#3944] and [#3967]</li>
<li>Version 1.0.1. 23 August 2009. Apply thumbnail-settings on &#039;Album-covers&#039; [#3924], Bugfix for [#3927], Added missing /div to thickbox-template, fixed an issue with generating thumbnails when no thumbs available in ImageManager, improved the naming of thumbs, Bugfix for [#3931], Prevent display the content of galleries that are set as inactive.</li>
<li>Version 1.0. 17 August 2009. Pagination parameters added, Standard templates changed accordingly, Extended the module-help, Added functions to show random or latest images, Support of pretty-urls, Various improvements</li>
<li>Version 0.6. 9 August 2009. Initial Beta Release.</li>
</ul>';
$lang['help'] = '<h3>Wat doet dit?</h3>
<p>De Gallery module is een gebruikersvriendelijk foto-album die automatisch de foto&#039;s uit een opgegeven directory toont.
Subdirectories worden getoont als sub-galerie&euml;n. De module heeft veel ingebouwde mogelijkheden, zoals het automatisch genereren van miniaturen, het gebruik van meerdere Lightbox-achtige sjablonen of elk ander gewenste css/javascript-sjabloon, en aan zowel galerie&euml;n als foto&#039;s kunnen titels en omschrijvingen worden toegekend.</p>
<h3>Hoe gebruik ik het</h3>
<p>Allereerst moet de {Gallery} tag in een pagina of sjabloon geplaatst worden. Daarna kunnen foto&#039;s via de Image Manager, File Manager (bijv. meerdere foto&#039;s uploaden in &eacute;&eacute;n zip-bestand), of FTP naar de map uploads/images/Gallery/ ge&uuml;pload worden.</p>
<p>Dat is alles!</p>
<p>Als meer galerie&euml;n gewenst zijn, cre&euml;er dan subdirectories en upload er foto&#039;s naartoe zoals omschreven. Door parameters toe te voegen aan de {Gallery} tag, kan eenvoudig ingesteld worden welke subgalerie getoond moet worden met welk sjabloon, bijv. {Gallery dir=&quot;vakanties/Veluwe2009&quot; template=&quot;Lightbox&quot;}</p>
<p>Standaard worden de miniaturen van de Image Manager gebruikt bij de weergave van galerie&euml;n.</p>
<h3>Geavanceerde opties, maar nog steeds eenvoudig te gebruiken</h3>
<p>In de admin sectie van de module zijn de volgende opties te vinden:</p>
<ul>
<li>Markeer een foto als standaard voor een galerie, om deze miniatuur te tonen in de bovenliggende galerie, als &#039;album-cover&#039; in plaats van het standaard-ikoon.</li>
<li>Geef titels en omschrijvingen aan galerie&euml;n.</li>
<li>Stel een standaard sjabloon in voor elke galerie.</li>
<li>Geef miniatuur-afmetingen op voor elk sjabloon, met mogelijkheid om foto&#039;s te schalen, bij te snijden en/of in te zoomen.</li>
<li>Geef voor elk sjabloon aan in welke volgorde de foto&#039;s moeten worden gesorteerd.</li>
<li>Sorteer foto&#039;s handmatig met de drag&amp;drop functie</li>
<li>Geef titels en omschrijvingen aan foto&#039;s.</li>
<li>Zet een foto of galerie inactief om weergave te voorkomen.</li>
<li>Bewerk/kopi&euml;er of maak nieuwe sjablonen. Klik op het info-icoon onder het veld met sjabloon-code voor een opsomming van de beschikbare variabelen.</li>
</ul>
<p>Om sjablonen te kunnen bewerken, moet een gebruiker lid zijn van een groep met &#039;Modify Templates&#039; rechten. Om de Gallery opties in te stellen, moet een gebruiker tot een groep behoren die &#039;Modify Site Preferences&#039; rechten heeft.</p>
<p>Alle titels, omschrijvingen en instellingen worden opgeslagen in de database. De database wordt gesynchroniseerd met de filedirectory telkens wanneer de betreffende galerie wordt bezocht in de Gallery-admin. Een kleine waarschuwing: de titel, omschrijving en instellingen worden verwijderd als een foto of galerie wordt verplaatst naar een andere directory.</p>
<h3>Ondersteuning</h3>
<p>Deze module geniet geen commerci&euml;le ondersteuning. Er zijn echter meerdere mogelijkheden om hulp te krijgen:</p>
<ul>
<li>Bezoek de Module Forge
<a href="http://dev.cmsmadesimple.org/projects/gallery/">Gallery pagina</a> voor de laatste versie van deze module, of om een &#039;feature request&#039; of &#039;bug&#039; te rapporteren.</li>
<li>Aanvullende bespreking van deze module kan ook worden gevonden in de <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.</li>
<li>Tot slot kunt u wellicht enig succes boeken door de auteur rechtstreeks te mailen.</li>  
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>

<h3>Copyright and License</h3>
<p>Copyright &copy; 2009, Jos <a href="mailto:josvd@live.nl">-josvd@live.nl-</a>. All Rights Are Reserved.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>
';
$lang['utmz'] = '156861353.1255299530.423.75.utmcsr=dev.cmsmadesimple.org|utmccn=(referral)|utmcmd=referral|utmcct=/project/files/6';
$lang['utma'] = '156861353.1423585787.1205867177.1257589321.1257674230.449';
$lang['utmc'] = '156861353';
?>
<?php
#-------------------------------------------------------------------------
# Module: Gallery
# Version: 1.1.2, Jos
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/gallery/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------


define('DEFAULT_GALLERY_PATH', "uploads/images/Gallery/");
define('DEFAULT_GALLERYTHUMBS_PATH', "uploads/images/GalleryThumbs/");
define('IM_PREFIX', "thumb_");   // $IMConfig['thumbnail_prefix']
define('TEMPLATE_SEPARATOR', "{*----------");


class Gallery extends CMSModule
{

  function GetName()
  {
    return 'Gallery';
  }

  function GetFriendlyName()
  {
    return $this->Lang('friendlyname');
  }


  function GetVersion()
  {
    return '1.1.2';
  }

  function GetHelp()
  {
    return $this->Lang('help');
  }

  function GetAuthor()
  {
    return 'Jos';
  }

  function GetAuthorEmail()
  {
    return 'josvd@live.nl';
  }

  function GetChangeLog()
  {
    return $this->Lang('changelog');
  }

  function IsPluginModule()
  {
    return true;
  }

  function HasAdmin()
  {
    return true;
  }

  function GetAdminSection()
  {
    return 'content';
  }

  function GetAdminDescription()
  {
    return $this->Lang('moddescription');
  }

  function VisibleToAdminUser()
  {
    return $this->CheckPermission('Use Gallery');
  }

  function GetDependencies()
  {
    return array();
  }

  function MinimumCMSVersion()
  {
    return "1.6";
  }

   /*
  function MaximumCMSVersion()
  {
    return "1.99";
  }
  */

  function SetParameters()
  {
	  $this->RegisterModulePlugin();

	  $this->RestrictUnknownParams();

	  // syntax for creating a parameter is parameter name, default value, description
	  $this->CreateParameter('dir', 'sub1/sub2', $this->Lang('help_dir'));
	  $this->SetParameterType('dir',CLEAN_STRING);

	  $this->CreateParameter('template', '', $this->Lang('help_template'));
	  $this->SetParameterType('template',CLEAN_STRING);

	  $this->CreateParameter('number', '100', $this->Lang('help_number'));
	  $this->SetParameterType('number',CLEAN_INT);

	  $this->CreateParameter('start', '1', $this->Lang('help_start'));
	  $this->SetParameterType('start',CLEAN_INT);

	  $this->CreateParameter('show', 'all', $this->Lang('help_show'));
	  $this->SetParameterType('show',CLEAN_STRING);
/*
	  $this->CreateParameter('category', '', $this->Lang('help_category'));
	  $this->SetParameterType('category',CLEAN_STRING);
*/
	  $this->CreateParameter('action', 'default', $this->Lang('help_action'));
	  $this->SetParameterType('action',CLEAN_STRING);

	  $this->SetParameterType('fid',CLEAN_INT);
	  $this->SetParameterType('gid',CLEAN_INT);

	  // Pretty url route for viewing a gallery in all its variations
	  $this->RegisterRoute('/gallery\/(?P<dir>.+)\/(?P<start>[0-9]+)-(?P<number>[0-9]+)-(?P<show>[a-zA-Z]+)-(?P<returnid>[0-9]+)$/', array('action'=>'default'));
		$this->RegisterRoute('/gallery\/(?P<dir>.+)\/(?P<start>[0-9]+)-(?P<number>[0-9]+)-(?P<returnid>[0-9]+)$/', array('action'=>'default'));
	  $this->RegisterRoute('/gallery\/(?P<dir>.+)\/(?P<returnid>[0-9]+)$/', array('action'=>'default'));
		$this->RegisterRoute('/gallery\/(?P<start>[0-9]+)-(?P<number>[0-9]+)-(?P<show>[a-zA-Z]+)-(?P<returnid>[0-9]+)$/', array('action'=>'default'));
		$this->RegisterRoute('/gallery\/(?P<start>[0-9]+)-(?P<number>[0-9]+)-(?P<returnid>[0-9]+)$/', array('action'=>'default'));
		$this->RegisterRoute('/gallery\/(?P<returnid>[0-9]+)$/', array('action'=>'default'));

  }

	function ContentPostRender(&$content) {
	  $pos = stripos($content,"</head");
	  if( $pos !== FALSE && isset($this->GalleryMetadata) )
	  {
		  $content = substr($content, 0, $pos) . $this->GalleryMetadata . substr($content, $pos);
		}
	  return $content;
	}

  function GetEventDescription ( $eventname )
  {
    return; // $this->Lang('event_info_'.$eventname );
  }

  function GetEventHelp ( $eventname )
  {
    return; // $this->Lang('event_help_'.$eventname );
  }

  function InstallPostMessage()
  {
    return $this->Lang('postinstall');
  }

  function UninstallPostMessage()
  {
    return $this->Lang('postuninstall');
  }

  function UninstallPreMessage()
  {
    return $this->Lang('really_uninstall');
  }


	/**
	 * Search methods
	 */

	function SearchResult($returnid, $gid, $attr = '')
	{
		$result = array();

		if ($attr == 'gallery')
		{
			$galleryinfo = $this->_Getgalleryinfobyid($gid);
			if ( $galleryinfo && $galleryinfo['active'] )
			{
				//0 position is the prefix displayed in the list results.
				$result[0] = $this->GetFriendlyName();

				//1 position is the title
				$result[1] = empty($galleryinfo['title']) ? trim($galleryinfo['filename'],"/") : $galleryinfo['title'];

				//2 position is the URL to the title.
				$gdir = $gid == 1 ? '' : str_replace('%2F','/',rawurlencode((empty($galleryinfo['filepath']) ? '' : $galleryinfo['filepath'] . '/') . $galleryinfo['filename']));
				$prettyurl = 'gallery/' . $gdir . $returnid;
				$result[2] = $this->CreateLink('cntnt01', 'default', $returnid, '', array('dir' => trim($gdir,'/')) ,'', true, false, '', true, $prettyurl);
			}
		}
		return $result;
	}


	function SearchReindex(&$module)
	{
		$galleries = $this->_GetGalleries();
		
		foreach ($galleries as $gid=>$gallery) 
		{
			$galleryinfo = $this->_Getgalleryinfobyid($gid);
			if ( $galleryinfo['active'] )
			{
				$searchwords = $gallery['title'] . ' ' . $gallery['comment'];
				$db =& $this->GetDB();
				$query = "SELECT title, comment 
									FROM " . cms_db_prefix() . "module_gallery g1
									WHERE galleryid=?";
				$result = $db->Execute($query, array($gid));
				if ( $result && $result->RecordCount() > 0 )
				{
					while ( $row=$result->FetchRow() )
					{
						$searchwords .= ' ' . $row['title'] . ' ' . $row['comment'];
					}
				}
				if ( !$result )
				{
					echo 'ERROR: ' . mysql_error();
					exit();
				}
				$module->AddWords($this->GetName(), $gid, 'gallery',$searchwords);
			}
		}
	}

	
	function GetHeaderHTML()
	{
		$tmpl = <<<EOT
<link rel="stylesheet" type="text/css" href="../modules/Gallery/templates/jquery/jquery.treeTable.css" media="screen" />
{literal}
<script type="text/javascript">
if (typeof jQuery == 'undefined') {
    var fileref = document.createElement("script")
    fileref.setAttribute("type","text/javascript")
    fileref.setAttribute("src", "../modules/Gallery/templates/jquery/jquery.js")
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}
</script>
<script type="text/javascript" src="../modules/Gallery/templates/jquery/jquery.tablednd.js"></script>
<script type="text/javascript" src="../modules/Gallery/templates/jquery/jquery.treeTable.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#gtree").treeTable();
	$('#gtree tr.initialized:odd').addClass('row1');
	$('#gtree tr.initialized:even').addClass('row2');
	$('#gtable tr, #gtree tr').hover(function(){
		$(this).stop().addClass('row1hover');
	}, function(){
		$(this).stop().removeClass('row1hover');
	});
	$('#gtable').tableDnD({
		onDragStart: function(table,row) {
			$('#gtable tr').removeClass();
		},
		onDragClass: "row1hover",
		onDrop: function(table, row) {
			$('#gtable tr:odd').addClass('row1');
			$('#gtable tr:even').addClass('row2');
			var rows = table.tBodies[0].rows;
			var sortstr = rows[0].id;
			for (var i=1; i<rows.length; i++) {
				sortstr += ","+rows[i].id;
			}
			$('#sort').val(sortstr);
		}
	});

	$('a#addfield').click(function() {
		var htmlStr = $('.sortfield').html();
				htmlStr = htmlStr.replace(/ selected="selected"/g, '');
		$('<p class="sortfield">' + htmlStr + '</p>').appendTo('#sortfields');
		return false;
	});
	$('a#deletefield').click(function() {
		if($('.sortfield').size() > 1) {
			$('.sortfield:last').remove();
		}
		return false;
	});
});
</script>
{/literal}
EOT;
		return $this->ProcessTemplateFromData($tmpl);
	}


  /**
   * Gallery methods
   */

	function _Getdirfiles($path,$recursive)
	{
		$path = empty($path) ? '' : trim($path,"/") . '/';
		$updir = is_dir(DEFAULT_GALLERY_PATH . $path) ? '' : '../';
		$allowext = explode(',', $this->GetPreference('allowed_extensions',''));
		$maxext = array('jpg','jpeg','gif','png');
		$output = array();
		if ( $handle = opendir(str_replace('/',DIRECTORY_SEPARATOR,$updir . DEFAULT_GALLERY_PATH . $path)) )
		{
			while ( false !== ($file = readdir($handle)) )
			{
				$ext = substr($file, strrpos($file, '.') + 1);
				if ( $file != "." && $file != ".." && substr($file,0,strlen(IM_PREFIX)) != IM_PREFIX && ((in_array(strtolower($ext),$allowext) && in_array(strtolower($ext),$maxext)) || is_dir($updir . DEFAULT_GALLERY_PATH . $path . $file)) )
				{
					$output[$path . $file] = array(
						'filename' => is_dir($updir . DEFAULT_GALLERY_PATH . $path . $file) ? $file . '/' : $file,
						'filepath' => trim($path,"/"),
						'filemdate' => date("Y-m-d H:i:s", filemtime($updir . DEFAULT_GALLERY_PATH . $path . $file)),
					);
					if ( $recursive && is_dir($updir . DEFAULT_GALLERY_PATH . $path . $file) )
					{
						$output = array_merge($output,$this->_Getdirfiles($path . $file,$recursive));
					}
				}
			}
			closedir($handle);
		}
		return $output;
	}


	function _Getgalleryfiles($path)
	{
		$path = trim($path,"/");
		$output = array();
		$db =& $this->GetDB();
		$query = "SELECT
								g1.*, CONCAT(g2.filepath,?,g2.filename) AS thumb
							FROM
								" . cms_db_prefix() . "module_gallery g1
							LEFT JOIN
								" . cms_db_prefix() . "module_gallery g2
							ON
								g1.defaultfile = g2.fileid
							WHERE
								g1.filepath=?";
		$result = $db->Execute($query, array('/'.IM_PREFIX,$path));
		if ( $result && $result->RecordCount() > 0 )
		{
		  while ( $row=$result->FetchRow() )
		  {
		    $output[trim($row['filepath'] . '/' . $row['filename'],'/')] = $row;
		  }
		}
		if ( !$result )
		{
			echo 'ERROR: ' . mysql_error();
			exit();
		}
		return $output;
	}


	function _GetGalleries()
	{
		$output = array();
		$db =& $this->GetDB();
		$query = "SELECT
								g1.*, CONCAT(g2.filepath,?,g2.filename) AS thumb
							FROM
								" . cms_db_prefix() . "module_gallery g1
							LEFT JOIN
								" . cms_db_prefix() . "module_gallery g2
							ON
								g1.defaultfile = g2.fileid
							WHERE
								g1.filename LIKE '%/'
							ORDER BY
								IF(g1.fileid=1,0,1) ASC,
								CONCAT(g1.filepath,CAST(IF(g1.filepath='','','/') AS BINARY),g1.filename) ASC";
		$result = $db->Execute($query, array('/'.IM_PREFIX));
		if ( $result && $result->RecordCount() > 0 )
		{
		  while ( $row=$result->FetchRow() )
		  {
		    $output[$row['fileid']] = $row;
		  }
		}
		if ( !$result )
		{
			echo 'ERROR: ' . mysql_error();
			exit();
		}
		return $output;
	}


	function _Getgalleryinfo($path)
	{
		$path = trim($path,"/");
		if( strpos($path,"/") === FALSE )
		{
			$filename = empty($path) ?  'Gallery/' : $path . '/';
			$filepath = '';

		}
		else
		{
			$filename = substr($path, strrpos($path, '/') + 1) . '/';
			$filepath = substr($path, 0, strrpos($path, '/'));
		}
		$db =& $this->GetDB();
		$query = "SELECT g.*, gp.hideparentlink, gt.templateid, gt.template, gt.thumbwidth, gt.thumbheight, gt.resizemethod, gt.maxnumber, gt.sortitems
				FROM " . cms_db_prefix() . "module_gallery g
				LEFT JOIN " . cms_db_prefix() . "module_gallery_props gp
				ON g.fileid=gp.fileid
				LEFT JOIN " . cms_db_prefix() . "module_gallery_templateprops gt
				ON gp.templateid=gt.templateid
				WHERE g.filename=? AND g.filepath=?";
		$result = $db->Execute($query, array($filename, $filepath));

		if( $result && $result->RecordCount() > 0 )
		{
			$output = $result->FetchRow();
		}
		if ( !$result )
		{
			echo 'ERROR: ' . mysql_error();
			exit();
		}
		return isset($output) ? $output : FALSE;
	}


	function _Getgalleryinfobyid($gid)
	{
		$db =& $this->GetDB();
		$query = "SELECT g.*, gp.hideparentlink, gt.templateid, gt.template, gt.thumbwidth, gt.thumbheight, gt.resizemethod, gt.maxnumber, gt.sortitems
				FROM " . cms_db_prefix() . "module_gallery g
				LEFT JOIN " . cms_db_prefix() . "module_gallery_props gp
				ON g.fileid=gp.fileid
				LEFT JOIN " . cms_db_prefix() . "module_gallery_templateprops gt
				ON gp.templateid=gt.templateid
				WHERE g.fileid=?";
		$result = $db->Execute($query, array($gid));
		if( $result && $result->RecordCount() > 0 )
		{
			$output = $result->FetchRow();
		}
		if ( !$result )
		{
			echo 'ERROR: ' . mysql_error();
			exit();
		}
		return isset($output) ? $output : FALSE;
	}


	function _AddFileToDB($filename,$filepath,$filemdate,$gid)
	{
		$db =& $this->GetDB();
		$insertid = 0;
		$query = "INSERT INTO " . cms_db_prefix() . "module_gallery (filename, filepath, filedate, fileorder, active, defaultfile, galleryid, title, comment) VALUES (?,?,?,0,1,0,?,'','')";
		$result = $db->Execute($query, array($filename, $filepath, $filemdate, $gid));
		if ( $result )
		{
			$insertid = $db->Insert_ID();
			if ( substr($filename,-1) == '/' )
			{
				$query = "INSERT INTO " . cms_db_prefix() . "module_gallery_props (fileid,templateid,hideparentlink) VALUES (?,?,?)";
				$result = $db->Execute($query, array($insertid,0,0));
				if ( !$result )
				{
					echo 'Error: ' . mysql_error();
					exit();
				}
			}
		}
		else
		{
			echo 'Error: ' . mysql_error();
			echo "<br /><br />" . $query;
			exit();
		}
		return $insertid;
	}


	function _UpdateGalleryDB($path,$gid)
	{
		$path = trim($path,"/");
		$file_gallery = $this->_Getdirfiles($path,false);
		$db_gallery = $this->_Getgalleryfiles($path);

		if ( $file_gallery )
		{
			// add to DB:
			$gallery_add = empty($db_gallery) ? $file_gallery : array_diff_assoc($file_gallery,$db_gallery);
			foreach($gallery_add as $key=>$item)
			{
				$fileid = $this->_AddFileToDB($item['filename'],$item['filepath'],$item['filemdate'],$gid);
			}
		}
		if ( $db_gallery )
		{
			// delete from DB:
			$gallery_del = empty($file_gallery) ? $db_gallery : array_diff_assoc($db_gallery,$file_gallery);
			foreach($gallery_del as $key=>$item)
			{
				// make sure that the root gallery and other directories are not deleted here
				if ( $item['fileid'] != 1 && substr($item['filename'],-1) != "/"  ) $delete_ids[] = $item['fileid'];
			}
			if ( isset($delete_ids) )
			{
				$query = implode("','",$delete_ids);
				$query = "DELETE FROM " . cms_db_prefix() . "module_gallery WHERE fileid IN('" . $query . "')";
				$db =& $this->GetDB();
				$result = $db->Execute($query);
				if ( !$result )
				{
					echo 'Error: ' . mysql_error();
					exit();
				}
			}
		}
		return TRUE;
	}

	function _DeleteGalleryDB($path,$gid)
	{
		$path = trim($path,"/");
		$query = "DELETE FROM " . cms_db_prefix() . "module_gallery WHERE
			fileid <> 1 AND
			(fileid = ? OR filepath = ? OR filepath like ?)";
		$db =& $this->GetDB();
		$result = $db->Execute($query, array($gid, $path, $path . '/%'));
		if ( !$result )
		{
			echo 'Error: ' . mysql_error();
			exit();
		}
	}


	function _GetTemplateprops($template)
	{
		$db =& $this->GetDB();
		$query = "SELECT *
				FROM " . cms_db_prefix() . "module_gallery_templateprops
				WHERE template=?";
		$result =& $db->Execute($query, array($template));

		if( $result && $result->RecordCount() > 0 )
		{
			$output = $result->FetchRow();
		}
		if ( !$result )
		{
			echo 'ERROR: ' . mysql_error();
			exit();
		}
		return isset($output) ? $output : FALSE;
	}


	function _ArraySort($array, $arguments = array(), $keys = true)
	{
		// source:  http://nl2.php.net/manual/en/function.uasort.php#42723

		// comparing function code
		$code = "\$result=0; ";

		// foreach sorting argument (array key)
		foreach ($arguments as $argument)
		{
			if ( !empty($argument) )
			{
				// order field
				$field = substr($argument, 2, strlen($argument));

				// sort type ("s" -> string, "n" -> numeric)
				$type = $argument[0];

				// sort order ("+" -> "ASC", "-" -> "DESC")
				$order = $argument[1];

				// add "if" statement, which checks if this argument should be used
				$code .= "if (!Is_Numeric(\$result) || \$result == 0) ";

				// if "numeric" sort type
				if (strtolower($type) == "n")
				{
					$code .= $order == "-" ? "\$result = (\$a->{$field} > \$b->{$field} ? -1 : (\$a->{$field} < \$b->{$field} ? 1 : 0));" : "\$result = (\$a->{$field} > \$b->{$field} ? 1 : (\$a->{$field} < \$b->{$field} ? -1 : 0)); ";
				}
				else
				{
					// if "string" sort type
					$code .= $order == "-" ? "\$result = strcoll(\$a->{$field}, \$b->{$field}) * -1;" : "\$result = strcoll(\$a->{$field}, \$b->{$field}); ";
				}
			}
		}
		// return result
		$code .= "return \$result;";

		// create comparing function
		$compare = create_function('$a, $b', $code);

		// sort array and preserve keys
		uasort($array, $compare);
		
		// return array
		return $array;
	}


	function _CreateThumbnail($thumbname, $image, $thumbwidth, $thumbheight, $method)
	{
		$zoom = .30; // zoom percentage
		$imgdata = getimagesize($image);
		$imgratio = $imgdata[0] / $imgdata[1];  // width/height
		$thumbratio = $thumbwidth / $thumbheight;
		switch($method) {
			case "sc": // scale
			case "zs": // zoom & scale
				$src_x = 0;
				$src_y = 0;
				$src_w = $imgdata[0];
				$src_h = $imgdata[1];
				if( $imgratio > $thumbratio )
				{
					$newwidth = $thumbwidth;
					$newheight = ceil($thumbwidth / $imgratio);
				}
				else
				{
					$newheight = $thumbheight;
					$newwidth = ceil($thumbheight * $imgratio);
				}
				break;

			case "cr": // crop
			case "zc": // zoom & crop
				$newwidth = $thumbwidth;
				$newheight = $thumbheight;
				if( $imgratio > $thumbratio )
				{
					$src_x = ceil(($imgdata[0] - $imgdata[1] * $thumbratio) / 2);
					$src_y = 0;
					$src_w = $imgdata[0] - $src_x * 2;
					$src_h = $imgdata[1];
				}
				else
				{
					$src_x = 0;
					$src_y = ceil(($imgdata[1] - $imgdata[0] / $thumbratio) / 3);
					$src_w = $imgdata[0];
					$src_h = $imgdata[1] - $src_y * 3;
				}
				break;
		}
		if ( $method == "zs" || $method == "zc" )
		{
			$src_x = $src_x + ceil($zoom / 2 * $src_w);
			$src_y = $src_y + ceil($zoom / 3 * $src_h);
			$src_w = ceil($src_w * (1-$zoom));
			$src_h = ceil($src_h * (1-$zoom));
		}
		if ( file_exists($thumbname) )
		{
			$thumbdata = getimagesize($thumbname);
		}
		if ( !isset($thumbdata) || $thumbdata[0] != $newwidth || $thumbdata[1] != $newheight )
		{
			$newimage = imagecreatetruecolor($newwidth,$newheight);
			switch($imgdata['mime']) {
				case "image/gif":
					$source = imagecreatefromgif($image);
					break;
				case "image/pjpeg":
				case "image/jpeg":
				case "image/jpg":
					$source = imagecreatefromjpeg($image);
					break;
				case "image/png":
				case "image/x-png":
					$source = imagecreatefrompng($image);
					break;
				}
			imagecopyresampled($newimage,$source,0,0,$src_x,$src_y,$newwidth,$newheight,$src_w,$src_h);
			switch($imgdata['mime']) {
				case "image/gif":
						imagegif($newimage,$thumbname);
					break;
				case "image/pjpeg":
				case "image/jpeg":
				case "image/jpg":
						imagejpeg($newimage,$thumbname,80);
					break;
				case "image/png":
				case "image/x-png":
					imagepng($newimage,$thumbname);
					break;
				}
			imagedestroy($newimage);
			chmod($thumbname, 0777);
		}
		return $thumbname;
	}

}

?>
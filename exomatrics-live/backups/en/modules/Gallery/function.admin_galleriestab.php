<?php
$themeObject =& $gCms->variables['admintheme'];
$this->_UpdateGalleryDB('',1);
$galleries = $this->_GetGalleries();

$showgalleries = array();
if ( empty($galleries) )
{
	$this->smarty->assign('nogalleriestext', $this->lang("nogalleriestext"));
} 
else 
{
	foreach ($galleries as $gallery) 
	{
		$onerow = new stdClass();

		$onerow->id = $gallery['fileid'];
		$onerow->gidclass = $gallery['galleryid'] > 1 ? ' child-of-node-' . $gallery['galleryid'] : '';
		$onerow->file = $this->CreateLink($id, 'editgallery', $returnid, (empty($gallery['filepath']) ? '' : $gallery['filepath'] . '/') . $gallery['filename'], array('gid'=>$gallery['fileid'],'mode'=>"edit"));

		if ( $gallery['active'] ) 
    {
			$activeimage = $themeObject->DisplayImage('icons/system/true.gif', $this->Lang('setfalse'),'','','systemicon');
		} 
    else 
    {
			$activeimage = $themeObject->DisplayImage('icons/system/false.gif', $this->Lang('settrue'),'','','systemicon');
		}
		$onerow->activelink = $this->CreateLink($id, 'switchactive', $returnid, $activeimage, array('fid' => $gallery['fileid'], 'origaction' => 'defaultadmin'));

		$onerow->editlink = $this->CreateLink($id, 'editgallery', $returnid,
		$themeObject->DisplayImage('icons/system/edit.gif', $this->Lang('editgallery'),'','','systemicon'),
		array('gid' => $gallery['fileid'],'mode'=>"edit"));

		if ( is_dir('../' . DEFAULT_GALLERY_PATH . (empty($gallery['filepath']) ? '' : $gallery['filepath'] . '/') . $gallery['filename']) || $gallery['fileid'] == 1 )
		{
			array_push($showgalleries, $onerow);
		}
		else
		{
			// delete directory and all of its contents from the database
			$this->_DeleteGalleryDB((empty($gallery['filepath']) ? '' : $gallery['filepath'] . '/') . $gallery['filename'], $gallery['fileid']);
		}
	}

}

$this->smarty->assign_by_ref('items', $showgalleries);
$this->smarty->assign('itemcount', count($showgalleries));
$this->smarty->assign('gallerypath', $this->Lang('gallerypath'));
$this->smarty->assign('active', $this->Lang('active'));
$this->smarty->assign('actions', $this->Lang('actions'));

$link = $this->CreateLink($id, 'editgallery', 0, $themeObject->DisplayImage('icons/system/newobject.gif', $this->Lang('addgallery'),'','','systemicon'), array(), '', false, false, '') .' '. $this->CreateLink($id, 'editgallery', $returnid, $this->Lang("addgallery"), array("mode"=>"add"), '', false, false, 'class="pageoptions"');

$this->smarty->assign('addlink', $link);


// Display the populated template
echo $this->ProcessTemplate ('admingalleries.tpl');

?>
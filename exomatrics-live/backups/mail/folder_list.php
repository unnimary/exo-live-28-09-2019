<?
$folders = $sess["folders"];
$scounter = 0;
$pcounter = 0;
foreach($folders as $entry) {
	$entry = $entry["name"];
	$boxname = $entry;
	if(preg_match("/(inbox|sent|trash)/i",$entry)) {
		switch(strtolower($entry)) {
		case "inbox":
			$boxname = $inbox_extended;
			break;
		case "sent":
			$boxname = $sent_extended;
			break;
		case "trash":
			$boxname = $trash_extended;
			break;
		}
		$system[$scounter]["systemname"]    = strtolower($entry);
		$system[$scounter]["name"]      	= $boxname;
		$system[$scounter]["link"] 			= "process.php?folder=".strtolower($entry)."&sid=$sid&tid=$tid&lid=$lid";
		$scounter++;
	} else {
		$personal[$pcounter]["systemname"]  = $entry;
		$personal[$pcounter]["name"]    	= $boxname;
		$personal[$pcounter]["link"] 		= "process.php?folder=$entry&sid=$sid&tid=$tid&lid=$lid";
		$pcounter++;
	}
}
array_qsort2 ($system,"name");
array_qsort2 ($personal,"name");
$smarty->assign("umSystemFolders",$system);
$smarty->assign("umPersonalFolders",$personal);
?>
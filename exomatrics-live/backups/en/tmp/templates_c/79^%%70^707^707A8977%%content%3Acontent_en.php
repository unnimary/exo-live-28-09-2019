<?php /* Smarty version 2.6.25, created on 2013-07-07 03:44:42
         compiled from content:content_en */ ?>
<p style="text-align: justify;"><strong>Hexomatrixx &ndash; MPCL</strong> operating philosophy emphasizes protecting the environment by managing operations for continual improvement, through quality equipment, maintenance, operations, and human resources.&nbsp;&nbsp; We will continue recognizing and exercising its responsibility to minimize environmental risks through the EHSMS&nbsp; Programme.</p>
<p style="text-align: justify;">We believe that each one of us are responsible for protecting the Environment, Health, Safety &amp; Security of our people, customers, and communities and should renew our commitment to working safely in an environmentally sound manner. Its our policy to ensure the systems and processes are in place to manage and address Environmental, Health, Safety &amp; Security within each segment of our business and&nbsp; ensure compliance with national and international requirements.</p>
<p style="text-align: justify;">Hexomatrixx work with its partners, contractors and customers to solicit agreement to implement Hexomatrixx EHS Management System. As part of this policy Hexomatrixx- MPCL Venture&nbsp; is committed to:</p>
<ul style="color:#000;">
    <li style="text-align: justify;">Eliminating accidents and Environmental incidents from every aspects of our business.</li>
    <li style="text-align: justify;">Being prepared to respond to emergencies and to provide support to our partners, customers and contractors.</li>
    <li style="text-align: justify;">Helping our employees, contractors, business partners and service providers and understands how their actions influence the EHS performance.</li>
    <li style="text-align: justify;">Measuring and communicating EHS performance for continuous improvement.</li>
    <li style="text-align: justify;">Recognizing outstanding EHS performance. <br />
    &nbsp;</li>
</ul>
<!-- Add code here that should appear in the content block of all new pages -->
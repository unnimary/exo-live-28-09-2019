<?php /* Smarty version 2.6.25, created on 2014-06-17 05:57:19
         compiled from content:content_en */ ?>
<p>&nbsp;</p>
<p style="text-align: justify;"><img src="http://www.hexomatrixx.com/en/uploads/images/800px-Greek_tanker_ship%20(2)%20(300x195).jpg" width="250" height="163" alt="" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input type="image" src="http://www.hexomatrixx.com/en/uploads/images/offshore-oil-drilling-rig(1).jpg" width="250" height="160" /></p>
<p style="text-align: justify;">We offer a choice of lubricants from some of the world's leading lubricant manufacturers.&nbsp; Our supply net work ensures timely supply of products at the ports in Middle East, Africa and&nbsp; South East Asia.&nbsp; We supply&nbsp; specialized lubricants and greases to some of the major offshore&nbsp; rigs and Marine&nbsp; majors.&nbsp; We&nbsp; stock specialized&nbsp; fully Synthetic &amp; Mineral lubricants &amp;&nbsp; Greases&nbsp; specific for Marine, Aviation sectors to meet any demanding operational environments.</p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:12.0pt;
text-align:justify;line-height:16.8pt"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;">Hexomatrixx-MPCL is one of the leading providers of lubricants and lubrication services to the marine industry. We are able to recommend and supply world class marine lubricants to ports around the world.&nbsp; We also provide a range of services aimed at reducing your costs and improving profit margins. In addition, we offer a customized &lsquo;Total Fluid Management&rsquo; solution where we&rsquo;ll work with you to reduce your lubrication costs and extend the life of your equipment.<o:p></o:p></span></p>
<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:16.8pt;mso-outline-level:
3"><b><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#002060">Reliable Service &amp; Fast Delivery<o:p></o:p></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:16.8pt"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;">We stock the right products and are able to deliver on time. With our strategically placed depots and our fleet of delivery vehicles, we provide a fast and efficient delivery service.<b> </b><o:p></o:p></span></p>
<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:16.8pt;mso-outline-level:
3"><b><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#03004A">Premium Quality Products<o:p></o:p></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:16.8pt"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;">We understand that reducing oil consumption and increasing oil-drain intervals are important factors. We work with the world leaders in lubricant production, and can supply you with premium quality products, that will ensure downtime is reduced and production is increased.<b> </b><o:p></o:p></span></p>
<p class="MsoNormal" style="margin-left: -3pt;"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;">World class Marine Cylinder oils, System Oils, Auxiliary Engine Oils, Hydraulic oils, Gear oils, Synthetic and Greases.<o:p></o:p></span></p>
<p class="MsoListParagraph" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:38.25pt;mso-add-space:auto;text-indent:-.25in;line-height:
16.8pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:
11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;color:#404040">&middot;<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size:11.5pt;font-family:OpenSansRegular;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:#404040">Control piston deposits and linear wear in severe applications <o:p></o:p></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:38.25pt;text-indent:-.25in;line-height:16.8pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#404040">&middot;<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size:11.5pt;font-family:OpenSansRegular;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:#404040">Provide superior wear protection at high temperatures.<o:p></o:p></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:38.25pt;text-indent:-.25in;line-height:16.8pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#404040">&middot;<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size:11.5pt;font-family:OpenSansRegular;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:#404040">Deliver consistent quality and superior performance.<o:p></o:p></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:38.25pt;text-indent:-.25in;line-height:16.8pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#404040">&middot;<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size:11.5pt;font-family:OpenSansRegular;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:#404040">Allow safe optimization of feed rate settings without compromising wear.<o:p></o:p></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:2.25pt;text-align:justify;line-height:16.8pt"><span style="font-size:11.5pt;font-family:OpenSansRegular;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">We are independent of all oil companies and have unique service-based audit processes and reporting programs to optimize your oil life, improve the overall performance of your machine life and implement action plans based on routine machinery testing and lubricant scheduling.</span><b><span style="font-size:11.5pt;mso-bidi-font-size:11.0pt;
font-family:OpenSansRegular;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"> </span></b><span style="font-size:
11.5pt;font-family:OpenSansRegular;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>
<p class="MsoListParagraph" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:38.25pt;mso-add-space:auto;text-indent:-.25in;line-height:
16.8pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:
11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">&middot;<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size:11.5pt;font-family:OpenSansRegular;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">We offer a full lubricant scheduling service for vessels and have worked with some of the largest shipping companies in Europe to ensure they are using the best lubricants for their ships.<o:p></o:p></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:16.8pt"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;">&nbsp;</span><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#03004A">Technical Expertise<o:p></o:p></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:16.8pt"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;">We've built a vast wealth of knowledge and expertise in the marine industry, ensuring we can efficiently deal with any technical issues or questions you may have. By liaising with our team of experts, our professional and friendly Customer Service team will guarantee a fast and reliable response to your query.<o:p></o:p></span></p>
<p style="text-align: justify;"><span style="font-size:12.0pt;line-height:115%;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA"><a href="http://www.thelubricantcompany.co.uk/contact" target="_self" title="Contact Us"><span style="color:#990000;text-decoration:none;text-underline:
none">Contact us to discuss your requirements: cs@hexomatrixx.com</span></a> Or hxmatrix@emirates.net.ae, Tel:+97165548433 Fax:+97165548435</span></p>
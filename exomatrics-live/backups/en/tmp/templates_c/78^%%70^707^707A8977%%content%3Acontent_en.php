<?php /* Smarty version 2.6.25, created on 2013-07-08 02:32:47
         compiled from content:content_en */ ?>
<p style="text-align: justify;">Protecting the Environment, Health &amp; Safety (EHS) of our employees, contractors and partners is one of Hexomatrixx &ndash; MPCL&nbsp; core values. Good EHS performance is&nbsp;a business imperative, critical to our survival as a company. We have formulated a EHS Management System for <strong>HEXOMATRIXX &amp; MPCL </strong>operations.&nbsp; Our EHSMS will help us to prevent major incidents, reduce operating costs and maintain our reputation as a company that is committed to protecting people and the environment.</p>
<p style="text-align: justify;"><strong>Our Vision</strong><br />
To be able to supply and handle your cargo safely and provide full range of logistic services within the supply chain with total care for Quality, Safety &amp; Security.</p>
<p style="text-align: justify;"><strong>Objectives</strong></p>
<ul style="color:#000;">
    <li style="text-align: justify;">To review and evaluate the status of EHSMS program implementation</li>
    <li style="text-align: justify;">To develop an EHS Policy based on our business practice</li>
    <li style="text-align: justify;">To consolidate reporting of EHS performance from each of the service partners, contractors including that we do not control or manage.</li>
</ul>
<p style="text-align: justify;">&nbsp;</p>
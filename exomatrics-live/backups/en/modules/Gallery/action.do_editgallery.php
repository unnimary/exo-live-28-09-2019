<?php
if( !isset( $gCms ) ) exit();

if( isset( $params['cancel'] ) )
{
	$params = array('active_tab' => 'galleries');
	$this->Redirect($id, 'defaultadmin', '', $params);
}

if( !$this->CheckPermission('Use Gallery') )
{
	return $this->DisplayErrorPage($id, 'defaultadmin', $returnid,$this->Lang('accessdenied'));
}

if( !isset($params['gid']) || !isset($params['mode']) )
{
	$params = array('gid' => $params['gid'], 'mode' => 'edit', 'errors' => $this->Lang('error_insufficientparams'));
	$this->Redirect($id,'editgallery','',$params);
	return;
}

if( $params['mode'] == 'unsort')
{
	$query = "UPDATE " . cms_db_prefix() . "module_gallery SET fileorder=0 WHERE galleryid = ?";
	$result = $db->Execute($query, array($params['gid']));
	if ( !$result )
	{
		echo 'ERROR: ' . mysql_error();
		exit();
	}
}
else
{
	$gallerytitle = isset($params['gallerytitle']) ? $params['gallerytitle'] : '';
	$gallerycomment = isset($params['gallerycomment']) ? $params['gallerycomment'] : '';

	$query = "UPDATE " . cms_db_prefix() . "module_gallery SET title = ?, comment = ? WHERE fileid = ?";
	$result = $db->Execute($query, array($gallerytitle, $gallerycomment, $params['gid']));
	if ( !$result )
	{
		echo 'ERROR: ' . mysql_error();
		exit();
	}

	$params['hideparentlink'] = isset($params['hideparentlink']) ? $params['hideparentlink'] : 0;
	$params['hideparentlink'] = $params['gid'] == 1 ? 1 : $params['hideparentlink'];

	$query = "UPDATE " . cms_db_prefix() . "module_gallery_props SET templateid=?,hideparentlink=? WHERE fileid=?";
	$result = $db->Execute($query, array($params['templateid'],$params['hideparentlink'],$params['gid']));
	if ( !$result )
	{
		echo 'ERROR: ' . mysql_error();
		exit();
	}


	if ( !empty($params['sort']) )
	{
		$sort = explode(",",$params['sort']);
	}
	$searchwords = $gallerytitle . ' ' . $gallerycomment;
	if ( isset($params['filetitle']) )
	{
		foreach($params['filetitle'] as $key=>$filetitle)
		{
			if ( !empty($params['sort']) )
			{
				$sortkey = empty($sort) ? 0 : array_search($key, $sort) + 1;
				if ( $filetitle == "#dir" )
				{
					$query = "UPDATE " . cms_db_prefix() . "module_gallery SET fileorder=? WHERE fileid = ?";
					$result = $db->Execute($query, array($sortkey, $key));
				}
				else
				{
					$searchwords .= ' ' . $filetitle . ' ' . $params['filecomment'][$key];
					$query = "UPDATE " . cms_db_prefix() . "module_gallery SET title=?, comment=?, fileorder=? WHERE fileid = ?";
					$result = $db->Execute($query, array($filetitle, $params['filecomment'][$key], $sortkey, $key));
				}
			}
			elseif ( $filetitle != "#dir" )
			{
				$searchwords .= ' ' . $filetitle . ' ' . $params['filecomment'][$key];
				$query = "UPDATE " . cms_db_prefix() . "module_gallery SET title=?, comment=? WHERE fileid = ?";
				$result = $db->Execute($query, array($filetitle, $params['filecomment'][$key], $key));
			}
			if ( !$result )
			{
				echo 'ERROR: ' . mysql_error();
				exit();
			}
		}
	}
}


if ( $result ) 
{
	$search =& $this->GetModuleInstance('Search');
	if( $search && $params['mode'] != 'unsort' )
	{
		$search->AddWords($this->GetName(),$params['gid'],'gallery',$searchwords);
	}

	$params = array('gid' => $params['gid'], 'mode' => 'edit', 'module_message' => $this->Lang('galleryupdated'));
	$this->Redirect($id, 'editgallery', '', $params);
}
else 
{
	$params = array('gid' => $params['gid'], 'mode' => 'edit', 'errors' => $this->Lang('error_updategalleryfailed'));
	$this->Redirect($id, 'editgallery', '', $params);
}

?>
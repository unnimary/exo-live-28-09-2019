<?php /* Smarty version 2.6.25, created on 2013-07-07 13:39:54
         compiled from content:content_en */ ?>
<p style="text-align: justify;">We are&nbsp;an organization with core skill bases in all areas of business and operations management offers consulting and implementation services that facilitate improved business profitability and cash generation.</p>
<p>We&nbsp; manages your&nbsp; in country outsourcing requirement&nbsp; and&nbsp; manage&nbsp; the full cycle of&nbsp; &ldquo;Order to Cash&rdquo; and &ldquo; Purchase&nbsp; to Pay &ldquo;</p>
<p><strong>Fulfillment Planning</strong></p>
<ul style="color:#000;">
    <li>Demand planning</li>
    <li>Production &amp; Manufacturing Planning</li>
</ul>
<p>&nbsp;<strong>Product life Cycle Management</strong></p>
<ul style="color:#000;">
    <li>Research &amp; Developement</li>
    <li>Manufacturing &amp; Operations Management</li>
    <li>Order Management</li>
    <li>Fulfillment Management&nbsp;</li>
</ul>
<p><br />
&nbsp;<strong>Manufacturing&nbsp; Scheduling</strong></p>
<ul style="color:#000;">
    <li>Demand Planning</li>
    <li>Procurement</li>
    <li>Inventory Control</li>
    <li>Vendor Managed Inventory</li>
    <li>Supplier Coordination</li>
    <li>Order to Delivery</li>
</ul>
<p><br />
&nbsp;<strong>Supplier Management</strong></ul>
<ul style="color:#000;">
        <li>Supplier Monitoring</li>
        <li>Supplier Evaluation</li>
        <li>Performance&nbsp; Metrics <br />
        <strong>&nbsp;</strong></li>
    </ul>
    <p><strong>&nbsp;Operations Management</strong></p>
<?php
if( !isset( $gCms ) ) exit();


if( !$this->CheckPermission('Modify Templates') )
  {
	echo $this->ShowErrors($this->Lang('accessdenied'));
	return;
  }

if( !isset( $params['template'] ) )
  {
		$params = array('errors'=> 'error_insufficientparams', 'active_tab' => 'templates');
    $this->Redirect($id, 'defaultadmin' , $returnid, $params);
    return;
  }

$this->SetPreference('current_template',$params['template']);
$this->Redirect($id, 'defaultadmin' , $returnid, array('active_tab' => 'templates'));
?>
<?php
$this->RemovePreference();
$this->RemovePermission('Modify FCKeditorX');
$this->RemovePermission('Use FCKeditorX');

		global $gCms;
		$db =& $gCms->GetDb();
		$dict = NewDataDictionary( $db );

		$sqlarray = $dict->DropTableSQL( cms_db_prefix()."module_FCKX" );
		$dict->ExecuteSQLArray($sqlarray);

		// put mention into the admin log
		$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('uninstalled'));
?>
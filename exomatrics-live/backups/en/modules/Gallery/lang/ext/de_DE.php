<?php
$lang['friendlyname'] = 'Galerie';
$lang['moddescription'] = 'Die einfachste Art, eine Bildergalerie zu verwalten und darzustellen.';
$lang['description'] = 'Eine einfache Bildergalerie, die automatisch die Bilder eines angegebenen Verzeichnisses darstellt.';
$lang['postinstall'] = 'Das Galerie-Modul wurde erfolgreich installiert.';
$lang['installed'] = 'Die Version %s des Galerie-Moduls wurde installiert';
$lang['upgraded'] = 'Das Galerie-Modul wurde auf die Version %s aktualisiert.';
$lang['postuninstall'] = 'Das Galerie-Modul wurde deinstalliert';
$lang['uninstalled'] = 'Das Galerie-Modul wurde deinstalliert';
$lang['really_uninstall'] = 'Sind sie sicher, dass sie das Galerie-Modul deinstallieren wollen? Das betrifft nicht die Bilder selbst, aber die Kommentare und Vorschaubilder, die das Modul erstellt hat, werden dadurch mit gel&ouml;scht.';
$lang['accessdenied'] = 'Zugriff verweigert. Bitte &uuml;berpr&uuml;fen sie die Zugriffsrechte.';
$lang['actions'] = 'Aktionen';
$lang['active'] = 'aktiv';
$lang['apply'] = 'Anwenden';
$lang['areyousure'] = 'Wirklich l&ouml;schen?';
$lang['cancel'] = 'Abbrechen';
$lang['copy'] = 'kopieren';
$lang['default'] = 'Standard';
$lang['delete'] = 'l&ouml;schen';
$lang['edit'] = 'bearbeiten';
$lang['error'] = 'Fehler!';
$lang['setfalse'] = 'deaktivieren';
$lang['settrue'] = 'aktivieren';
$lang['submit'] = 'Speichern';
$lang['galleries'] = 'Galerien';
$lang['gallerypath'] = 'Galerie';
$lang['editgallery'] = 'Galerie bearbeiten';
$lang['addgallery'] = 'Galerie hinzuf&uuml;gen';
$lang['nogalleriestext'] = 'keine Galerien verf&uuml;gbar';
$lang['item'] = 'Bild';
$lang['title'] = 'Titel';
$lang['date'] = 'Datum';
$lang['nofilestext'] = 'keine Bilder verf&uuml;gbar';
$lang['gallerytitle'] = 'Galerietitel';
$lang['comment'] = 'Kommentar';
$lang['template'] = 'Vorlage';
$lang['usedefault'] = 'Standard verwenden';
$lang['thumbnailsize'] = 'Gr&ouml;&szlig;e des Vorschaubildes (auf Benutzerseite)';
$lang['leaveempty'] = '(frei lassen, um die Standard-Vorschaubilder der Bildverwaltung zu verwenden)';
$lang['width'] = 'Breite';
$lang['height'] = 'H&ouml;he';
$lang['resizemethod'] = 'Skalierungsmethode';
$lang['crop'] = 'beschneiden';
$lang['scale'] = 'skalieren';
$lang['zoomcrop'] = 'zoomen &amp; beschneiden';
$lang['zoomscale'] = 'zoomen &amp; skalieren';
$lang['createthumbs'] = 'Vorschaubilder (neu) erstellen';
$lang['thumbscreated'] = 'Vorschaubilder wurden erstellt.';
$lang['hideparentlink'] = 'Hide link to parent gallery';
$lang['sortbysettings'] = 'Sort by template settings';
$lang['galleryupdated'] = 'Die Galerie wurde erfolgreich aktualisiert.';
$lang['error_updategalleryfailed'] = 'Aktualisierung der Galerie fehlgeschlagen.';
$lang['templates'] = 'Vorlagen';
$lang['title_available_templates'] = 'verf&uuml;gbare Vorlagen';
$lang['prompt_name'] = 'Name';
$lang['prompt_default'] = 'Standard';
$lang['prompt_newtemplate'] = 'Neue Vorlage erstellen';
$lang['title_template'] = 'Vorlagen-Editor';
$lang['prompt_templatename'] = 'Vorlagenname';
$lang['prompt_template'] = 'Vorlagenquelle';
$lang['prompt_templatejs'] = 'Vorlagen-JavaScript';
$lang['prompt_templatecss'] = 'Vorlagen-Stylesheet';
$lang['resetoriginal'] = 'Reset to original';
$lang['resetoriginalwarning'] = 'Are you sure you want to reset the template code to its original code?';
$lang['templateupdated'] = 'Die Vorlage wurde erfolgreich aktualisiert.';
$lang['templateadded'] = 'Die Vorlage wurde erfolgreich hinzugef&uuml;gt.';
$lang['error_templatenameexists'] = 'Der Vorlagenname existiert bereits.';
$lang['templatedeleted'] = 'Vorlage gel&ouml;scht.';
$lang['availablevariables'] = 'Verf&uuml;gbare Smarty-Variablen';
$lang['availablevariableslist'] = 'These are the variables you can use to customize your template:<br /><br />
<code>{$module_message}</code> - error message, is only set if there\'s a message<br />
<code>{$gallerytitle}</code> - title of the gallery. If there is no title, this will show the directory name<br />
<code>{$gallerycomment}</code> - comment of the gallery<br />
<code>{$parentlink}</code> - link to the parent folder<br />
<code>{$hideparentlink}</code> - true/false<br />
<code>{$imagecount}</code> - shows e.g. "6 images", depending on language<br />
<code>{$itemcount}</code> - number of items, images + folders<br />
<code>{$pages}</code> - number of pages<br />
<code>{$prevpage}</code> - link to previous page, if applicable<br />
<code>{$nextpage}</code> - link to next page, if applicable<br />
<code>{$pagelinks}</code> - links to each existing page<br />
<code>{$images}</code> - array with keys:<br />
 - <code>file</code> - relative path to the original image (or subgallery)<br />
 - <code>title</code> - title of the image. If there is no title, this will show the filename<br />
 - <code>comment</code> - comment to the image<br />
 - <code>filedate</code> - creation date/time<br />
 - <code>thumb</code> - relative path to the thumbnail. In case of a subgallery, this wil revert to the image which is set as default in that subgallery. If no image is set as default, a standard folder-icon will be used.<br />
 - <code>fileid</code> - unique id for the image or subgallery<br />
 - <code>isdir</code> - true if it\'s a subgallery<br />
';
$lang['options'] = 'Optionen';
$lang['allowed_extensions'] = 'erlaubte Dateiendungen';
$lang['use_comment_wysiwyg'] = 'Einen grafischen Editor f&uuml;r das Kommentarfeld verwenden';
$lang['optionsupdated'] = 'Die Optionen wurden erfolgreich aktualisiert.';
$lang['image'] = 'Bild';
$lang['images'] = 'Bilder';
$lang['prevpage'] = '&amp;lt; zur&uuml;ck';
$lang['nextpage'] = 'weiter &amp;gt;';
$lang['defaultgallerycomment'] = 'Was sie hier sehen, ist das Galerie-Modul. Wenn sie ein paar Bilder in das Verzeichnis &bdquo;uploads/images/Gallery&ldquo; kopiert haben, dann sehen sie sie jetzt unten. Sie k&ouml;nnen Titel, Beschreibungen und Vorschaubilder im Administrationsbereich bearbeiten. Alle weiteren Funktionen dieses Moduls sind in der Modulhilfe beschrieben.';
$lang['help_dir'] = 'Parameter to specify a directory, relative to uploads/images/Gallery/';
$lang['help_template'] = 'Use a separate database template for displaying the photo gallery. This template must exist and be visible in the template tab of the Gallery admin, though it does not need to be the default. If this parameter is not specified, then the template which is assigned to the directory will be used, else the default template.';
$lang['help_number'] = 'Maximum number of imagethumbs to display (per page). Leaving empty will show all images.';
$lang['help_start'] = 'Start at the nth image. Leaving empty will start at the first image.';
$lang['help_show'] = 'Overide which images have to be shown. Possible values are:
<ul>
<li>\'active\' - to display the images marked as active (default)</li>
<li>\'inactive\' - to display only the images marked as inactive </li>
<li>\'all\' - to display all images</li>
</ul>
';
$lang['help_category'] = 'To display only images/galleries assigned to the specified categories. Use * after the name to show children.  Multiple categories can be used if separated with a comma. Leaving empty, will show all categories.';
$lang['help_action'] = 'Override the default action. Use it in combination with the above parameters. Possible values are:
<ul>
<li>\'showrandom\' - to display a set of random thumb-images (applies only to the images which are stored in the database, defaults to a number of 6 images). Use \'/*\' after the directoryname in the dir parameter to include images from subdirectories</li>
<li>\'showlatest\' - to display the most recently added images (applies only to the images which are stored in the database, defaults to a number of 6 images) The dir parameter can be set, or the default Gallery-root will be used. Subdirectories are automatically included</li>
<li>\'showlatestdir\' - to display a set of random thumb-images from the most recently added directory (applies only to the images which are stored in the database, defaults to a number of 6 images)</li>
</ul>
Note that images are only stored in the database when the gallery is visited in the admin.
';
$lang['changelog'] = '<ul>
<li>Version 1.1.2  5 December 2009. Update Fancybox system to version 1.2.6, Fix for Bugreport #4422: Error when sortingoption is empty, Built in check for templates/css directory (BR #4305).</li>
<li>Version 1.1.1. 12 November 2009. Fix for Bugreport #4294: Thumbfolder not created when upgrading.</li>
<li>Version 1.1. 8 November 2009. Added sorting and drag&drop feature (FR #3907 #4210), Moved thumbnailsettings to templatestab, Make galleries searchable (FR #3956), Create backend thumbs if they don\'t exist in IM (FR #4101), Fix for BR #4027, Added fileid to image-object (FR #4259)</li>
<li>Version 1.0.3. 8 September 2009. Bugfix for [#4001], Improved synchronizing the database to files, and other minor improvements.</li>
<li>Version 1.0.2. 5 September 2009. Bugfix for [#3983] regarding spaces in directory names, Added templates Fancybox and Lytebox_slideshow as in Feature requests [#3944] and [#3967]</li>
<li>Version 1.0.1. 23 August 2009. Apply thumbnail-settings on \'Album-covers\' [#3924], Bugfix for [#3927], Added missing /div to thickbox-template, fixed an issue with generating thumbnails when no thumbs available in ImageManager, improved the naming of thumbs, Bugfix for [#3931], Prevent display the content of galleries that are set as inactive.</li>
<li>Version 1.0. 17 August 2009. Pagination parameters added, Standard templates changed accordingly, Extended the module-help, Added functions to show random or latest images, Support of pretty-urls, Various improvements</li>
<li>Version 0.6. 9 August 2009. Initial Beta Release.</li>
</ul>';
$lang['help'] = '<h3>What Does This Do?</h3>
<p>The Gallery module is an easy to use photo gallery which automatically shows the images of a specified directory. Subdirectories will be shown as subgalleries. It has lots of features, such as automatic thumbnailing, the use of multiple Lightbox-like templates or any css/javascript template you like, and you can give titles and descriptions to your galleries as well as your photos.</p>
<h3>How Do I Use It</h3>
<p>First, insert the module with the <code>{Gallery}</code> tag into your page or template anywhere you whish. Then upload some images with the Image Manager, File Manager (e.g. multiple images in zipfile) or FTP to the uploads/images/Gallery/ directory.</p>
<p>That\'s it!</p>
<p>If you want more photo galleries, simply create a subdirectory and upload your photos as described. By adding parameters to the <code>{Gallery}</code> tag, you can easily manipulate which subgallery will be shown in which template, e.g. <code>{Gallery dir="holidays/Netherlands2009" template="Lightbox"}</code></p>
<p>By default the thumbnails from the Image Manager are used to display the photo galleries.</p>
<h3>Advanced Options, but still easy to use</h3>
<p>In the admin section you have lots of other options:</p>
<ul>
<li>Set a photo as the default for a gallery, so it will show in the parent gallery as a \'cover\' in stead of the default folder-icon.</li>
<li>Give titles and descriptions to galleries.</li>
<li>Set a default template for each gallery.</li>
<li>Set thumbnail sizes for each template, with posibilities to scale, crop and/or zoom the images.</li>
<li>Specify for each template in which order the photos have to be sorted.</li>
<li>Overrule the sorting manually by dragging&dropping the photos.
<li>Give titles and descriptions to photos.</li>
<li>Switch a specific photo or gallery to inactive, preventing it from display.</li>
<li>Edit/copy templates or create new ones. Check the info-icon beneath the template-code for the available variables.</li>
</ul>
<p>In order to edit templates, the user must belong to a group with the \'Modify Templates\' permission. To edit the global Gallery options, the user must belong to a group with the \'Modify Site Preferences\' permission.</p>
<p>All titles, descriptions and settings are stored in the database. The database will synchronize with the filedirectory each time the according gallery is visited in the Gallery-admin. A little warning: when you move an image or subdirectory to another directory, you will loose its title, description and settings.</p>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Feature Request or Bug Report, please visit the Module Forge
<a href="http://dev.cmsmadesimple.org/projects/gallery/">Gallery Page</a>.</li>
<li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.</li>
<li>Lastly, you may have some success emailing the author directly.</li>
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>

<h3>Copyright and License</h3>
<p>Copyright &copy; 2009, Jos -<a href="mailto:josvd@live.nl">josvd@live.nl</a>-. All Rights Are Reserved.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>
';
$lang['qca'] = '1249923764-15871878-96895245';
$lang['utma'] = '156861353.4340126149939662000.1247151222.1252331279.1252340654.22';
$lang['utmz'] = '156861353.1247151575.1.2.utmccn=(organic)|utmcsr=google|utmctr=translationcenter cmsmadesimple|utmcmd=organic';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353.1.10.1252340654';
?>
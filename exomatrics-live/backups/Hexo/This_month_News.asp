<%@ Language=VBScript%>
<%
sTo = Trim(Request.Form("bc"))
sFrom = "newsletter@hexomatrixx.com"
sSubject = "Hexomatrixx's This Month Newsletter"
sMailServer = "127.0.0.1"


	Set objMail = Server.CreateObject("CDO.Message")
	Set objConf = Server.CreateObject("CDO.Configuration") 
	Set objFields = objConf.Fields
	
	With objFields
		.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver")  = sMailServer 
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10 
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Update 
	End With

	With objMail
		Set .Configuration = objConf
		.From = sFrom
		.To = sTo
		.Subject = sSubject
		'.TextBody = sBody
		'.HTMLBody = "<h1>This is a message.</h1>" 
		.CreateMHTMLBody "http://www.hexomatrixx.com/hexo/Newsletter.html"
	End With
    
    objMail.Send

	Set objFields = Nothing
	Set objConf = Nothing
	Set objMail = Nothing
	
%>

 
<html>
<title>Newsletter Sent Successfully</title>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
<body>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<table width="60%" border="0" align="center" cellpadding="5" cellspacing="3">
  <tr> 
    <td bgcolor="#FFFFFF"><div align="center"><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif"><img src="../admin/images/logo.jpg" width="117" height="51"></font></div></td>
  </tr>
  <tr>
    <td bgcolor="#FF0000"><div align="center"><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Newsletter 
        Send Successfully<br>
        Check your mail box now, Thanks<br>
        <% if len(mes) > 0 then	Alert(mes) end if %>
        </strong></font></div></td>
  </tr>
  <tr> 
    <td bgcolor="#0000FF"><div align="center"> 
        <input name="Submit" type="submit" onClick="MM_goToURL('parent','http://www.hexomatrixx.com');return document.MM_returnValue" value="Continue">
      </div></td>
  </tr>
</table>
</body>
</html>

<?php /* Smarty version 2.6.25, created on 2013-07-08 13:41:45
         compiled from module_file_tpl:MenuManager%3Bsimple_navigation.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'repeat', 'module_file_tpl:MenuManager;simple_navigation.tpl', 12, false),)), $this); ?>
<?php $this->_cache_serials['/home/hexom0/public_html/en/tmp/templates_c/892eb2f8aaf567d67e54180dabf3533c^%%C2^C24^C24843E6%%module_file_tpl%3AMenuManager%3Bsimple_navigation.tpl.inc'] = 'f97f8e1dbe9306efeddc0b35639ad340'; ?> 

<?php if ($this->_tpl_vars['count'] > 0): ?>
<ul>
<?php $_from = $this->_tpl_vars['nodelist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['node']):
?>
<?php if ($this->_tpl_vars['node']->depth > $this->_tpl_vars['node']->prevdepth): ?>
<?php if ($this->caching && !$this->_cache_including): echo '{nocache:f97f8e1dbe9306efeddc0b35639ad340#0}'; endif;$_cache_attrs =& $this->_smarty_cache_attrs('f97f8e1dbe9306efeddc0b35639ad340','0');echo smarty_cms_function_repeat(array('string' => "<ul>",'times' => $this->_tpl_vars['node']->depth-$this->_tpl_vars['node']->prevdepth), $this);if ($this->caching && !$this->_cache_including): echo '{/nocache:f97f8e1dbe9306efeddc0b35639ad340#0}'; endif;?>

<?php elseif ($this->_tpl_vars['node']->depth < $this->_tpl_vars['node']->prevdepth): ?>
<?php if ($this->caching && !$this->_cache_including): echo '{nocache:f97f8e1dbe9306efeddc0b35639ad340#1}'; endif;$_cache_attrs =& $this->_smarty_cache_attrs('f97f8e1dbe9306efeddc0b35639ad340','1');echo smarty_cms_function_repeat(array('string' => "</li></ul>",'times' => $this->_tpl_vars['node']->prevdepth-$this->_tpl_vars['node']->depth), $this);if ($this->caching && !$this->_cache_including): echo '{/nocache:f97f8e1dbe9306efeddc0b35639ad340#1}'; endif;?>

</li>
<?php elseif ($this->_tpl_vars['node']->index > 0): ?></li>
<?php endif; ?>


<?php if ($this->_tpl_vars['node']->parent == true || ( $this->_tpl_vars['node']->current == true && $this->_tpl_vars['node']->haschildren == true )): ?>
<li class="menuactive menuparent"><a class="menuactive menuparent" href="<?php echo $this->_tpl_vars['node']->url; ?>
"><span><?php echo $this->_tpl_vars['node']->menutext; ?>
</span></a>

<?php elseif ($this->_tpl_vars['node']->haschildren == true): ?>
<li class="parent"><a class="parent" href="<?php echo $this->_tpl_vars['node']->url; ?>
"><span><?php echo $this->_tpl_vars['node']->menutext; ?>
</span></a>

<?php elseif ($this->_tpl_vars['node']->current == true): ?>
<li class="currentpage"><h3><span><?php echo $this->_tpl_vars['node']->menutext; ?>
</span></h3>

<?php elseif ($this->_tpl_vars['node']->type == 'sectionheader'): ?>
<li class="sectionheader"><span><?php echo $this->_tpl_vars['node']->menutext; ?>
</span>

<?php elseif ($this->_tpl_vars['node']->type == 'separator'): ?>
<li class="separator" style="list-style-type: none;"> <hr />

<?php else: ?>
<li><a href="<?php echo $this->_tpl_vars['node']->url; ?>
"><span><?php echo $this->_tpl_vars['node']->menutext; ?>
</span></a>

<?php endif; ?>

<?php endforeach; endif; unset($_from); ?>
<?php if ($this->caching && !$this->_cache_including): echo '{nocache:f97f8e1dbe9306efeddc0b35639ad340#2}'; endif;$_cache_attrs =& $this->_smarty_cache_attrs('f97f8e1dbe9306efeddc0b35639ad340','2');echo smarty_cms_function_repeat(array('string' => "</li></ul>",'times' => $this->_tpl_vars['node']->depth-1), $this);if ($this->caching && !$this->_cache_including): echo '{/nocache:f97f8e1dbe9306efeddc0b35639ad340#2}'; endif;?>
</li>
</ul>
<?php endif; ?>
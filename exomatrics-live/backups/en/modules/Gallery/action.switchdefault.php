<?php
if (!isset($gCms)) exit;

// Check permissions
if( !$this->CheckPermission('Use Gallery') )
{
	echo $this->ShowErrors($this->Lang('accessdenied'));
	return;
}

$file_id = isset($params['fid']) ? $params['fid'] : '';
$gallery = isset($params['gid']) ? $params['gid'] : '';

$query = "UPDATE " . cms_db_prefix() . "module_gallery SET defaultfile = ? WHERE fileid = ?";
$db->Execute($query, array($file_id,$gallery));

$this->Redirect($id, 'editgallery' , $returnid, $params);

?>
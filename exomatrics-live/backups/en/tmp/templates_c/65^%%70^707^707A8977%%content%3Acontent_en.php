<?php /* Smarty version 2.6.25, created on 2014-06-17 05:55:44
         compiled from content:content_en */ ?>
<p style="text-align: justify;">Our third party logistics (3PL) services include warehousing, pick and pack, assembly, fulfillment, labeling, packaging, freight forwarding, shipping, and transportation management. Our warehousing and distribution facilities are strategically located to support efficient distribution throughout the Middle East region. Cost effective and efficient container transportation to our warehousing facilities enables your products to be ready for distribution within the shortest possible time frame. <br />
<br />
Infrastructure &amp; Storage  Capacities</p>
<ul style="color:#000;">
    <li>Modern logistics centers and  EHS approved  state of art warehouses.</li>
    <li>Purpose built DC to suit the specific requirements of each retail brands</li>
    <li>Located in a FZ environment to support cross border business</li>
    <li>In-house 24/7 Customer &amp; Supply  management facilities.</li>
    <li>Best in class logistics centers to attract new brands</li>
    <li>Consistency in  service level</li>
    <li>Best logistics facility in the region for storage of Petroleum products</li>
    <li>Flexible to operate under single or multiple WMS as per brand.</li>
    <li>Bonded storage facility &ndash; Offers a 100% duty exemption</li>
</ul>
<p>&nbsp;</p>
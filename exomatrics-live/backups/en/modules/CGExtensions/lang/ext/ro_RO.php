<?php
$lang['CGFILEUPLOAD_NOFILE'] = 'Nici un fisier incarcat potrivit speficatiilor';
$lang['CGFILEUPLOAD_FILESIZE'] = 'Marimea fisierului incarcat a ajuns la maximul permis';
$lang['CGFILEUPLOAD_FILETYPE'] = 'Fisier de acest gen nu poate fi incarcat';
$lang['CGFILEUPLOAD_FILEEXISTS'] = 'Un fisier cu acest nume exita deja';
$lang['CGFILEUPLOAD_BADDESTDIR'] = 'Destinatia directorului specificat pentru incarcare nu exista';
$lang['CGFILEUPLOAD_BADPERMS'] = 'Fisierele permise nu permit scriere pe fisierul incarcat in locatia destinatiei';
$lang['CGFILEUPLOAD_MOVEFAILED'] = 'Incercarea de muta fisierul incarcat la destiantia finala nu a fost reusita';
$lang['thumbnail_size'] = 'Marimea pozei mici (thumbnails)';
$lang['image_extensions'] = 'Extensia Fisier Imagine';
$lang['group'] = 'Grup';
$lang['template'] = 'Sablon';
$lang['select_one'] = 'Alegeti unul(a)';
$lang['priority_countries'] = 'Tari Prioritare';
$lang['prompt_edittemplate'] = 'Editeaza sablon';
$lang['prompt_deletetemplate'] = 'Sterge sablon';
$lang['prompt_templatename'] = 'Nume sablon';
$lang['prompt_template'] = 'Test sablon';
$lang['prompt_name'] = 'Nume sablon';
$lang['prompt_newtemplate'] = 'Sablon nou';
$lang['prompt_default'] = 'Implicit';
$lang['yes'] = 'Da';
$lang['no'] = 'Nu';
$lang['submit'] = 'Trimite';
$lang['apply'] = 'Aplica';
$lang['cancel'] = 'Anuleaza';
$lang['edit'] = 'Editeaza';
$lang['areyousure'] = 'Sunteti sigur(a)?';
$lang['resettofactory'] = 'Restabilire valori de fabricare';
$lang['error_template'] = 'Sablon eroare';
$lang['error_templatenameexists'] = 'Un sablon cu cu acest Nume exista deja';
$lang['friendlyname'] = 'Extensii modul Calguys';
$lang['postinstall'] = 'Acest modul este pregatit pentru folosire.  Codare usoara!';
$lang['postuninstall'] = 'La revedere';
$lang['uninstalled'] = 'Modul dezinstalat.';
$lang['installed'] = 'Versiunea %s a modului a fost instalata.';
$lang['prefsupdated'] = 'Preferinte modul actualizate.';
$lang['accessdenied'] = 'Acces interzis. Verificati-va permisiunile va rog.';
$lang['error'] = 'Eroare!';
$lang['upgraded'] = 'Modul actualizat la versiunea %s.';
$lang['moddescription'] = 'Acest modul este o biblioteca de clase php folosite pentru construirea de formulare avansate.';
$lang['changelog'] = '<ul>
<li>Version 1.0.0. March 2007.  Initial Release.</li>
<li>Version 1.0.1. April 2007.  Fixes to CreateImageLink</li>
<li>Version 1.0.2. April 2007.  CreateImageLink will now work on the frontend.</li>
<li>Version 1.0.3. September 2007.  Adds apply buttons to new templates.</li>
<li>Version 1.1.   September 2007.  Adds default frontend error handling plugin.</li>
</ul>';
$lang['help'] = '<h3>Ce realizeaza acest modul?</h3>
<p>Acest modul ofera in principal interfete aplicatie si etichete smarty pentru folosirea lor in alte module. Este constituit din clase de baza si clase utilitare disponibile pentru alte module.</p>
<h3>Cum il folosesc</h3>
<p>Ei bine, incepeti modulul dumneavoastra (sugerez sa incepeti cu modulul Skeleton), si apoi cand doriti sa folositi un obiect formular avansat din aceasta biblioteca, trebuie doar sa faceti modulul dumneavoastra dependent din FormObjects, si sa instantiati un obiect de tipul corespunzator.  Vezi codul din directorul FormObjects pentru instructiuni de utilizare.</p>
<h3>Addon-uri Smarty</h3>
<p>Acest modul ofera in plus cateva conveniente smarty pentru folosirea in alte module. Acestea sunt listate si descrise aici:</p>
<ul>
<li>cgerror - <em>bloc</em> plugin
<p>i.e: <code>{cgerror}Acesta este textul erorii{/cgerror}</code><br/>
    or: <code>{cgerror}{$errortextvar}{/cgerror}</br>
</p>
<p>parametri optionali: &#039;errorclass&#039; = suprascrie numele clasei implicite in sablon.
</p>
<p>Descriere: Acest plugin bloc foloseste sablonul eroare (configurabil din interfata de administrare CGExtensions) pentru afisearea unui mesaj de eroare.</p>
</ul>
<h3>Suport</h3>
<p>Acest modul nu ofera suport comercial. Totusi, exista un numar disponibil de resurse care va pot ajuta:</p>
<ul>
<li>Pentru ultima versiune a acestui modul, FAQ-uri, raportarea unui Bug sau achizitia de suport comercial, va rog sa vizitati cms development forge la <a href=&quot;http://dev.cmsmadesimple.org&quot;>dev.cmsmadesimple.org</a>.</li>
<li>Discutii referitoare la acest modul pot fi gasite la <a href=&quot;http://forum.cmsmadesimple.org&quot;>CMS Made Simple Forums</a>.</li>
<li>Autorul, calguy si altii pot fi adesea gasiti la <a href=&quot;irc://irc.freenode.net/#cms&quot;>CMS IRC Channel</a>.</li>
<li>In final, ati putea avea succes prin trimiterea unui e-mail direct autorului.</li>  
</ul>
<p>Referitor la GPL, acest soft este oferit asa cum este. Va rog sa cititi textul licentei pentru disculpa totala.</p>

<h3>Copyright si Licenta</h3>
<p>Copyright &copy; 2007, Robert Campbell <a href=&quot;mailto:calguy1000@hotmail.com&quot;><calguy1000@hotmail.com></a>. Toate drepturile rezervate.</p>
<p>Acest modul a fost livrat sub <a href=&quot;http://www.gnu.org/licenses/licenses.html#GPL&quot;>Licenta publica GNU</a>. Trebuie sa fiti de acord cu aceasta inainte sa folositi modulul.</p>
';
$lang['utmz'] = '156861353.1211532692.6.3.utmccn=(organic)|utmcsr=google|utmctr=http://translations.cmsmadesimple.org/|utmcmd=organic';
$lang['utma'] = '156861353.18530930.1207129453.1211452481.1211532306.6';
$lang['utmb'] = '156861353.2.10.1211532306';
$lang['utmc'] = '156861353';
?>
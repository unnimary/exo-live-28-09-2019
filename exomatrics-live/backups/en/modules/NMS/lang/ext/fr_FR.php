<?php
$lang['html'] = 'HTML ';
$lang['prompt_text_only'] = 'Ce message ne contient que du texte';
$lang['prompt_archivable'] = 'Ce message peut-il figurer dans les listes d&#039;archives publiques';
$lang['select_one'] = 'En s&eacute;lectionner un';
$lang['help_archivemsg_template'] = 'Sp&eacute;cifie un gabarit de la liste des gabarits archiv&eacute;s, qui doit &ecirc;tre utilis&eacute; pour l&#039;affichage d&#039;un seul message. Si cet argument n&#039;est pas sp&eacute;cifi&eacute; dans la balise, le gabarit message d&#039;archive marqu&eacute; &quot;par d&eacute;faut&quot; sera utilis&eacute;.';
$lang['help_archivelist_template'] = 'Sp&eacute;cifie un gabarit de la liste des gabarits archiv&eacute;s, qui doit &ecirc;tre utilis&eacute; pour l&#039;affichage archivelist. Ceci est utilis&eacute; avec l&#039;action &#039;archivelist&#039;, et si aucune valeur n&#039;est d&eacute;finie, le gabarit marqu&eacute; &quot;par d&eacute;faut&quot; sera utilis&eacute;';
$lang['help_limit'] = 'Utilis&eacute; lors de l&#039;action &#039;archivelist&#039;, ce param&egrave;tre d&eacute;termine le nombre maximal de messages archiv&eacute;s &agrave; afficher.';
$lang['help_sortorder'] = 'Utilis&eacute; lors de l&#039;action &#039;archivelist&#039;, ce param&egrave;tre d&eacute;termine l&#039;ordre de tri des messages affich&eacute;s. Les valeurs possibles sont ASC et DESC';
$lang['help_sortby'] = 'Utilis&eacute; lors de l&#039;action &#039;archivelist&#039;, ce param&egrave;tre g&egrave;re le champ utilis&eacute; pour trier les entr&eacute;es. Les valeurs possibles sont msgid, id, subjectdate and entered';
$lang['title_setdflt_archivemsgtmpl'] = 'Gabarit par D&eacute;faut Affichage de Message';
$lang['info_setdflt_archivemsgtmpl'] = 'Ce formulaire vous permet de sp&eacute;cifier le contenu par d&eacute;faut du gabarit d&#039;affichage de message quand vous cr&eacute;ez un nouveau gabarit dans la section Affichage de message de l&#039;onglet &#039;Gabarit d&#039;archive&#039;. Modifier ce contenu n&#039;aura pas d&#039;effet imm&eacute;diat sur les pages de votre site.';
$lang['addedit_archivemsg_template'] = 'Ajoute/Edite Gabarit Affichage de Message';
$lang['info_templatemessage_archivemsg'] = ' ';
$lang['modified'] = 'Modifi&eacute;';
$lang['text_message'] = 'Message Texte';
$lang['html_message'] = 'Message HTML';
$lang['message_id'] = 'ID du message';
$lang['archive_summary_templates'] = 'Archive des Gabarits de Sommaire';
$lang['archive_detail_templates'] = 'Archive des Gabarits de D&eacute;tail';
$lang['title_setdflt_archivelistmpl'] = 'Gabarit par d&eacute;faut de la liste des archives';
$lang['info_setdflt_archivelisttmpl'] = 'Ce formulaire vous permet de sp&eacute;cifier le contenu par d&eacute;faut d&#039;un gabarit archive quand vous cr&eacute;ez un nouveau gabarit dans l&#039;onglet &#039;gabarit d&#039;archive&#039;.  Modifier ce contenu n&#039;aura pas d&#039;effet imm&eacute;diat sur les pages de votre site.';
$lang['title_setdflt_messagetemplate'] = 'Gabarit par d&eacute;faut du message';
$lang['info_setdflt_messagetemplate'] = 'Ce formulaire vous permet de modifier le message mod&egrave;le qui sera utilis&eacute; quand vous cr&eacute;erez un nouveau gabarit dans l&#039;onglet des gabarits des messages.';
$lang['addedit_archivelist_template'] = 'Ajouter/Editer Gabarit de Liste d&#039;Archive';
$lang['info_templatemessage_archivelist_'] = ' ';
$lang['default_templates'] = 'Gabarits par D&eacute;faut';
$lang['archive_templates'] = 'Gabarits d&#039;Archive';
$lang['jobs_warning'] = '<h3>Attention:</h3>Une utilisation non conforme de ce module peut causer de graves probl&egrave;mes avec votre h&eacute;bergeur, vos clients, et potentiellement avec la loi. Le d&eacute;veloppeur de ce module rejette tout risque ou toute responsabilit&eacute; dus &agrave; l&#039;utilisation de ce code.  <strong>Utilisation &agrave; vos risques et p&eacute;rils</strong>';
$lang['warning'] = 'Attention';
$lang['send_admin_copy'] = 'Envoie une Copie &agrave; l&#039;Admin';
$lang['message_charset'] = 'Encodage des caract&egrave;res du message&nbsp;';
$lang['bounce_limit'] = 'Limite de Rejet&nbsp;';
$lang['info_bounce_limit'] = 'Le nombre de rejets autoris&eacute;s avant qu&#039;un utilisateur soit d&eacute;sactiv&eacute; (max 100)';
$lang['bounce_messagelimit'] = 'Message de Limite de Rejet&nbsp;';
$lang['info_bounce_messagelimit'] = 'Le nombre de messages pop3 &agrave; ex&eacute;cuter en m&ecirc;me temps (max 1000).';
$lang['error_pop3_processing'] = 'Probl&egrave;me de mise en oeuvre d&#039;un rejet';
$lang['error_pop3_connect'] = 'Probl&egrave;me de connexion au serveur pop3';
$lang['error_invalidbounces'] = 'Le Nombre de Rejets est invalide (0 &agrave; 100)';
$lang['bounce_count'] = 'Nombre de Rejets';
$lang['nms_job_complete_subject'] = 'T&acirc;che de Courriel NMS effectu&eacute;e';
$lang['nms_job_complete_msg'] = 'La t&acirc;che de courriel NMS est finie.  Elle a dur&eacute; %s secondes';
$lang['pop3_server'] = 'Serveur pop3&nbsp;';
$lang['pop3_username'] = 'Identifiant pop3&nbsp;';
$lang['pop3_password'] = 'Mot de passe pop3&nbsp;';
$lang['admin_email'] = 'Adresse email de l&#039;admin&nbsp;';
$lang['admin_name'] = 'Nom de l&#039;admin&nbsp;';
$lang['admin_replyto'] = 'Adresse de retour (ReplyTo) de l&#039;admin&nbsp;';
$lang['process_bounces'] = 'Ex&eacute;cute les bounces';
$lang['error_notemplates'] = 'Impossible de trouver un gabarit correspondant';
$lang['error_notemplatebyname'] = 'Impossible de trouver le gabarit nomm&eacute; %s';
$lang['addedit_template'] = 'Ajout ou &eacute;dition du gabarit message';
$lang['prompt_textmessage'] = 'Message texte';
$lang['found'] = 'trouv&eacute;';
$lang['template'] = 'Gabarit';
$lang['templates'] = 'Gabarits';
$lang['add_template'] = 'Ajout de gabarit';
$lang['message_templates'] = 'Gabarits message';
$lang['default_confirm_subject'] = 'Veuillez confirmer votre inscription';
$lang['default_postsubscribe_text'] = 'Merci pour votre inscription.';
$lang['default_subscribe_subject'] = 'Inscription confirm&eacute;e';
$lang['default_unsubscribe_subject'] = 'Notification de d&eacute;sinscription';
$lang['cleantemptable'] = 'Table temporaire nettoy&eacute;e';
$lang['error_problemwithmessage'] = 'Probl&egrave;me message non sp&eacute;cifi&eacute;';
$lang['prompt_usersettings_page'] = 'Page de retour pour le formulaire des param&egrave;tres utilisateurs&nbsp;';
$lang['info_usersettings_page'] = 'Page de retour quand un lien email est cliqu&eacute; pour &eacute;diter les pr&eacute;f&eacute;rences utilisateur.<br/>L&#039;ast&eacute;risque (*) indique la page par d&eacute;faut.';
$lang['jobname'] = 'T&acirc;che cr&eacute;e &agrave;';
$lang['msg_jobprocessing'] = 'Fen&ecirc;tre d&#039;ex&eacute;cution par lot';
$lang['prompt_page'] = 'Page de retour&nbsp;';
$lang['error_insufficientparams'] = 'Un ou plusieurs param&egrave;tres requis sont manquants dans la requ&ecirc;te. Cette action ne peut pas &ecirc;tre ex&eacute;cut&eacute;e.';
$lang['prompt_from'] = 'Exp&eacute;diteur&nbsp;';
$lang['prompt_replyto'] = 'R&eacute;pondre &agrave;&nbsp;';
$lang['prompt_subject'] = 'Sujet&nbsp;';
$lang['prompt_template'] = 'Gabarit&nbsp;';
$lang['prompt_selectstatus'] = 'S&eacute;lectionner le statut';
$lang['prompt_message'] = 'Message ';
$lang['prompt_userfilter'] = 'Filtre email (regexp)';
$lang['prompt_listfilter'] = 'Filtre sur liste des membres';
$lang['info_listfilter'] = '<strong>Note :</strong> Tenir la touche CTRL enfonc&eacute;e et cliquer sur les &eacute;l&eacute;ments pour les s&eacute;lectionner ou les d&eacute;s&eacute;lectionner
';
$lang['message_help'] = 'En cr&eacute;ant un message, les variables <em>smarty</em> suivantes sont disponibles&nbsp;:<br/>
<em>{$username}</em> - Nom de l&#039;utilisateur<br/>
<em>{$email}</em> - Adresse email de l&#039;utilisateur<br/>
<em>{$unsubscribe}</em> - URL qui peut &ecirc;tre utilis&eacute;e pour l&#039;affichage de la page de d&eacute;sinscription<br/>
<em>{$preferences}</em> - URL qui peut &ecirc;tre utilis&eacute;e pour l&#039;affichage de la page des pr&eacute;f&eacute;rences<br/>
<em>{$confirmurl}</em> - URL qui peut &ecirc;tre utilis&eacute;e pour la confirmation des inscriptions<br/>
<p>De plus, vous pouvez utiliser n&#039;importe quelle autre variable, fonction ou &#039;modifier&#039; de Smarty. Aidez-vous de {get_template_vars} et du &#039;modifier&#039; {$object|print_r} pour d&eacute;bugguer vos messages.</p>';
$lang['message_help2'] = '<h3>Note</h3>
<p>Si vous n&#039;avez pas de gabarit de message proprement d&eacute;fini, alors vous serez seulement capable de cr&eacute;er des messages texte, utilisant les contenus de la zone d&#039;&eacute;dition &quot;Message texte&quot;. Si au moins un bloc de contenu existe, le message sera suppos&eacute; &ecirc;tre en format HTML. Si la zone d&#039;&eacute;dition &quot;Message texte&quot; reste vide, alors la partie multipart de l&#039;email ne sera pas envoy&eacute;e.</p>';
$lang['applyfilter'] = 'Filtre';
$lang['info_event_OnNewUser'] = 'G&eacute;n&eacute;r&eacute; quand un nouvel utilisateur s&#039;inscrit &agrave; une ou plusieurs listes';
$lang['info_event_OnEditUser'] = 'G&eacute;n&eacute;r&eacute; quand un utilisateur est modifi&eacute; aussi bien dans la partie administration que dans la partie publique';
$lang['info_event_OnDeleteUser'] = 'G&eacute;n&eacute;r&eacute; quand un utilisateur est supprim&eacute;';
$lang['info_event_OnNewList'] = 'G&eacute;n&eacute;r&eacute; quand une mailing liste est modifi&eacute;e';
$lang['info_event_OnDeleteList'] = 'G&eacute;n&eacute;r&eacute; quand une mailing liste est supprim&eacute;e';
$lang['info_event_OnCreateMessage'] = 'G&eacute;n&eacute;r&eacute; quand une mailing liste est cr&eacute;&eacute;e';
$lang['info_event_OnEditMessage'] = 'G&eacute;n&eacute;r&eacute; quand un message est modifi&eacute;';
$lang['info_event_OnDeleteMessage'] = 'G&eacute;n&eacute;r&eacute; quand un message est supprim&eacute;';
$lang['info_event_OnCreateJob'] = 'G&eacute;n&eacute;r&eacute; quand une t&acirc;che est cr&eacute;&eacute;e';
$lang['info_event_OnDeleteJob'] = 'G&eacute;n&eacute;r&eacute; quand une t&acirc;che est supprim&eacute;e';
$lang['info_event_OnStartJob'] = 'G&eacute;n&eacute;r&eacute; quand l&#039;ex&eacute;cution d&#039;une t&acirc;che commence';
$lang['info_event_OnFinishJob'] = 'G&eacute;n&eacute;r&eacute; quand l&#039;ex&eacute;cution d&#039;une t&acirc;che est termin&eacute;e';
$lang['help_OnNewUser'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand un nouvel utilisateur s&#039;incrit &agrave; une ou plusieurs listes</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>email</em> - Adresse email de l&#039;utilisateur</li>
<li><em>Nom de l&#039;utilisateur</em> - Nom de l&#039;utilisateur</li>
<li><em>listes</em> - Tableau des listes de membres (array)</li>
</ul>
';
$lang['help_OnEditUser'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand un utilisateur est modifi&eacute; aussi bien dans la partie administration que dans la partie publique</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>email</em> - Adresse email de l&#039;utilisateur</li>
<li><em>username</em> - Nom de l&#039;utilisateur</li>
<li><em>lists</em> - Tableau (array) des listes de membres</li>
<li><em>id</em> - Identifiant de l&#039;utilisateur</li>
</ul>
';
$lang['help_OnDeleteUser'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand un utilisateur est supprim&eacute;</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>id</em> - Identifiant de l&#039;utilisateur</li>
</ul>
';
$lang['help_OnNewList'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand une nouvelle liste de diffusion est cr&eacute;&eacute;e</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>name</em> - Nom de la liste</li>
<li><em>description</em> - Description de la liste</li>
<li><em>public</em> - Public (oui/non)</li>
</ul>
';
$lang['help_OnEditList'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand une liste de diffusion est modifi&eacute;e</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>name</em> - Nom de la liste</li>
<li><em>description</em> - Description de la liste</li>
<li><em>public</em> - Public (oui/non)</li>
<li><em>listid</em> - Identifiant de la liste</li>
</ul>
';
$lang['help_OnDeleteList'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand une liste de diffusion est supprim&eacute;e</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>listid</em> - ID de la liste</li>
</ul>
';
$lang['help_OnCreateMessage'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand un message est cr&eacute;&eacute;</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>fromwho</em> - Nom de la personne originaire du message</li>
<li><em>reply_to</em> - Adresse email pour le champ de r&eacute;ponse (ReplyTo)</li>
<li><em>subject</em> - Sujet du message</li>
<li><em>message</em> - Corps du message <em>(peut contenir des balises smarty)</em></li>
<li><em>entered</em> - Date de cr&eacute;ation du message</li>
<li><em>uniqueid</em> - Id unique de ce message</em>
</ul>
';
$lang['help_OnEditMessage'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand un message est modifi&eacute;</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>fromwho</em> - Nom de la personne originaire du message</li>
<li><em>reply_to</em> - Adresse email pour le champ de r&eacute;ponse (ReplyTo)</li>
<li><em>subject</em> - Sujet du message</li>
<li><em>message</em> - Corps du message <em>(peut contenir des balises smarty)</em></li>
<li><em>messageid</em> - ID de ce message</id>
</ul>
';
$lang['help_OnDeleteMessage'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand un message est supprim&eacute;</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>messageid</em> - ID du message qui a &eacute;t&eacute; supprim&eacute;</li>
</ul>
';
$lang['help_OnCreateJob'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand une t&acirc;che est cr&eacute;&eacute;e</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>jobid</em> - ID de la t&acirc;che</li>
<li><em>jobname</em> - Nom de la t&acirc;che</li>
<li><em>lists</em> - Tableau des listids</li>
</ul>
';
$lang['help_OnDeleteJob'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand une t&acirc;che est supprim&eacute;e</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>jobid</em> - ID de cette t&acirc;che</li>
</ul>
';
$lang['help_OnStartJob'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand l&#039;ex&eacute;cution d&#039;une t&acirc;che commence</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>jobid</em> - ID de cette t&acirc;che</li>
<li><em>jobname</em> - Nom de cette t&acirc;che</li>
</ul>
';
$lang['help_OnFinishJob'] = '<p>Ev&eacute;nement g&eacute;n&eacute;r&eacute; quand l&#039;ex&eacute;cution d&#039;une t&acirc;che est termin&eacute;e</p>
<h4>Param&egrave;tres</h4>
<ul>
<li><em>jobid</em> - ID de cette t&acirc;che</li>
<li><em>jobname</em> - Nom de cette t&acirc;che</li>
</ul>';
$lang['error_needreplyto'] = 'Vous devez entrer une r&eacute;ponse valide pour l&#039;adresse email';
$lang['error_needfrom'] = 'Vous devez entrer un nom valide pour l&#039;exp&eacute;diteur de ce message';
$lang['error_needsubject'] = 'Vous devez entrer un sujet valide';
$lang['error_needmessagetext'] = 'Vous devez entrer quelque chose dans le corps du message';
$lang['error_formerror'] = 'Erreur du formulaire';
$lang['error_dberror'] = 'Erreur de base de donn&eacute;e';
$lang['error_nameexists'] = 'Un objet de ce nom existe d&eacute;j&agrave;';
$lang['error_itemnotfound'] = 'L&#039;objet sp&eacute;cifi&eacute; n&#039;a pas &eacute;t&eacute; trouv&eacute;';
$lang['error_invalidparam'] = 'Param&egrave;tre invalide';
$lang['invalidparam'] = 'Param&egrave;tre invalide dans la balise du module NMS';
$lang['prompt_users_per_page'] = 'Nombres d&#039;utilisateurs &agrave; afficher sur chaque page des listes d&#039;utilisateurs';
$lang['disabled'] = 'D&eacute;sactiv&eacute;';
$lang['confirmed'] = 'Confirm&eacute;';
$lang['prompt_usersettings_text2'] = 'Texte affich&eacute; apr&egrave;s la soumission du formulaire des param&egrave;tres utilisateurs&nbsp;';
$lang['prompt_usersettings_form2'] = 'Formulaire affich&eacute; aux utilisateurs d&eacute;sirant changer leurs param&egrave;tres&nbsp;';
$lang['prompt_usersettings_text'] = 'Texte affich&eacute; apr&egrave;s l&#039;envoi du message des param&egrave;tres utilisateur&nbsp;';
$lang['prompt_usersettings_email_body'] = 'Corps (body) du mail des param&egrave;tres utilisateur&nbsp;';
$lang['prompt_usersettings_subject'] = 'Sujet du mail de param&egrave;tres utilisateur&nbsp;';
$lang['prompt_usersettings_form'] = 'Formulaire demandant aux utilisateurs qui veulent changer leurs param&egrave;tres leur adresse email&nbsp;';
$lang['prompt_post_unsubscribe_text'] = 'Texte affich&eacute; une fois la d&eacute;sinscription r&eacute;alis&eacute;e';
$lang['user_settings'] = 'Param&egrave;tres utilisateurs';
$lang['prompt_unsubscribe_prompt'] = 'Invitation demandant &agrave; l&#039;utilisateur son email pour le d&eacute;sinscrire';
$lang['prompt_unsubscribe_text'] = 'Texte affich&eacute; une fois le formulaire de d&eacute;sinscription rempli';
$lang['prompt_unsubscribe_subject'] = 'Sujet de l&#039;email envoy&eacute; pour la d&eacute;sinscription';
$lang['prompt_unsubscribe_email_body'] = 'Corps (body) de l&#039;email envoy&eacute; pour la d&eacute;sinscription';
$lang['prompt_unsubscribe_form'] = 'Gabarit pour le formulaire de d&eacute;sinscription';
$lang['error_accountdisabled'] = 'Nous sommes d&eacute;sol&eacute;, mais votre compte a &eacute;t&eacute; d&eacute;sactiv&eacute; et cette action n&#039;est pas possible en ce moment.';
$lang['prompt_post_confirm_text'] = 'Message affich&eacute; apr&egrave;s l&#039;email de confirmation&nbsp;';
$lang['prompt_confirm_email_body'] = 'Corps (body) de l&#039;email de confirmation';
$lang['prompt_confirm_subject'] = 'Sujet de l&#039;email de confirmation';
$lang['confirm_subscribe'] = 'Confirmer l&#039;inscription';
$lang['confirm_unsubscribe'] = 'Confirmer la d&eacute;sinscription';
$lang['nolists'] = 'Aucune listes trouv&eacute;e pour s&#039;inscrire';
$lang['public'] = 'Public ';
$lang['yes'] = 'Oui';
$lang['no'] = 'Non';
$lang['prompt_postsubscribetext'] = 'Message affich&eacute; apr&egrave;s l&#039;inscription';
$lang['subscription_confirmed'] = 'Inscription confirm&eacute;e';
$lang['reset'] = 'R&eacute;initialisation';
$lang['suspend'] = 'Pause';
$lang['resume'] = 'Continue';
$lang['action'] = 'Action ';
$lang['processing_records'] = 'Ex&eacute;cution des enregistrements %s &agrave; %s';
$lang['initializing_job'] = 'Initialisation de la t&acirc;che';
$lang['processing_job'] = 'Effectue la t&acirc;che';
$lang['deletecompleted'] = 'Supprimer les t&acirc;ches accomplies';
$lang['error_importerrorcount'] = 'Erreur de comptage';
$lang['lines'] = 'Lignes';
$lang['users_added'] = 'Utilisateurs ajout&eacute;s';
$lang['memberships'] = 'Adh&eacute;sions';
$lang['subscribe'] = 'S&#039;inscrire';
$lang['resetdefaults'] = 'R&eacute;initialisation aux param&egrave;tres par d&eacute;faut';
$lang['confirm_adjustsettings'] = '&Ecirc;tes-vous sur de vouloir changer ces param&egrave;tres?';
$lang['confirm_resetdefaults'] = '&Ecirc;tes-vous sur de vouloir r&eacute;initialiser ce formulaire aux valeurs syst&egrave;mes par d&eacute;faut?';
$lang['prompt_subscribetext'] = 'Texte d&#039;inscription';
$lang['prompt_subscribesubject'] = 'Sujet du mail d&#039;inscription&nbsp;';
$lang['prompt_subscribe_email_body'] = 'Corps du mail d&#039;inscription&nbsp;';
$lang['prompt_subscribe_form'] = 'Gabarit pour le formulaire d&#039;inscription';
$lang['subscribe_form'] = 'Inscription';
$lang['unsubscribe_form'] = 'D&eacute;sinscription';
$lang['error_insertinglist'] = 'Probl&egrave;me lors de la cr&eacute;ation de la liste';
$lang['error_emailexists'] = 'Cette adresse email existe d&eacute;ja dans la liste d&#039;utilisateurs';
$lang['error_invalidusername'] = 'Nom d&#039;utilisateur invalide';
$lang['error_invalidid'] = 'Identifiant (id) unique invalide';
$lang['error_invalidemail'] = 'ERREUR - Adresse email invalide';
$lang['username'] = 'Nom d&#039;utilisateur';
$lang['name'] = 'Nom';
$lang['prompt_public'] = 'Liste d&#039;envoi publique';
$lang['error_usernotfound'] = 'Impossible de trouver l&#039;utilisateur avec l&#039;id unique sp&eacute;cifi&eacute;';
$lang['prompt_ms_between_message_sleep'] = 'D&eacute;lai (en millisecondes) entre l&#039;envoi de chaque message';
$lang['prompt_between_batch_sleep'] = 'D&eacute;lai (en seconde) entre chaque op&eacute;ration';
$lang['prompt_messages_per_batch'] = 'Le nombre maximum de messages &agrave; envoyer par lot';
$lang['okclosewindow'] = 'Vous pouvez maintenant fermer cette fen&ecirc;tre';
$lang['queuefinished'] = 'Fin du traitement de la file d&#039;attente';
$lang['totaltime'] = 'Temps d&#039;ex&eacute;cution total:';
$lang['seconds'] = 'Secondes';
$lang['totalmails'] = 'Nombre total d&#039;emails envoy&eacute;s (administrateur inclus):';
$lang['page'] = 'Page ';
$lang['of'] = 'de';
$lang['info_csvformat'] = 'Le fichier import&eacute; doit &ecirc;tre au format CSV (valeurs s&eacute;par&eacute;es par des virgules), une entr&eacute;e par ligne.<br />
 * Les colonnes sont  <code>adresse email, nom de l&#039;utilisateur</code><br />
 * Le nom de l&#039;utilisateur est optionnel.<br />
 * Les commentaires (tout ce qui suit un # ou un //) sont ignor&eacute;s.<br />
 * Les lignes vides sont ignor&eacute;es.<br />';
$lang['prompt_lines'] = 'Lignes import&eacute;es';
$lang['prompt_usersadded'] = 'Utilisateurs ajout&eacute;s';
$lang['prompt_membershipsadded'] = 'Adh&eacute;sions ajout&eacute;es';
$lang['prompt_errorcount'] = 'Nombre d&#039;erreurs';
$lang['importerror_cantgetuserid'] = 'Erreur &agrave; la ligne %s, impossible de r&eacute;cup&eacute;rer l&#039;identifiant utilisateur %s';
$lang['importerror_cantcreateuser'] = 'Erreur &agrave; la ligne %s, impossible de cr&eacute;er l&#039;utilisateur %s';
$lang['importerror_nosuchlist'] = 'Erreur &agrave; la ligne %s, aucune liste existante %s';
$lang['importerror_nofields'] = 'Erreur &agrave; la ligne %s, pas assez de champs';
$lang['import_users'] = 'Utilisateurs import&eacute;s depuis le fichier CVS';
$lang['error_emptyfile'] = 'ERREUR - Un fichier vide a &eacute;t&eacute; charg&eacute;';
$lang['error_nofilesuploaded'] = 'ERREUR - aucun fichier n&#039;a &eacute;t&eacute; charg&eacute;';
$lang['filename'] = 'Nom du fichier d&#039;import';
$lang['title_users_import'] = 'Importer des utilisateurs';
$lang['title_users_export'] = 'Exporter des utilisateurs depuis la base de donn&eacute;es';
$lang['nummessages'] = '# de Messages';
$lang['created'] = 'Cr&eacute;e';
$lang['started'] = 'D&eacute;marr&eacute;';
$lang['finished'] = 'Termin&eacute;';
$lang['processjobs'] = 'Ex&eacute;cuter la t&acirc;che';
$lang['status_error'] = 'ERREUR';
$lang['status_unstarted'] = 'Non d&eacute;marr&eacute;';
$lang['status_inprogress'] = 'En cours';
$lang['status_paused'] = 'En pause';
$lang['status_complete'] = 'Termin&eacute;';
$lang['status_unknown'] = 'Inconnu';
$lang['error_jobnameexists'] = 'ERREUR - Une t&acirc;che du m&ecirc;me nom existe d&eacute;ja';
$lang['createjob'] = 'Cr&eacute;er une t&acirc;che';
$lang['prompt_email_user_on_admin_subscribe'] = 'Envoyer un email aux utilisateurs quand l&#039;administrateur ajoute manuellement des adh&eacute;sions &agrave; une liste';
$lang['error_nomessagesselected'] = 'Aucun message s&eacute;lectionn&eacute;';
$lang['error_nolistsselected'] = 'Aucune liste s&eacute;lectionn&eacute;e';
$lang['createjobmsg'] = 'S&eacute;lectionner un message, et une ou plusieurs listes pour envoyer le message';
$lang['error_nojobname'] = 'ERREUR - Aucun nom de t&acirc;che sp&eacute;cifier';
$lang['error_nolists'] = 'ERREUR - Aucune liste !';
$lang['error_nomessages'] = 'ERREUR - Aucun message !';
$lang['jobs'] = 'T&acirc;che';
$lang['status'] = 'Statut';
$lang['jobsfoundtext'] = 'T&acirc;ches trouv&eacute;es';
$lang['messagesfoundtext'] = 'Messages trouv&eacute;s';
$lang['entered'] = 'Entr&eacute;';
$lang['subject'] = 'Sujet';
$lang['from'] = 'Exp&eacute;diteur';
$lang['delete_user_confirm'] = '&Ecirc;tes-vous sur de vouloir supprimer cet utilisateur';
$lang['info_singlelist'] = 'Cet utilisateur sera ajout&eacute; &agrave; la liste s&eacute;lectionn&eacute;e';
$lang['error_selectonelist'] = 'ERREUR - Vous devez s&eacute;lectionner au moins une liste';
$lang['error_invaliduniqueid'] = 'ERREUR -  ID unique invalide';
$lang['error_couldnotfindjobpart'] = 'ERREUR - Impossible de trouver la t&acirc;che requise dans la base de donn&eacute;es. Ceci peut indiquer qu&#039;un autre utilisateur (vous) est en train d&#039;&eacute;diter ou de supprimer des t&acirc;ches pendant que celle-ci est en cours.';
$lang['error_couldnotfindmessage'] = 'ERREUR - Impossible de trouver le message requis. Ceci peut indiquer qu&#039;un autre utilisateur (vous?) est en train d&#039;&eacute;diter ou de supprimer des messages pendant qu&#039;un t&acirc;che est en cours.';
$lang['error_couldnotfindtemplate'] = 'ERREUR - Impossible de trouver le gabarit de page requis. Ceci peut indiquer que le gabarit a &eacute;t&eacute; supprim&eacute; depuis que le message email a &eacute;t&eacute; cr&eacute;&eacute;.';
$lang['error_temporarytableexists'] = 'ERREUR - La table temporaire dans la base de donn&eacute;e, utilis&eacute;e pour l&#039;ex&eacute;cution des t&acirc;ches, existe d&eacute;j&agrave;. Ceci indique probablement qu&#039;une erreur s&#039;est produite lors de l&#039;ex&eacute;cution d&#039;une t&acirc;che ant&eacute;rieure.';
$lang['error_buildingtemptable'] = 'ERREUR - Un probl&egrave;me est survenu lors du remplissage de la table temporaire';
$lang['error_otherprocessingerror'] = 'ERREUR - Un probl&egrave;me est survenu lors de l&#039;ex&eacute;cution';
$lang['userid'] = 'Identifiant utilisateur';
$lang['emailaddress'] = 'Adresse email';
$lang['usersfoundtext'] = 'Utilisateurs trouv&eacute;s';
$lang['title_user_createnew'] = 'Ajouter un utilisateur';
$lang['error_invalidlistname'] = 'ERREUR - Nom de liste invalide';
$lang['editlist_text'] = 'Modifier la liste';
$lang['id'] = 'Identifiant (ID)';
$lang['listsfoundtext'] = 'Listes trouv&eacute;es';
$lang['users'] = 'Utilisateurs';
$lang['preferences'] = 'Pr&eacute;f&eacute;rences';
$lang['messages'] = 'Messages ';
$lang['queue'] = 'File d&#039;attente';
$lang['submit'] = 'Envoyer';
$lang['cancel'] = 'Annuler';
$lang['description'] = 'Description ';
$lang['createnewlist_text'] = 'Cr&eacute;er une liste';
$lang['lists'] = 'Listes';
$lang['friendlyname'] = 'Newsletter Made Simple ';
$lang['postinstall'] = 'Merci d&#039;avoir install&eacute; NMS. V&eacute;rifier que vous avez bien la permission &quot;Use NMS&quot; pour utiliser ce module!';
$lang['postuninstall'] = 'Newsletter Made Simple d&eacute;sinstall&eacute;.';
$lang['uninstalled'] = 'Module d&eacute;sinstall&eacute;.';
$lang['installed'] = 'Version du module %s install&eacute;.';
$lang['prefsupdated'] = 'Les pr&eacute;f&eacute;rences de Newsletter Made Simple Module ont &eacute;t&eacute; mises &agrave; jour.';
$lang['newslettercreated'] = 'Une nouvelle newsletter a &eacute;t&eacute; cr&eacute;&eacute;e. ';
$lang['no_email_error'] = 'Vous devez rentrer une adresse email.';
$lang['subscribe_thankyou'] = 'Merci pour votre inscription. Un email de confirmation vous a &eacute;t&eacute; envoy&eacute;.';
$lang['enter_valid_email'] = 'Vous devez entrer une adresse email valide et s&eacute;lectionner au moins une liste.';
$lang['newslettercreatederror'] = 'Vous devez entrer un nom et une description.';
$lang['accessdenied'] = 'Acc&eacute;s refus&eacute;. Merci de v&eacute;rifier vos permissions.';
$lang['error'] = 'Erreur !';
$lang['sent'] = 'Envoy&eacute;';
$lang['inqueue'] = 'En file d&#039;attente';
$lang['send_next_batch'] = 'Envoi du prochain batch de %s emails; starting at: %s';
$lang['messages_sent'] = '%s ont &eacute;t&eacute; envoy&eacute;';
$lang['closewindow'] = 'Fermer la fen&ecirc;tre';
$lang['testmode'] = 'Vous &ecirc;tes en mode test. Aucun email ne sera envoy&eacute;.';
$lang['confirmdeletejob'] = '&Ecirc;tes-vous s&ucirc;r de vouloir supprimer cette t&acirc;che ?';
$lang['confirmsend'] = '&Ecirc;tes-vous s&ucirc;r de vouloir envoyer tous les messages? \n\nL&#039;envoi de grandes quantit&eacute;s de messages peut prendre un temps consid&eacute;rable et utiliser une grande part des ressources de votre h&eacute;bergeur.\n\nUtilisation &agrave; vos risques et p&eacute;rils</strong>';
$lang['confirmdelete'] = '&Ecirc;tes-vous s&ucirc;r de vouloir supprimer ce message ?';
$lang['confirmdeletelist'] = '&Ecirc;tes-vous s&ucirc;r de vouloir supprimer cette liste ?';
$lang['keepwindowopen'] = 'Merci de laisser cette fen&ecirc;tre ouverte jusqu&#039;&agrave; l&#039;envoi de tous les messages.<br>';
$lang['profileupdated'] = 'Votre profil a &eacute;t&eacute; mis &agrave; jour.';
$lang['upgraded'] = 'Module NMS mis &agrave; jour vers la version %s.';
$lang['title_mod_prefs'] = 'Configuration ';
$lang['title_mod_messages'] = 'File d&#039;attente des messages';
$lang['title_mod_createnew'] = 'Cr&eacute;er une liste';
$lang['title_mod_manage_user'] = 'Utilisateurs';
$lang['title_mod_compose_job'] = 'Cr&eacute;er une t&acirc;che';
$lang['title_mod_compose_message'] = 'Composer un message';
$lang['title_mod_process_queue'] = 'Ex&eacute;cute la file d&#039;attente';
$lang['title_mod_admin'] = 'G&eacute;rer les listes';
$lang['user_delete_confirm'] = '&Ecirc;tes-vous s&ucirc;r de vouloir supprimer cet utilisateur ?';
$lang['userdeleted'] = 'Utilisateur supprim&eacute;';
$lang['newsletterdeleted'] = 'Newsletter supprim&eacute;e';
$lang['delete'] = 'Supprimer';
$lang['edit'] = 'Modifier';
$lang['previous'] = 'Pr&eacute;c&eacute;dent';
$lang['next'] = 'Suivant';
$lang['unsubscribemessage'] = 'Vous avez &eacute;t&eacute; d&eacute;sincrit. Merci';
$lang['title_admin_panel'] = 'Newsletter Made Simple ';
$lang['moddescription'] = 'Ce module vous permet de cr&eacute;er un syst&egrave;me de newsletter';
$lang['welcome_text'] = '<p>Bienvenue dans le panneau d&#039;administration du module Newsletter Made Simple (NMS) </p>';
$lang['changelog'] = '<ul>
<li>Todo:
<ul>
<li>Lists Tab - show disabled vs active lists</li>
<li>Lists - ability to Mark a list as inactive</li>
<li>Users - Ability to filter on confirmed or unconfirmed
<p>Admin should be able to check all unconfirmed users and either confirm them all, or send them another confirmation email message</p>
<li>Users - Ability to prefer text or html mail (much later)</li>
<li>Something to allow a user to re-get the confirmation email</li>
<li>Docs, docs, and more docs</li>
<li><b>The frontend</b></li>
<ul>
  <li>complete the two stage unsubscribe process</li>
  <li>complete the two stage change preferences process</li>
  <li>Add a preference as to wether or not users should get a confirmation email after subscribing and unsubscribing</p>
</ul>
<li>Styling the progress page</li>
</ul>
</li>
<li>Version 2.2 - June, 2008
    <p>Now require CMS 1.3.1</p>
    <p>Fix the user settings functionality.</p>
    <p>Fix the CSV Import stuff to be more useable</p>
    <p>Adds the ability to mark a message for display in the archive list.  This solves the problem where some messages were showing up in the archive list even if they were just sent to a private list.  This is because messages in NMS are re-useable and you may send the same message to multiple lists at the same time.</p>
    <p>Fixes problems with the archive message display</p>
    <p>Adds the ability to mark a message as text only</p>
</li>
<li>Version 2.1.1 - March, 2008 (just after 2.1)
    <p>Adds two missing lines that resulted in the confirmation not working when subscribing</p>
</li>
<li>Version 2.1 - March, 2008
    <p>Adds the ability for other modules to interact with NMS</p>
</li>
<li>Version 2.0.1 - February, 2008
    <p>Bump dependencies,</p>
</li>
<li>Version 2.0 - December, 2007
    <p><strong>Note:</strong> This is a Significant set of enhancements to NMS that required breaking backwards compatibility, this version will NOT upgrade from previous versions of NMS.  You should export all data, save it to text files, etc.... and re-import the data later.</p>
    <ul>
      <li>Complete templating support</li>
      <li>Complete support for multipart messages (text and html)</li>
      <li>Complete support for embedded images and attachments</li>
      <li>Bounce Processing via pop3</li>
      <li>Significant rewrite of the frontend (archive and showtemplate actions)</li>
    </ul>
</li>
<li>Version 1.0.2 - August, 2007</li>
<p>Allow importing users from FEU <em>(originally done by skypanther, re-implemented by calguy100).</em></p>
<p>Numerous bug fixes</p>
<li>Version 1.0.1 - December, 2006</li>
<p>Fixes to import users, and to a ternery expression when creating a message. I also fixed some stupid problems with process_queue resulting from me doing this too quickly.</p>
<li>Version 1.0 - December, 2006</li>
<p>This <b>is</b> essentially a complete rewrite of the old NMS module.  Everything has been cleaned up and attemtpts have been made to bring it up to proper standards, and lots of new features added. here is a list of the major improvements:</p>
<ul>
<li>Cleanup of the lang strings, etc.</li>
<li>Param-ize the queries for security</li>
<li>Added the concept of Jobs, so that messages can be re-used</li>
<li>Added the concept of username (optional)</li>
<li>Added smarty processing on templates</li>
<li>Added bulk import</li>
<li>Added the concept of a &#039;private list&#039;</p>
<li>Devided the admin panel into tabs</li>
<li>Show progress nicely when processing large jobs</li>
<li>Uses CMSMailer module</li>
<li>Events that can be trapped to add additional behaviour</li>
</ul>
<p><strong>Note</strong>, upgrade from the previous version <em>(Including previous betas)</em>is not possible.  A complete uninstall of the old version is required before installing this version of NMS.</p>
</li>
<li>Version .74 27 November 2005. Alpha 3 Release. Fixed bug with confirmation message returnid, image urls, windows php, and a few other small issues.</li>
<li>Version .73 23 November 2005. Alpha 2 Release. Fixed bug with adding messages.</li>
<li>Version .71 22 November 2005. Alpha 1 Release.</li>
<li>Version .5. 17 September 2005. Internal Release.</li>
</ul>';
$lang['helpselect'] = 'Sp&eacute;cifie une liste, s&eacute;par&eacute;e par des virgules, des listes d&#039;envoi &agrave; afficher.  <em>(Valide seulement en mode souscription)</em>';
$lang['helpaction'] = 'Une des fa&ccedil;ons de sp&eacute;cifier le comportement de ce module. Les options sont&nbsp;:
<ul>
<li>archivelist - affiche les archives des messages</li>
<li>showmessage - affiche un message sp&eacute;cifique</li>
</ul>
';
$lang['helpmode'] = 'Sp&eacute;cifie le mode op&eacute;ratoire. Les options sont&nbsp;:
<ul>
<li>subscribe (default) - affiche le formulaire d&#039;inscription</li>
<li>unsubscribe - affiche le formulaire de d&eacute;sinscription</li>
<li>usersettings - affiche le formulaire des param&egrave;tres utilisateur</li>
</ul>
';
$lang['help'] = '<h3>Newsletter Made Simple (NMS) </h3>
<h3><strong>Que fait ce module ? </strong></h3>
<p>Vous permet de disposer d&#039;un syst&egrave;me de newsletter par courriel dans CMS Made Simple.</p>
<p>Features:
<ul>
  <li>Allows for public (users can subscribe and unsubscribe at will) and private (admins maintained) lists</li>
  <li>Allows for email confirmation of subscription and unsubscribe activities</li>
  <li>Allows for disabling certain users</li>
  <li>Import and export of users</li>
  <li>Completely template driven</li>
  <li>Allows displaying archived messages in the frontend</li>
  <li>Complete template driven messaging system</li>
  <li>Messages can be stored and re-used</li>
  <li>Allows for embedding images into emails</li>
  <li>Allows attaching files to emails</li>
  <li>Handles bounces</li>
  <li>Supports pretty urls</li>
  <li>Lots more</li>
</ul>
</p>
<h4 style=&quot;color: red;&quot;>A LIRE EN PREMIER</h4>
<p>Ce module peut &ecirc;tre utilis&eacute; pour envoyer des courriels en grande quantit&eacute; et doit &ecirc;tre utilis&eacute; par des utilisateurs avertis. Il n&#039;est pas destin&eacute; aux utilisateurs ne disposant pas des connaissances li&eacute;es aux avantages et inconv&eacute;nients de l&#039;envoi en grande quantit&eacute; de courriels en HTML. Si vous n&#039;avez pas ces connaissances, vous ne devriez pas utiliser ce logiciel.</p>
<p>Vous devriez vous renseigner sur les th&egrave;mes suivants avant d&#039;utiliser ce logiciel :</p>
<ul>
  <li>Comment envoyer des courriels en grande quantit&eacute; sans qu&#039;ils ne soient interpr&eacute;t&eacute;s comme ind&eacute;sirables ?</li>
  <li>Repeatedly sending spam, or sending spam to a large amount of email addresses could get your domain blacklisted, and possibly all of the domains hosted by your ISP.  <strong>Use extreme caution.</strong></li>
  <li>Comment envoyer des courriels au format HTML et qu&#039;ils puissent &ecirc;tre lus par vos clients</li>
  <li>Your hosts policy and limitations about email sending</li>
  <li>If sending bulk email using an address list you acquired or purchased from others there may be legal rammifications for sending unsolicited email.</li>
</ul>
<p>This module can and will require significant additional resources on your host.  The minimum CMS Made Simple requirements <strong>are probably not sufficient</strong>.  You should be prepared to debug issues, and contact your host to increase limits such as memory limit and timeouts before using this module.  This module is best used on a host where you have complete control of the php settings.</p>
<p>The developers and supporters of this package assume no risk or responsibility for its improper use. <strong>Use At Your Own Risk.</strong></p>
<h3>Comment utiliser ce module ?</h3>
<ol>
<li>Installez le module (ce qui doit normalement &ecirc;tre fait &agrave; cette &eacute;tape).</li>
<li>Grant appropriate permissions to the groups that will manage your lists. these include:
	<ul>
	<li>Manage NMS Lists &amp;mdash; Add, remove mailing lists</li>	
	<li>Manage NMS Users &amp;mdash; Add, remove users in the database</li>
	<li>Manage NMS Messages &amp;mdash; Add, edit, remove messages but not send them</li> 	
	<li>Manage NMS Jobs &amp;mdash; Send messages and perform other &#039;job&#039; functions</li>	
	</ul>
</li>
<li>Cr&eacute;ez une liste</li>
<li>(Optionnel) Ajoutez la balise {cms_module module=&#039;NMS&#039;} dans une page pour permettre aux visiteurs de votre site de s&#039;enregistrer.</li>
<li>(Optionnel) Ajoutez manuellement des clients dans votre liste dans l&#039;onglet <strong>Utilisateurs</strong> depuis la partie d&#039;administration du module.</li>
<li>Create a message to be sent as part of a job</li>
<li>Create a job. You will select a message to send and a list to send it to.</li>
<li>Process the job and your message will be sent</li>
</ol>

<h3>Syntaxe basique</h3>
<p>{NMS}</p>

<h3>Autres options</h3>

<h4>Afficher le lien de d&eacute;sinscription &agrave; la liste :</h4>
<p>{NMS mode=&#039;unsubscribe&#039;}</p>

<h4>Afficher la page des r&eacute;glages de l&#039;utilisateur :</h4>
<p>{NMS mode=&#039;usersettings&#039;}</p>

<h4>Afficher les archives des anciennes newsletters :</h4>
<p>{NMS action=&#039;archivelist&#039;}</p>
<ul>
<lh>Param&eacute;tres optionnels pour l&#039;archive :</lh>
<li>show=&#039;#&#039;  o&ugrave; <code>#</code> est le nombre d&#039;anciennes newsletters &agrave; afficher. Si n&#039;est pas pr&eacute;cis&eacute;, toutes seront affich&eacute;es.</li>
<li>sortby=&#039;date&#039; &amp;mdash; <code>date</code> (d&eacute;fault), <code>id</code> (message ID), ou <code>subject</code></li>
<li>sortorder=&#039;DESC&#039; &amp;mdash; either <code>DESC</code> (descending order, default) or <code>ASC</code> (ascending order)</li>
<li>dateformat=&#039;%Y-%m-%d&#039; &amp;mdash; sets the date output format following the PHP strftime() syntax</li>
</ul>		

<h4>Importer des utilisateurs depuis la base de donn&eacute;es du Front End Users</h4>
<p>Note: You must have a property defined in FEU that stores the users&#039; email address. It can be named whatever you want.</p>
<ol>
<li>Choose Extensions &amp;gt; Newsletter Made Simple</li>
<li>Activate the Users tab</li>
<li>At the bottom, click Import Users from FrontEndUsers</li>
<li>Select the FEU field that contains the users&#039; email addresses</li>
<li>Specify whether to copy the FEU user name to the NMS user name field. This is what would be output in your message if you included the {<span>$</span>username} placeholder.</li>
<li>Select the list to import to.</li>
<li>Click Submit.</li>
</ol>
<p>You will receive a report of the users processed, those already in your NMS database, those already on the list, and those that were added and subscribed. Users are not duplicated so there should be no harm in running an import periodically.</p>

<h3>Message variables</h3>
<p>These are covered elsewhere, but added here for completeness. If you add these to your message, they will be replaced at sending time with the appropriate true values.</p>

<ul>
<li>{<span>$</span>username}  &amp;mdash;  The user&#039;s name</li>
<li>{<span>$</span>email}  &amp;mdash;  The user&#039;s email address</li>
<li>{<span>$</span>unsubscribe}  &amp;mdash;  A URL that can be used to display a page for unsubscribing</li>
<li>{<span>$</span>preferences}  &amp;mdash;  A URL that can be used to display a user preferences page</li>
<li>{<span>$</span>confirmurl}  &amp;mdash;  A URL that can be used to confirm subscriptions</li>
</ul>
<h3>Bounce Processing</h3>
<p>The built in bounce processing capabilities allow reading a pop3 mailbox and searching the mails in that inbox for matching messages.  If the user can be properly detected, the &#039;bounce count&#039; for that user will be incremented.  Once the &#039;bounce limit&#039; is reached, the user will be disabled.</p>
<p><strong>Note:</strong> Your hosts email server configuration may be such that you don&#039;t receive bounce notifications in your pop3 mail box.  You may have to work with your hosting provider to ensure that this functionality works for you.</p>
<p>It is strongly recommended that you configure your sending email address to be the same as your pop3 email account, and that you use this account only for bounce processing.</p>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Bug Report  please visit <a href=&#039;http://dev.cmsmadesimple.org/projects/newsletter/&#039; target=&#039;_blank&#039;>http://dev.cmsmadesimple.org/projects/newsletter/</a>.</li>
<li>Additional discussion of this module may also be found in the <a href=&#039;http://forum.cmsmadesimple.org&#039; target=&#039;_blank&#039;>CMS Made Simple Forums</a>.</li>
<li>The author, Robert Campbell, can often be found in the <a href=&#039;irc://irc.freenode.net/#cms&#039;>CMS IRC Channel</a> (username: calguy1000).</li>
<li>Lastly, you may have some success emailing the author directly.</li>  
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text
of the license for the full disclaimer.</p>
<h3>Copyright and License</h3>
<p>Copyright &amp;copy, 2007, Robert Campbell <a href=&#039;mailto:calguy1000@hotmail.com&#039;>calguy1000@hotmail.com</a>.  All Rigts Are Reserved.</p>
<p>Credits to the original author of the module:, Paul Lemke <a href=&#039;mailto:lemkepf@gmx.net&#039;>&amp;lt;lemkepf@gmx.net&amp;gt;</a></p>
<p>This module has been released under the <a href=&#039;http://www.gnu.org/licenses/licenses.html#GPL&#039;>GNU Public License</a>. You must agree to this license before using the module.</p>';
$lang['import_feu_title'] = 'Importer les utilisateur depuis le module FrontEndUsers';
$lang['import_feu_info'] = 'S&eacute;lectionner le groupe FEU &agrave; importer dans NMS. <em>(seuls les groupes avec un champ email sont affich&eacute;s)</em>';
$lang['import_feu_noproperties'] = 'Vous n&#039;avez pas d&eacute;fini de propri&eacute;t&eacute;s dans le module FrontEndUsers. Vous devez d&eacute;finir au moins une propri&eacute;t&eacute; pour enregistrer l&#039;adresse email de l&#039;utilisateur';
$lang['import_feu_prompt_groupname'] = 'Groupe FrontEndUsers';
$lang['import_feu_prompt_copyusername'] = 'Copier le nom de l&#039;utilisateur FEU dans NMS?';
$lang['import_feu_info_copyusername'] = '<em>Si vous s&eacute;lectionnez &#039;Non&#039;, le champ nom sera laiss&eacute; vide.</em>';
$lang['import_feu_selectlists'] = 'Importer les utilisateur dans (nom de liste)';
$lang['import_feu_feunotinstalled'] = 'Le module Front End Users n&#039;est pas install&eacute; ou les propri&eacute;t&eacute;s de d&eacute;finition de table n&#039;existent pas. Impossible d&#039;importer des utilisateurs depuis le module Front End Users.';
$lang['processedAddressesTitle'] = 'Adresses ex&eacute;cut&eacute;es';
$lang['inDatabaseTitle'] = 'D&eacute;j&agrave; dans NMS';
$lang['onListAlreadyTitle'] = 'D&eacute;j&agrave; inscrit';
$lang['addressSubscribedTitle'] = 'Inscrit';
$lang['listidInfo'] = 'Importation &agrave; la liste dont l&#039;ID est&nbsp;:';
$lang['processedAddressesCountInfo'] = 'D&eacute;j&agrave; ex&eacute;cut&eacute;s&nbsp;:';
$lang['archive_heading'] = '<h1>Archive des messages</h1>';
$lang['archivedmessage'] = '<h1>Messages archiv&eacute;s</h1>';
$lang['archive_tbl_msgID'] = 'ID de message';
$lang['archive_tbl_subject'] = 'Sujet';
$lang['archive_tbl_fullurl'] = 'Lien au message';
$lang['archive_tbl_href'] = 'Cible du lien';
$lang['archive_tbl_date'] = 'Date ';
$lang['utma'] = '156861353.23806501199468600.1229087340.1236692280.1236764336.45';
$lang['utmz'] = '156861353.1236167241.41.27.utmcsr=dev.cmsmadesimple.org|utmccn=(referral)|utmcmd=referral|utmcct=/';
$lang['utmb'] = '156861353.1.10.1236764336';
$lang['utmc'] = '156861353';
?>
<?php /* Smarty version 2.3.1, created on 2007-09-12 15:41:43
         compiled from default/address-list.htm */ ?>
<?php $this->_config_load($this->_tpl_vars['umLanguageFile'], "AddressBook", 'local'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>UebiMiau - <?php echo $this->_config[0]['vars']['adr_title']; ?>
</title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_config[0]['vars']['default_char_set']; ?>
">
</head>
<link rel="stylesheet" href="themes/default/webmail.css" type="text/css">
<script language="JavaScript" src="themes/default/webmail.js" type="text/javascript"></script>

<?php echo $this->_tpl_vars['umJS']; ?>


<body bgcolor="#778899" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">


<table width="760" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor=white>
        <tr>
                <td valign=top width="15%">
                        <table cellspacing=1 cellpadding=3 width="100%" border=0 bgcolor=White>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"> <a class="menu" href="javascript:goinbox()"><?php echo $this->_config[0]['vars']['messages_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:newmsg()"><?php echo $this->_config[0]['vars']['compose_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:refreshlist()"><?php echo $this->_config[0]['vars']['refresh_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:folderlist()"><?php echo $this->_config[0]['vars']['folders_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:search()"><?php echo $this->_config[0]['vars']['search_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:addresses()"><?php echo $this->_config[0]['vars']['address_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:emptytrash()"><?php echo $this->_config[0]['vars']['empty_trash_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:prefs()"><?php echo $this->_config[0]['vars']['prefs_mnu']; ?>
</a></td></tr>
                                <tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:goend()"><?php echo $this->_config[0]['vars']['logoff_mnu']; ?>
</a></td></tr>
                        </table>
                </td>

                <td bgcolor=white valign=top>
                        <table width="100%" border="0" cellspacing="1" cellpadding="0">
                                <tr>
                                        <td width="40%" class="headers"><?php echo $this->_config[0]['vars']['adr_name_hea']; ?>
</td>
                                        <td width="40%" class="headers"><?php echo $this->_config[0]['vars']['adr_email_hea']; ?>
</td>
                                        <td width="10%" class="headers"><?php echo $this->_config[0]['vars']['adr_edit_hea']; ?>
</td>
<!---
                                         <td width="10%" class="headers"><?php echo $this->_config[0]['vars']['adr_expo_hea']; ?>
</td>
--->
                                        <td width="10%" class="headers"><?php echo $this->_config[0]['vars']['adr_dele_hea']; ?>
</td>
                                </tr>
                                <?php if (isset($this->_sections["i"])) unset($this->_sections["i"]);
$this->_sections["i"]['name'] = "i";
$this->_sections["i"]['loop'] = is_array($this->_tpl_vars['umAddressList']) ? count($this->_tpl_vars['umAddressList']) : max(0, (int)$this->_tpl_vars['umAddressList']);
$this->_sections["i"]['show'] = true;
$this->_sections["i"]['max'] = $this->_sections["i"]['loop'];
$this->_sections["i"]['step'] = 1;
$this->_sections["i"]['start'] = $this->_sections["i"]['step'] > 0 ? 0 : $this->_sections["i"]['loop']-1;
if ($this->_sections["i"]['show']) {
    $this->_sections["i"]['total'] = $this->_sections["i"]['loop'];
    if ($this->_sections["i"]['total'] == 0)
        $this->_sections["i"]['show'] = false;
} else
    $this->_sections["i"]['total'] = 0;
if ($this->_sections["i"]['show']):

            for ($this->_sections["i"]['index'] = $this->_sections["i"]['start'], $this->_sections["i"]['iteration'] = 1;
                 $this->_sections["i"]['iteration'] <= $this->_sections["i"]['total'];
                 $this->_sections["i"]['index'] += $this->_sections["i"]['step'], $this->_sections["i"]['iteration']++):
$this->_sections["i"]['rownum'] = $this->_sections["i"]['iteration'];
$this->_sections["i"]['index_prev'] = $this->_sections["i"]['index'] - $this->_sections["i"]['step'];
$this->_sections["i"]['index_next'] = $this->_sections["i"]['index'] + $this->_sections["i"]['step'];
$this->_sections["i"]['first']      = ($this->_sections["i"]['iteration'] == 1);
$this->_sections["i"]['last']       = ($this->_sections["i"]['iteration'] == $this->_sections["i"]['total']);
?>
                                <tr>
                                        <td class="default"> &nbsp;<a class="menu" href="<?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['viewlink']; ?>
"><?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['name']; ?>
</a></td>
                                        <td class="default"> &nbsp;<a class="menu" href="<?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['composelink']; ?>
"><?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['email']; ?>
</a></td>
                                        <td class="default"> &nbsp;<a class="menu" href="<?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['editlink']; ?>
">OK</a></td>
<!---
                                        <td class="default"> &nbsp;<a class="menu" href="<?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['exportlink']; ?>
">OK</a></td>
--->
                                        <td class="default"> &nbsp;<a class="menu" href="<?php echo $this->_tpl_vars['umAddressList'][$this->_sections['i']['index']]['dellink']; ?>
">OK</a></td>
                                </tr>
                                <?php endfor; endif; ?>
                                <tr><td colspan=3 align=center><br><form><input type=button value="<?php echo $this->_config[0]['vars']['adr_new_entry']; ?>
" class=button onClick="location = '<?php echo $this->_tpl_vars['umNew']; ?>
'"></form></td></tr>
                        </table>

                </td>
        </tr>
</table>



</body>
</html>
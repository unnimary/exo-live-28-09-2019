<?
########################################################################
# Language & themes settings
########################################################################

$allow_user_change_theme			= yes; //allow users select theme
$default_theme						= 0; //index of theme, starting with zero
$allow_user_change_language			= yes; //allow users select language
$default_language					= 5; //index of language, starting with zero

$themes[] = Array(
	"name" 		=> "UebiMiau Default",
	"path" 		=> "default"
);

$themes[] = Array(
	"name" 		=> "Test",
	"path" 		=> "test"
);

$languages[] = Array(
	"name" 		=> "Bulgarian",
	"path"	 	=> "bg"
);

$languages[] = Array(
	"name" 		=> "Czech",
	"path"	 	=> "cz_iso"
);

$languages[] = Array(
	"name" 		=> "Chinese Traditional (BIG5)",
	"path"	 	=> "ch_big5"
);

$languages[] = Array(
	"name" 		=> "Dutch (Netherlands)",
	"path" 		=> "nl"
);

$languages[] = Array(
	"name" 		=> "Danish",
	"path" 		=> "dk"
);

$languages[] = Array(
	"name" 		=> "English",
	"path" 		=> "en_UK"
);

$languages[] = Array(
	"name" 		=> "Espa�ol",
	"path" 		=> "es"
);

$languages[] = Array(
	"name" 		=> "French",
	"path" 		=> "fr"
);

$languages[] = Array(
	"name" 		=> "German",
	"path" 		=> "de"
);

 
$languages[] = Array(
	"name" 		=> "Hebrew",
	"path" 		=> "he"
);

$languages[] = Array(
	"name" 		=> "Hungarian",
	"path" 		=> "hu"
);

$languages[] = Array(
	"name" 		=> "Italian",
	"path" 		=> "it"
);

$languages[] = Array(
	"name" 		=> "Icelandic",
	"path" 		=> "is"
);

$languages[] = Array(
	"name" 		=> "Norwegian",
	"path" 		=> "nb"
);

$languages[] = Array(
	"name" 		=> "Polish",
	"path" 		=> "pl"
);

$languages[] = Array(
	"name" 		=> "Portuguese",
	"path" 		=> "pt_BR"
);

$languages[] = Array(
	"name" 		=> "Romenian",
	"path" 		=> "ro"
);

$languages[] = Array(
	"name" 		=> "Russian",
	"path" 		=> "ru"
);


$languages[] = Array(
	"name" 		=> "Russian (KOI8)",
	"path" 		=> "ru_KOI"
);


$languages[] = Array(
	"name" 		=> "Swedish",
	"path"	 	=> "se"
);

$languages[] = Array(
	"name" 		=> "Thailand",
	"path" 		=> "th"
);

$languages[] = Array(
	"name" 		=> "Turkish",
	"path" 		=> "tr"
);

?>
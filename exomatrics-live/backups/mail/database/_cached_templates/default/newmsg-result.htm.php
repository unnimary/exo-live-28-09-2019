<?php /* Smarty version 2.3.1, created on 2008-03-14 14:01:02
         compiled from default/newmsg-result.htm */ ?>
<?php $this->_config_load($this->_tpl_vars['umLanguageFile'], "Newmessage", 'local'); ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>UebiMiau</title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_config[0]['vars']['default_char_set']; ?>
">
	<link rel="stylesheet" href="themes/default/webmail.css" type="text/css">
	<script language="JavaScript" src="themes/default/webmail.js" type="text/javascript"></script>


<?php echo $this->_tpl_vars['umJS']; ?>


</head>

<body bgcolor="#778899" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">

<table cellspacing=2 cellpadding=0 border=0 align=center bgcolor=White width="760">
	<tr>
		<td valign=top width="15%">
			<table cellspacing=1 cellpadding=3 width="100%" border=0 bgcolor=White>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"> <a class="menu" href="javascript:goinbox()"><?php echo $this->_config[0]['vars']['messages_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:folderlist()"><?php echo $this->_config[0]['vars']['folders_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:search()"><?php echo $this->_config[0]['vars']['search_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:addresses()"><?php echo $this->_config[0]['vars']['address_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:emptytrash()"><?php echo $this->_config[0]['vars']['empty_trash_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:prefs()"><?php echo $this->_config[0]['vars']['prefs_mnu']; ?>
</a></td></tr>
				<tr><td bgcolor="#EEE5DE" onmouseover="mOvr(this,'#D1D7ED');" onmouseout="mOut(this,'#EEE5DE');" onclick="mClk(this);"><a class="menu" href="javascript:goend()"><?php echo $this->_config[0]['vars']['logoff_mnu']; ?>
</a></td></tr>
			</table>		
		</td>
		<td valign=center bgcolor=white>
			<table width="100%" border="0" cellspacing="1" cellpadding="0">
			<?php if ($this->_tpl_vars['umMailSent']): ?>
				<tr>
					<td class=cent>
						<?php echo $this->_config[0]['vars']['result_success']; ?>
<br><br>
						<a href="messages.php?sid=<?php echo $this->_tpl_vars['umSid']; ?>
&tid=<?php echo $this->_tpl_vars['umTid']; ?>
&lid=<?php echo $this->_tpl_vars['umLid']; ?>
"><?php echo $this->_config[0]['vars']['nav_continue']; ?>
</a>
					</td>
				</tr>
			<?php else: ?>
				<tr>
					<td class=cent>
						<?php echo $this->_config[0]['vars']['result_error']; ?>
<br><br>
						<font color=red><?php echo $this->_tpl_vars['umErrorMessage']; ?>
</font><br><br>
						<a href="javascript:history.go(-1)"><?php echo $this->_config[0]['vars']['nav_back']; ?>
</a>
					</td>
				</tr>
			<?php endif; ?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
